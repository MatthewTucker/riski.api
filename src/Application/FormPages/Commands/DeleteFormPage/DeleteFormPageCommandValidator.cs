﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.FormPages;

namespace Riski.Api.Application.DefinedAnswers
{
    class DeleteFormPageCommandValidator : AbstractValidator<DeleteFormPageCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteFormPageCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Id).NotNull().MustAsync(DoesExist).WithMessage("Id not found");
        }

        public async Task<bool> DoesExist(Guid id, CancellationToken cancellationToken)
        {
            var formPage = await _context.FormPages.FindAsync(id, cancellationToken);
            return formPage != null;
        }
    }
}
