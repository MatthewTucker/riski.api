﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;
using Microsoft.EntityFrameworkCore;
using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.FormPages
{
    public class DeleteFormPageCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteFormPageCommandHandler : IRequestHandler<DeleteFormPageCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteFormPageCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteFormPageCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var formPage = await _context.FormPages
                .Include(fp => fp.AlertDialogs)
                    .ThenInclude(ad => ad.EventCondition)
                .Include(fp => fp.UIComponents)
                .SingleOrDefaultAsync(fp => fp.Id == request.Id, cancellationToken);

            if (formPage.UIComponents != null)
            {
                _context.UIComponents.RemoveRange(formPage.UIComponents);
            }

            foreach (var alertDialog in formPage.AlertDialogs)
            {
                if (alertDialog.EventCondition != null)
                {
                    _context.EventConditions.Remove(alertDialog.EventCondition);
                }

                _context.AlertDialogs.Remove(alertDialog);
            }

            _context.FormPages.Remove(formPage);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
