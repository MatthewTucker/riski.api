﻿using System;
using System.Collections.Generic;

using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.FormPages
{
    public class UpdateFormPageDto
    {
        public Guid Id { get; set; }
        public string PageName { get; set; }
        public string Title { get; set; }
        public int Position { get; set; }
        public string PageType { get; set; }
        public virtual List<AlertDialogDto> NavigateNext { get; set; }
        public List<QuestionDto> Questions { get; set; }
        public Guid TemplateId { get; set; }
        public string Description { get; set; }
        public Guid FormPageTypeId { get; set; }
        public List<UIComponentDto> UIComponents { get; set; }
        public object Extras { get; set; }
    }
}
