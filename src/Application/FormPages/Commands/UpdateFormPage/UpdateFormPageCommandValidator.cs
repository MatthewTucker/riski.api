﻿using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

using System.Threading;
using System.Threading.Tasks;

namespace Riski.Api.Application.FormPages
{
    class UpdateFormPageCommandValidator : AbstractValidator<UpdateFormPageCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateFormPageCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateFormPage)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UpdateFormPage).MustAsync(DoesExist).WithMessage("The specified FormPage does not exist.");
                    RuleFor(v => v.UpdateFormPage.PageName).NotEmpty();
                    RuleFor(v => v.UpdateFormPage.Position).NotEmpty();
                    RuleFor(v => v.UpdateFormPage.PageType).NotEmpty();
                    RuleFor(v => v.UpdateFormPage.NavigateNext).NotNull();
                    RuleFor(v => v.UpdateFormPage.Questions).NotNull();
                    RuleFor(v => v.UpdateFormPage.TemplateId).NotEmpty();
                    RuleFor(v => v.UpdateFormPage.Description).NotEmpty();
                    RuleFor(v => v.UpdateFormPage.FormPageTypeId).NotEmpty();
                    RuleFor(v => v.UpdateFormPage.PageName).NotNull();
                });
        }

        public async Task<bool> DoesExist(UpdateFormPageDto formPage, CancellationToken cancellationToken)
        {
            var exists = await _context.FormPages.AnyAsync(u => formPage.Id == u.Id, cancellationToken);
            return exists;
        }
    }
}

