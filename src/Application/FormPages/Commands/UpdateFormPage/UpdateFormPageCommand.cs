﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.FormPages
{
    public class UpdateFormPageCommand : IRequest<Guid>
    {
        public UpdateFormPageDto UpdateFormPage { get; set; }
    }

    public class UpdateFormPageCommandHandler : IRequestHandler<UpdateFormPageCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateFormPageCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateFormPageCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateFormPage;

            var oldFormPage = await _context.FormPages
                                                .Include(fp => fp.AlertDialogs)
                                                    .ThenInclude(ad => ad.EventCondition)
                                            .SingleOrDefaultAsync(fp => fp.Id == requestDto.Id, cancellationToken);

            var isBeingUsed = await _context.Forms.AnyAsync(f => f.Template.Id == oldFormPage.TemplateId);

            if (isBeingUsed)
            {
                oldFormPage.Status = PersistenceStatus.Deleted;
                foreach (var ad in oldFormPage.AlertDialogs)
                {
                    ad.Status = PersistenceStatus.Deleted;
                    ad.EventCondition.Status = PersistenceStatus.Deleted;
                };
            }
            else
            {
                var eventConditions = oldFormPage.AlertDialogs.Select(ad => ad.EventCondition);
                var alertDialogs = oldFormPage.AlertDialogs;

                _context.EventConditions.RemoveRange(eventConditions);
                _context.AlertDialogs.RemoveRange(alertDialogs);
                _context.FormPages.Remove(oldFormPage);
            }

            var formPage = new FormPage
            {
                PageName = requestDto.PageName,
                Description = requestDto.Description,
                Title = requestDto.Title,
                FormPageType = await _context.FormPageTypes.FindAsync(new object[] { requestDto.FormPageTypeId }, cancellationToken),
                AlertDialogs = GetAlertDialogs(requestDto.NavigateNext),
                UIComponents = await GetUIComponents(requestDto.UIComponents),
                Questions = await GetQuestions(requestDto.Questions),
                Status = PersistenceStatus.Available,
                Template = await _context.Templates.FindAsync(new object[] { requestDto.TemplateId }, cancellationToken),
                Extras = JsonSerializer.Serialize(requestDto.Extras),
                Position = requestDto.Position
            };

            UpdatePositions(formPage, 1);

            await _context.FormPages.AddAsync(formPage);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return formPage.Id;
        }

        private static List<AlertDialog> GetAlertDialogs(List<AlertDialogDto> alertDialogs)
        {
            return alertDialogs.Select(n =>
            {
                var alertDialog = new AlertDialog
                {
                    Title = n.title,
                    Message = n.message,
                    PositiveText = n.positiveText,
                    NegativeText = n.negativeText,
                    PositiveAction = n.positiveAction,
                    NegativeAction = n.negativeAction,
                    EventCondition = new EventCondition
                    {
                        Scope = n.condition.Scope,
                        Specific = n.condition.Specific,
                        Member = n.condition.Member,
                        Value = n.condition.Value
                    },
                    Position = n.position
                };

                if (n.extras != null)
                {
                    alertDialog.Extras = n.extras.ToString().Replace("\n", "");
                }

                return alertDialog;
            }).ToList();
        }

        private Task<List<UIComponent>> GetUIComponents(List<UIComponentDto> uIComponents)
        {
            var uiComponentIds = uIComponents.Select(q => q.Id).ToList();

            return _context.UIComponents.Where(q => uiComponentIds.Contains(q.Id)).ToListAsync();
        }

        private Task<List<Question>> GetQuestions(List<QuestionDto> questions)
        {
            var questionIds = questions.Select(q => q.Id).ToList();

            return _context.Questions.Where(q => questionIds.Contains(q.Id)).ToListAsync();
        }

        internal void UpdatePositions(FormPage formPage, int positionDelta)
        {
            var template = formPage.Template;
            var templateFormPages = template.FormPages
                                                .Where(fp => fp.Id != formPage.Id && fp.Status != PersistenceStatus.Available)
                                                .OrderBy(fp => fp.Position)
                                                .ToList();

            foreach (var templateFormPage in templateFormPages)
            {
                if (templateFormPage.Position >= formPage.Position)
                {
                    templateFormPage.Position += positionDelta;
                }
            }

            CleanPositions(template);
        }

        private void CleanPositions(Template template)
        {
            var position = 1;
            foreach (var fp in template.FormPages.OrderBy(tfp => tfp.Position))
            {
                if (fp.Status == PersistenceStatus.Available)
                {
                    fp.Position = position;
                    position++;
                }
                else
                {
                    fp.Position = -1;
                }
            }
        }
    }
}
