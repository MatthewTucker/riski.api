﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings.Dto;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.FormPages
{
    public class CreateFormPageCommand : IRequest<Guid>
    {
        public CreateFormPageDto CreateFormPageDto { get; set; }
    }

    public class CreateFormPageCommandHandler : IRequestHandler<CreateFormPageCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateFormPageCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateFormPageCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateFormPageDto;
            var formPage = new FormPage
            {
                PageName = requestDto.PageName,
                Description = requestDto.Description,
                Title = requestDto.Title,
                FormPageType = await _context.FormPageTypes.FindAsync(new object[] { requestDto.FormPageTypeId }, cancellationToken),
                AlertDialogs = requestDto.NavigateNext.Select(n => n.ToAlertDialog()).ToList(),
                UIComponents = await GetUIComponents(requestDto.UIComponents),
                Questions = await GetQuestions(requestDto.Questions),
                Status = PersistenceStatus.Available,
                TemplateId = requestDto.TemplateId,
                Template = await _context.Templates.FindAsync(new object[] { requestDto.TemplateId }, cancellationToken),
                Extras = JsonSerializer.Serialize(requestDto.Extras)
            };

            await _context.FormPages.AddAsync(formPage, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return formPage.Id;
        }

        private Task<List<UIComponent>> GetUIComponents(List<UIComponentDto> uIComponents)
        {
            var uiComponentIds = uIComponents.Select(q => q.Id).ToList();

            return _context.UIComponents.Where(q => uiComponentIds.Contains(q.Id)).ToListAsync();
        }

        private Task<List<Question>> GetQuestions(List<QuestionDto> questions)
        {
            var questionIds = questions.Select(q => q.Id).ToList();

            return _context.Questions.Where(q => questionIds.Contains(q.Id)).ToListAsync();
        }
    }
}
