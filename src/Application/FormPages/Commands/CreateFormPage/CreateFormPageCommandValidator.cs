﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.FormPages
{
    class CreateFormPageCommandValidator : AbstractValidator<CreateFormPageCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateFormPageCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateFormPageDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateFormPageDto).MustAsync(DoesNotExist).WithMessage("The specified FormPage Title already exists for this Template.");
                    RuleFor(v => v.CreateFormPageDto.PageName).NotEmpty();
                    RuleFor(v => v.CreateFormPageDto.Position).NotEmpty();
                    RuleFor(v => v.CreateFormPageDto.PageType).NotEmpty();
                    RuleFor(v => v.CreateFormPageDto.NavigateNext).NotNull();
                    RuleFor(v => v.CreateFormPageDto.Questions).NotNull();
                    RuleFor(v => v.CreateFormPageDto.TemplateId).NotEmpty();
                    RuleFor(v => v.CreateFormPageDto.Description).NotEmpty();
                    RuleFor(v => v.CreateFormPageDto.FormPageTypeId).NotEmpty();
                    RuleFor(v => v.CreateFormPageDto.PageName).NotNull();
                    RuleFor(v => v.CreateFormPageDto.Status).NotEmpty();
                });
        }

        public async Task<bool> DoesNotExist(CreateFormPageDto formPage, CancellationToken cancellationToken)
        {
            var exists = await _context.FormPages.AnyAsync(u => u.Title == formPage.Title && u.Template.Id == formPage.TemplateId, cancellationToken);
            return !exists;
        }
    }
}

