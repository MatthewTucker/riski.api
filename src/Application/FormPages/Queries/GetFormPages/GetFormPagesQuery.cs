﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.FormPages
{
    public class GetFormPagesQuery : IRequest<List<FormPageDto>>
    {
    }

    public class GetFormPagesQueryHandler : IRequestHandler<GetFormPagesQuery, List<FormPageDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetFormPagesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<FormPageDto>> Handle(GetFormPagesQuery request, CancellationToken cancellationToken)
        {
            var query = _context.FormPages
                .Include(fp => fp.FormPageType)
                .Include(fp => fp.Questions)
                .Include(fp => fp.AlertDialogs)
                    .ThenInclude(ad => ad.EventCondition)
                .Include(fp => fp.UIComponents)
                .Include(fp => fp.AlertDialogs)
                .Include(fp => fp.Template)
                .Select(r => r.ToFormPageDto());

            return await query.ToListAsync(cancellationToken);
        }
    }
}
