﻿using FluentValidation;

namespace Riski.Api.Application.FormPages
{
    public class GetFormPageQueryValidator : AbstractValidator<GetFormPageQuery>
    {
        public GetFormPageQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
