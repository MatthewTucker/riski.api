﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.FormPages
{
    public class GetFormPageQuery : IRequest<FormPageDto>
    {
        public Guid Id { get; set; }
    }

    public class GetFormPageQueryHandler : IRequestHandler<GetFormPageQuery, FormPageDto>
    {
        private readonly IApplicationDbContext _context;

        public GetFormPageQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<FormPageDto> Handle(GetFormPageQuery request, CancellationToken cancellationToken)
        {
            var formPage = await _context.FormPages
                .Include(fp => fp.FormPageType)
                .Include(fp => fp.Questions)
                .Include(fp => fp.AlertDialogs)
                .Include(fp => fp.UIComponents)
                .Include(fp => fp.AlertDialogs)
                .Include(fp => fp.Template)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return formPage.ToFormPageDto();
        }
    }
}
