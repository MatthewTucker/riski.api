﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.Sites
{
    public class GetSitesQuery : IRequest<List<SiteDto>>
    {
    }

    public class GetSitesQueryHandler : IRequestHandler<GetSitesQuery, List<SiteDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetSitesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<SiteDto>> Handle(GetSitesQuery request, CancellationToken cancellationToken)
        {
            return await _context.Sites
                .Include(s => s.Users)
                .Select(r => r.ToSiteDto()).ToListAsync();
        }
    }
}
