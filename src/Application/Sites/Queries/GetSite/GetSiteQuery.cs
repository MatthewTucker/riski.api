﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Sites
{
    public class GetSiteQuery : IRequest<SiteDto>
    {
        public Guid Id { get; set; }
    }

    public class GetSiteQueryHandler : IRequestHandler<GetSiteQuery, SiteDto>
    {
        private readonly IApplicationDbContext _context;

        public GetSiteQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<SiteDto> Handle(GetSiteQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.Sites
                .Include(s => s.Users)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return record.ToSiteDto();
        }
    }
}
