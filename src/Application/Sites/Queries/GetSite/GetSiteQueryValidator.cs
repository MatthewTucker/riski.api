﻿using FluentValidation;

namespace Riski.Api.Application.Sites
{
    public class GetSiteQueryValidator : AbstractValidator<GetSiteQuery>
    {
        public GetSiteQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
