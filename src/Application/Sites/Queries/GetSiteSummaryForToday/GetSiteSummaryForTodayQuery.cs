﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Sites
{
    public class GetSiteSummaryForTodayQuery : IRequest<List<DashboardSiteDataDto>>
    {
        public Guid SiteId { get; set; }
    }

    public class GetSiteSummaryForTodayQueryHandler : IRequestHandler<GetSiteSummaryForTodayQuery, List<DashboardSiteDataDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IDateTime _dateTime;

        public GetSiteSummaryForTodayQueryHandler(IApplicationDbContext context, IDateTime dateTime)
        {
            _context = context;
            _dateTime = dateTime;
        }

        public async Task<List<DashboardSiteDataDto>> Handle(GetSiteSummaryForTodayQuery request, CancellationToken cancellationToken)
        {
            var results = new List<DashboardSiteDataDto>();

            var sites = await _context.Sites.Include(s => s.Users).ToListAsync();

            foreach (var site in sites)
            {
                var data = new List<DashboardGraphDataDto>();
                for (int i = 1; i <= 24; i++)
                {
                    data.Add(new DashboardGraphDataDto() { FormsSubmitted = 0, Risks = 0, Timestamp = DateTime.Today.AddHours(i).ToString("hh tt") });
                }

                var siteData = new DashboardSiteDataDto
                {
                    site = new SiteDto
                    {
                        Id = site.Id,
                        Name = site.Name,
                        Status = site.Status.ToString(),
                        Managers = site.Users.Select(u => new ManagerDto
                        {
                            Id = u.Id,
                            FullName = $"{u.FirstName} {u.LastName}",
                            MobileNumber = u.PhoneNumber.ToString()
                        }).ToList()
                    }
                };

                var forms = await _context.Forms.Include(f => f.Risks).Where(f => f.DateTimeSubmitted.Date == _dateTime.Now.Date && f.Site.Id == site.Id).ToListAsync(cancellationToken);

                foreach (var form in forms)
                {
                    var timestamp = form.DateTimeSubmitted.ToString("hh tt");
                    var index = data.FindIndex(x => x.Timestamp.Equals(timestamp));
                    if (index > -1)
                    {
                        var item = data.ElementAt(index);
                        item.Risks += form.Risks.Count;
                        item.FormsSubmitted += 1;
                    }
                }

                siteData.graphData = data;

                results.Add(siteData);
            }

            return results;
        }
    }
}
