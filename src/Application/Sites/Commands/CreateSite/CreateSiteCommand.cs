﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Sites
{
    public class CreateSiteCommand : IRequest<Guid>
    {
        public CreateSiteDto CreateSite { get; set; }
    }

    public class CreateSiteCommandHandler : IRequestHandler<CreateSiteCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateSiteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateSiteCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateSite;

            var managerIds = requestDto.Managers?.Select(m => m.Id).ToList() ?? new List<Guid>();
            var managers = await _context.Users.Include(u => u.Sites).Where(u => managerIds.Contains(u.Id)).ToListAsync();

            var site = new Site
            {
                Name = requestDto.Name,
                Users = managers,
                Status = PersistenceStatus.Available
            };

            foreach (var manager in managers)
            {
                manager.Sites.Add(site);
            }

            await _context.Sites.AddAsync(site, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return site.Id;
        }
    }
}
