﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Sites
{
    class CreateSiteCommandValidator : AbstractValidator<CreateSiteCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateSiteCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateSite).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateSite.Name).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("Site already exists with this Name");
                });
        }

        public async Task<bool> IsNotDuplicate(string name, CancellationToken cancellationToken)
        {
            return await _context.Sites.AnyAsync(r => r.Name == name && r.Status != PersistenceStatus.Available, cancellationToken);
        }
    }
}

