﻿using System.Collections.Generic;

using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Sites
{
    public class CreateSiteDto
    {
        public string Name { get; set; }
        public List<ManagerDto> Managers { get; set; }
    }
}
