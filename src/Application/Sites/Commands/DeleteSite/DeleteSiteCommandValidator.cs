﻿using FluentValidation;

namespace Riski.Api.Application.Sites
{
    class DeleteSiteCommandValidator : AbstractValidator<DeleteSiteCommand>
    {
        public DeleteSiteCommandValidator()
        {
            RuleFor(v => v.Id).NotEmpty().WithMessage("Id not found");
        }
    }
}
