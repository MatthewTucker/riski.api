﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Sites
{
    public class DeleteSiteCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteSiteCommandHandler : IRequestHandler<DeleteSiteCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteSiteCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteSiteCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var record = await _context.Sites.FindAsync(request.Id);

            record.Status = PersistenceStatus.Deleted;

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
