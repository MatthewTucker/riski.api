﻿using System;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class UpdateDefinedAnswerCategoryDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
    }
}
