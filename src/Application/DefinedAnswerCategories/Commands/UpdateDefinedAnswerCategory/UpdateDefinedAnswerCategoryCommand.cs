﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class UpdateDefinedAnswerCategoryCommand : IRequest<Guid>
    {
        public UpdateDefinedAnswerCategoryDto UpdateDefinedAnswerCategory { get; set; }
    }

    public class UpdateDefinedAnswerCategoryCommandHandler : IRequestHandler<UpdateDefinedAnswerCategoryCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateDefinedAnswerCategoryCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateDefinedAnswerCategoryCommand request, CancellationToken cancellationToken)
        {
            var requestDto = request.UpdateDefinedAnswerCategory;

            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var definedAnswerCategory = await _context.DefinedAnswerCategories.FindAsync(new object[] { requestDto.Id }, cancellationToken);

            definedAnswerCategory.Value = requestDto.Value;
            definedAnswerCategory.Description = requestDto.Description;
            definedAnswerCategory.Status = Enum.Parse<PersistenceStatus>(requestDto.Status);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return definedAnswerCategory.Id;
        }
    }
}
