﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    class UpdateDefinedAnswerCategoryCommandValidator : AbstractValidator<UpdateDefinedAnswerCategoryCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateDefinedAnswerCategoryCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateDefinedAnswerCategory)
                .NotNull().WithMessage("Update body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UpdateDefinedAnswerCategory.Id).NotEmpty();
                    RuleFor(v => v.UpdateDefinedAnswerCategory.Description).NotEmpty();
                    RuleFor(v => v.UpdateDefinedAnswerCategory.Value).NotEmpty().MustAsync(BeUniqueCategory).WithMessage("The specified Category already exists.");
                });
        }

        public async Task<bool> BeUniqueCategory(string categoryName, CancellationToken cancellationToken)
        {
            return await _context.DefinedAnswerCategories.AllAsync(u => u.Value != categoryName, cancellationToken);
        }
    }
}
