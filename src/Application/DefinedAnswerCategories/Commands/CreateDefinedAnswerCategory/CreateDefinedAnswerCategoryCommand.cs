﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class CreateDefinedAnswerCategoryCommand : IRequest<Guid>
    {
        public CreateDefinedAnswerCategoryDto CreateDefinedAnswerCategoryDto { get; set; }
    }

    public class CreateDefinedAnswerCategoryCommandHandler : IRequestHandler<CreateDefinedAnswerCategoryCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateDefinedAnswerCategoryCommandHandler(IApplicationDbContext context, IIdentityService identityService)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateDefinedAnswerCategoryCommand request, CancellationToken cancellationToken)
        {
            var requestDto = request.CreateDefinedAnswerCategoryDto;

            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var definedAnswerCategory = new DefinedAnswerCategory
            {
                Value = requestDto.Value,
                Description = requestDto.Description
            };

            await _context.DefinedAnswerCategories.AddAsync(definedAnswerCategory, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return definedAnswerCategory.Id;
        }
    }
}
