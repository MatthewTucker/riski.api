﻿using System;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class CreateDefinedAnswerCategoryDto
    {
        public string Description { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
    }
}
