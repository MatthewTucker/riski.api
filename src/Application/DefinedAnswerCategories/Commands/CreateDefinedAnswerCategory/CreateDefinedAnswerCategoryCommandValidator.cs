﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    class CreateDefinedAnswerCategoryCommandValidator : AbstractValidator<CreateDefinedAnswerCategoryCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateDefinedAnswerCategoryCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateDefinedAnswerCategoryDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateDefinedAnswerCategoryDto.Description).NotEmpty();
                    RuleFor(v => v.CreateDefinedAnswerCategoryDto.Value).NotEmpty().MustAsync(BeUniqueCategory).WithMessage("The specified Category already exists.");
                });
        }

        public async Task<bool> BeUniqueCategory(string categoryName, CancellationToken cancellationToken)
        {
            return await _context.DefinedAnswerCategories.AllAsync(u => u.Value != categoryName, cancellationToken);
        }
    }
}
