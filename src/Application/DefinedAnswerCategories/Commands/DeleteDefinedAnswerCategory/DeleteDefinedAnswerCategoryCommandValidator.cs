﻿using FluentValidation;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    class DeleteDefinedAnswerCategoryCommandValidator : AbstractValidator<DeleteDefinedAnswerCategoryCommand>
    {
        public DeleteDefinedAnswerCategoryCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id body not found");
        }
    }
}
