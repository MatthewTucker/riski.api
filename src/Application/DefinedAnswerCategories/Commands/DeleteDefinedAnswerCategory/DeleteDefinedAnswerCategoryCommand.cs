﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class DeleteDefinedAnswerCategoryCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteDefinedAnswerCategoryCommandHandler : IRequestHandler<DeleteDefinedAnswerCategoryCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteDefinedAnswerCategoryCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteDefinedAnswerCategoryCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var definedAnswerCategory = await _context.DefinedAnswerCategories.FindAsync(new object[] { request.Id }, cancellationToken);


            _context.DefinedAnswerCategories.Remove(definedAnswerCategory);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
