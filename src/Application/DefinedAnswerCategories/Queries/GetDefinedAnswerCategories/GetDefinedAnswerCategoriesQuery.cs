﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class GetDefinedAnswerCategoriesQuery : IRequest<List<DefinedAnswerCategoryDto>>
    {
    }

    public class GetDefinedAnswerCategoriesQueryHandler : IRequestHandler<GetDefinedAnswerCategoriesQuery, List<DefinedAnswerCategoryDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetDefinedAnswerCategoriesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<DefinedAnswerCategoryDto>> Handle(GetDefinedAnswerCategoriesQuery request, CancellationToken cancellationToken)
        {
            return await _context.DefinedAnswerCategories.Select(r => r.ToDefinedAnswerCategoryDto()).ToListAsync(cancellationToken);
        }
    }
}
