﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class GetDefinedAnswerCategoryQuery : IRequest<DefinedAnswerCategoryDto>
    {
        public Guid Id { get; set; }
    }

    public class GetDefinedAnswerCategoryQueryHandler : IRequestHandler<GetDefinedAnswerCategoryQuery, DefinedAnswerCategoryDto>
    {
        private readonly IApplicationDbContext _context;

        public GetDefinedAnswerCategoryQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DefinedAnswerCategoryDto> Handle(GetDefinedAnswerCategoryQuery request, CancellationToken cancellationToken)
        {
            var definedAnswerCategory = await _context.DefinedAnswerCategories.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return definedAnswerCategory.ToDefinedAnswerCategoryDto();
        }
    }
}
