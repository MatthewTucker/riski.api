﻿using FluentValidation;

namespace Riski.Api.Application.DefinedAnswerCategories
{
    public class GetDefinedAnswerCategoryQueryValidator : AbstractValidator<GetDefinedAnswerCategoryQuery>
    {
        public GetDefinedAnswerCategoryQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
