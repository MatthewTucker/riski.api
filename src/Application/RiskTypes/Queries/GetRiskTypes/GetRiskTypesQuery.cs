﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.RiskTypes
{
    public class GetRiskTypesQuery : IRequest<List<RiskTypeDto>>
    {
    }

    public class GetRiskTypesQueryHandler : IRequestHandler<GetRiskTypesQuery, List<RiskTypeDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskTypesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<RiskTypeDto>> Handle(GetRiskTypesQuery request, CancellationToken cancellationToken)
        {
            return await _context.RiskTypes.Select(r => r.ToRiskTypeDto()).ToListAsync();
        }
    }
}
