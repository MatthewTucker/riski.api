﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.RiskTypes
{
    public class GetRiskTypeQuery : IRequest<RiskTypeDto>
    {
        public Guid Id { get; set; }
    }

    public class GetRiskTypeQueryHandler : IRequestHandler<GetRiskTypeQuery, RiskTypeDto>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskTypeQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<RiskTypeDto> Handle(GetRiskTypeQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.RiskTypes.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return record.ToRiskTypeDto();
        }
    }
}
