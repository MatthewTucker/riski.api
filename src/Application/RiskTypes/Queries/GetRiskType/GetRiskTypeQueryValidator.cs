﻿using FluentValidation;

namespace Riski.Api.Application.RiskTypes
{
    public class GetRiskTypeQueryValidator : AbstractValidator<GetRiskTypeQuery>
    {
        public GetRiskTypeQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
