﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskTypes
{
    public class DeleteRiskTypeCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteRiskTypeCommandHandler : IRequestHandler<DeleteRiskTypeCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteRiskTypeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteRiskTypeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var record = await _context.RiskTypes.FindAsync(request.Id);

            record.Status = PersistenceStatus.Deleted;

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
