﻿using FluentValidation;

namespace Riski.Api.Application.RiskTypes
{
    class DeleteRiskTypeCommandValidator : AbstractValidator<DeleteRiskTypeCommand>
    {
        public DeleteRiskTypeCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
