﻿using System;

namespace Riski.Api.Application.RiskTypes
{
    public class UpdateRiskTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
