﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskTypes
{
    public class UpdateRiskTypeCommand : IRequest<Guid>
    {
        public UpdateRiskTypeDto UpdateRiskType { get; set; }
    }

    public class UpdateRiskTypeCommandHandler : IRequestHandler<UpdateRiskTypeCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateRiskTypeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateRiskTypeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateRiskType;

            var oldRecord = await _context.RiskTypes.FindAsync(requestDto.Id);
            oldRecord.Status = PersistenceStatus.Deleted;

            var RiskType = new RiskType
            {
                Name = requestDto.Name,
                Code = requestDto.Code,
                Status = PersistenceStatus.Available
            };

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return RiskType.Id;
        }
    }
}
