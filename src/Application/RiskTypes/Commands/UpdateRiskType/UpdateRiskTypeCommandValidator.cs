﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskTypes
{
    class UpdateRiskTypeCommandValidator : AbstractValidator<UpdateRiskTypeCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateRiskTypeCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateRiskType).NotNull().WithMessage("Update body not found").DependentRules(() =>
            {
                RuleFor(v => v.UpdateRiskType.Id).NotEmpty().MustAsync(IsValidId).WithMessage("Invalid Id");
                RuleFor(v => v.UpdateRiskType.Code).NotEmpty();
                RuleFor(v => v.UpdateRiskType.Name).NotEmpty();
            });
        }

        public async Task<bool> IsValidId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.RiskTypes.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}


