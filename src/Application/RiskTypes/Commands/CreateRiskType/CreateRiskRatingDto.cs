﻿namespace Riski.Api.Application.RiskTypes
{
    public class CreateRiskTypeDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
