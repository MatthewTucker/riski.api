﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskTypes
{
    public class CreateRiskTypeCommand : IRequest<Guid>
    {
        public CreateRiskTypeDto CreateRiskType { get; set; }
    }

    public class CreateRiskTypeCommandHandler : IRequestHandler<CreateRiskTypeCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskTypeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateRiskTypeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateRiskType;
            var RiskType = new RiskType
            {
                Name = requestDto.Name,
                Code = requestDto.Code,
                Status = PersistenceStatus.Available
            };

            await _context.RiskTypes.AddAsync(RiskType, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return RiskType.Id;
        }
    }
}
