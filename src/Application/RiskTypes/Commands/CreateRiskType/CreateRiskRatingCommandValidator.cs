﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskTypes
{
    class CreateRiskTypeCommandValidator : AbstractValidator<CreateRiskTypeCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskTypeCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateRiskType).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateRiskType.Code).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("RiskType already exists with this Code");
                    RuleFor(v => v.CreateRiskType.Name).NotEmpty();
                });
        }

        public async Task<bool> IsNotDuplicate(string code, CancellationToken cancellationToken)
        {
            return await _context.RiskTypes.AnyAsync(r => r.Code == code, cancellationToken);
        }
    }
}

