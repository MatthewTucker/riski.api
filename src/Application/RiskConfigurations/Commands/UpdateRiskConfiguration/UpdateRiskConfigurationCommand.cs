﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskConfigurations
{
    public class UpdateRiskConfigurationCommand : IRequest<Guid>
    {
        public UpdateRiskConfigurationDto UpdateRiskConfiguration { get; set; }
    }

    public class UpdateRiskConfigurationCommandHandler : IRequestHandler<UpdateRiskConfigurationCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateRiskConfigurationCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateRiskConfigurationCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateRiskConfiguration;

            var oldRecord = await _context.RiskConfigurations.FindAsync(requestDto.Id);
            oldRecord.Status = PersistenceStatus.Deleted;

            var riskConfiguration = new RiskConfiguration
            {
                Code = requestDto.Code,
                Value = requestDto.Value,
                Description = requestDto.Description,
                Category = requestDto.Category,
                Rating = requestDto.Rating,
                Status = PersistenceStatus.Available
            };

            await _context.RiskConfigurations.AddAsync(riskConfiguration, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return riskConfiguration.Id;
        }
    }
}
