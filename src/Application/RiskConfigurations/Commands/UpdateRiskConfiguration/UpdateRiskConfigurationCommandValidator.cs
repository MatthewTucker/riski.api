﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskConfigurations
{
    class UpdateRiskConfigurationCommandValidator : AbstractValidator<UpdateRiskConfigurationCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateRiskConfigurationCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateRiskConfiguration).NotNull().WithMessage("Create body not found").DependentRules(() =>
            {
                RuleFor(v => v.UpdateRiskConfiguration.Code).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("RiskConfiguration already exists with this Code");
                RuleFor(v => v.UpdateRiskConfiguration.Value).NotEmpty();
                RuleFor(v => v.UpdateRiskConfiguration.Description).NotEmpty();
                RuleFor(v => v.UpdateRiskConfiguration.Category).NotEmpty();
                RuleFor(v => v.UpdateRiskConfiguration.Rating).NotEmpty();
            });
        }

        public async Task<bool> IsNotDuplicate(string code, CancellationToken cancellationToken)
        {
            return await _context.RiskConfigurations.AnyAsync(r => r.Code == code, cancellationToken);
        }
    }
}


