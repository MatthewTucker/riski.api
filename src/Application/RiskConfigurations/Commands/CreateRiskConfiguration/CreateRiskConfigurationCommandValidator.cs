﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskConfigurations
{
    class CreateRiskConfigurationsCommandValidator : AbstractValidator<CreateRiskConfigurationCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskConfigurationsCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateRiskConfiguration).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateRiskConfiguration.Code).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("RiskConfiguration already exists with this Code");
                    RuleFor(v => v.CreateRiskConfiguration.Value).NotEmpty();
                    RuleFor(v => v.CreateRiskConfiguration.Description).NotEmpty();
                    RuleFor(v => v.CreateRiskConfiguration.Category).NotEmpty();
                    RuleFor(v => v.CreateRiskConfiguration.Rating).NotEmpty();
                });
        }

        public async Task<bool> IsNotDuplicate(string code, CancellationToken cancellationToken)
        {
            return await _context.RiskConfigurations.AnyAsync(r => r.Code == code, cancellationToken);
        }
    }
}

