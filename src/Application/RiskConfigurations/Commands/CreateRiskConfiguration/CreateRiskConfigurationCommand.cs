﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskConfigurations
{
    public class CreateRiskConfigurationCommand : IRequest<Guid>
    {
        public CreateRiskConfigurationDto CreateRiskConfiguration { get; set; }
    }

    public class CreateRiskConfigurationCommandHandler : IRequestHandler<CreateRiskConfigurationCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskConfigurationCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateRiskConfigurationCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateRiskConfiguration;
            var RiskConfiguration = new RiskConfiguration
            {
                Code = requestDto.Code,
                Value = requestDto.Value,
                Description = requestDto.Description,
                Category = requestDto.Category,
                Rating = requestDto.Rating,
                Status = PersistenceStatus.Available
            };

            await _context.RiskConfigurations.AddAsync(RiskConfiguration, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return RiskConfiguration.Id;
        }
    }
}
