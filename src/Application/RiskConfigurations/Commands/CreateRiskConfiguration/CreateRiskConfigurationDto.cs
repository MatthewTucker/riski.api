﻿namespace Riski.Api.Application.RiskConfigurations
{
    public class CreateRiskConfigurationDto
    {
        public string Code { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int Rating { get; set; }
    }
}
