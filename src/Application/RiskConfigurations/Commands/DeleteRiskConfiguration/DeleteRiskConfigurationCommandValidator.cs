﻿using FluentValidation;

namespace Riski.Api.Application.RiskConfigurations
{
    class DeleteRiskConfigurationCommandValidator : AbstractValidator<DeleteRiskConfigurationCommand>
    {
        public DeleteRiskConfigurationCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
