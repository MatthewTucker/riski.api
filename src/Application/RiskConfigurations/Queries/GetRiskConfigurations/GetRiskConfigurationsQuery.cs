﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.RiskConfigurations
{
    public class GetRiskConfigurationsQuery : IRequest<List<RiskConfigurationDto>>
    {
    }

    public class GetRiskConfigurationsQueryHandler : IRequestHandler<GetRiskConfigurationsQuery, List<RiskConfigurationDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskConfigurationsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<RiskConfigurationDto>> Handle(GetRiskConfigurationsQuery request, CancellationToken cancellationToken)
        {
            return await _context.RiskConfigurations.Select(r => r.ToRiskConfigurationDto()).ToListAsync(cancellationToken);
        }
    }
}
