﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.RiskConfigurations
{
    public class GetRiskConfigurationQuery : IRequest<RiskConfigurationDto>
    {
        public Guid Id { get; set; }
    }

    public class GetRiskConfigurationQueryHandler : IRequestHandler<GetRiskConfigurationQuery, RiskConfigurationDto>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskConfigurationQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<RiskConfigurationDto> Handle(GetRiskConfigurationQuery request, CancellationToken cancellationToken)
        {
            var riskConfiguration = await _context.RiskConfigurations.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return riskConfiguration.ToRiskConfigurationDto();
        }
    }
}
