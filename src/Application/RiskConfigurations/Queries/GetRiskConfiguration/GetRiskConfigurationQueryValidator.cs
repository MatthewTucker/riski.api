﻿using FluentValidation;

namespace Riski.Api.Application.RiskConfigurations
{
    public class GetRiskConfigurationQueryValidator : AbstractValidator<GetRiskConfigurationQuery>
    {
        public GetRiskConfigurationQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
