﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Questions
{
    class DeleteQuestionCommandValidator : AbstractValidator<DeleteQuestionCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteQuestionCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Id).NotNull().MustAsync(DoesExist).WithMessage("Id not found");
        }

        public async Task<bool> DoesExist(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Questions.AnyAsync(q => q.Id == id, cancellationToken);
        }
    }
}
