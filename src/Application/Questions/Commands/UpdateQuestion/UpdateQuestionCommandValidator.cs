﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Questions
{
    class UpdateQuestionCommandValidator : AbstractValidator<UpdateQuestionCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateQuestionCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateQuestionDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UpdateQuestionDto.Id).NotEmpty().MustAsync(DoesExist).WithMessage("Invalid Id");
                    RuleFor(v => v.UpdateQuestionDto.Description).NotEmpty();
                    RuleFor(v => v.UpdateQuestionDto.DisplayType).NotEmpty();
                    RuleFor(v => v.UpdateQuestionDto.PageName).NotEmpty();
                    RuleFor(v => v.UpdateQuestionDto.ReadOnly).NotNull();
                    RuleFor(v => v.UpdateQuestionDto.FormPageId).NotEmpty().MustAsync(IsValidFormPage).WithMessage("Invalid FormPageId");
                    RuleFor(v => v.UpdateQuestionDto.Mandatory).NotEmpty();
                    RuleFor(v => v.UpdateQuestionDto.Status).NotEmpty();
                    RuleFor(v => v.UpdateQuestionDto.Position).NotNull();
                });
        }

        public async Task<bool> IsValidFormPage(Guid id, CancellationToken cancellationToken)
        {
            return await _context.FormPages.AnyAsync(fp => fp.Id == id, cancellationToken);
        }

        public async Task<bool> DoesExist(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Questions.AnyAsync(fp => fp.Id == id, cancellationToken);
        }
    }
}

