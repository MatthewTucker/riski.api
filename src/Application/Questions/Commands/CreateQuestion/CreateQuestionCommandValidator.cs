﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Questions
{
    class CreateQuestionCommandValidator : AbstractValidator<CreateQuestionCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateQuestionCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateQuestionDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateQuestionDto.Description).NotEmpty();
                    RuleFor(v => v.CreateQuestionDto.DisplayType).NotEmpty();
                    RuleFor(v => v.CreateQuestionDto.ReadOnly).NotNull();
                    RuleFor(v => v.CreateQuestionDto.FormPageId).NotEmpty().MustAsync(IsValidFormPage).WithMessage("Invalid FormPageId");
                    RuleFor(v => v.CreateQuestionDto.Mandatory).NotEmpty();
                    RuleFor(v => v.CreateQuestionDto.Position).NotNull();
                });
        }

        public async Task<bool> IsValidFormPage(Guid id, CancellationToken cancellationToken)
        {
            return await _context.FormPages.AnyAsync(fp => fp.Id == id, cancellationToken);
        }
    }
}

