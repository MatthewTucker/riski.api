﻿ using System;

namespace Riski.Api.Application.Questions
{
    public class CreateQuestionDto
    {
        public string Description { get; set; }
        public string DisplayType { get; set; }
        public bool ReadOnly { get; set; }
        public string LookupKey { get; set; }
        public bool Mandatory { get; set; }
        public Guid FormPageId { get; set; }
        public int Position { get; set; }
    }
}
