﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;
using Microsoft.EntityFrameworkCore;
using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Questions
{
    public class CreateQuestionCommand : IRequest<Guid>
    {
        public CreateQuestionDto CreateQuestionDto { get; set; }
    }

    public class CreateQuestionCommandHandler : IRequestHandler<CreateQuestionCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateQuestionCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateQuestionCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateQuestionDto;

            var formPage = await _context.FormPages.Include(fp => fp.Questions).SingleAsync(fp => fp.Id == requestDto.FormPageId, cancellationToken);

            var question = new Question
            {
                Description = requestDto.Description,
                DisplayType = requestDto.DisplayType,
                ReadOnly = requestDto.ReadOnly,
                LookupKey = requestDto.LookupKey,
                Mandatory = requestDto.Mandatory,
                FormPage = formPage,
                Status = PersistenceStatus.Available
            };

            UpdateRelatedPositions(formPage, question);

            await _context.Questions.AddAsync(question, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return question.Id;
        }

        internal void UpdateRelatedPositions(FormPage formPage, Question updatedQuestion)
        {
            var newPosition = updatedQuestion.Position;

            //var questionsToUpdate = formPage.Questions.Where(q => q.Id != updatedQuestion.Id
            //                                                //&& q.Position >= newPosition
            //                                                && q.Status == PersistenceStatus.Available)
            //                                            .OrderBy(fpq => fpq.Position)
            //                                            .ToList();

            //if (!questionsToUpdate.Any())
            //{ 
            //    return;
            //}

            //for (var i = 0; i < questionsToUpdate.Count; i++)
            //{
            //    questionsToUpdate[i].Position = newPosition + i + 1;
            //}

            //var questions = formPage.Questions.OrderBy(fpq => fpq.Position).ToList();

            //for (var i = 0; i < questions.Count; i++)
            //{
            //    questions[i].Position = i + 1;
            //}

            var questions = formPage.Questions.Where(q => q.Id != updatedQuestion.Id && q.Status == PersistenceStatus.Available).ToList();

            if (!questions.Any())
            {
                return;
            }

            foreach (var question in questions)
            {
                if (question.Position >= newPosition)
                {
                    question.Position++;
                }
            }

            questions = questions.OrderBy(q => q.Position).ToList();

            for (var i = 0; i < questions.Count; i++)
            {
                questions[i].Position = i + 1;
            }
        }
    }
}
