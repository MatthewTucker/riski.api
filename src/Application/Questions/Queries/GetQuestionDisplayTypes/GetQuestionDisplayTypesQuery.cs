﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Questions.Queries.GetQuestionDisplayTypes
{
    public class GetQuestionDisplayTypesQuery : IRequest<List<QuestionDisplayTypeDto>>
    {
    }

    public class GetQuestionsQueryHandler : IRequestHandler<GetQuestionDisplayTypesQuery, List<QuestionDisplayTypeDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetQuestionsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<QuestionDisplayTypeDto>> Handle(GetQuestionDisplayTypesQuery request, CancellationToken cancellationToken)
        {
            var displayTypes = await _context.QuestionDisplayTypes.ToListAsync(cancellationToken);

            return displayTypes.Select(dt => dt.ToQuestionDisplayTypeDto()).ToList();
        }
    }
}
