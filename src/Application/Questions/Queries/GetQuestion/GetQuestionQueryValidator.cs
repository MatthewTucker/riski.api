﻿using FluentValidation;

namespace Riski.Api.Application.FormPages
{
    public class GetQuestionQueryValidator : AbstractValidator<GetFormPageQuery>
    {
        public GetQuestionQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
