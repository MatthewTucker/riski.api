﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Questions
{
    public class GetQuestionQuery : IRequest<QuestionDto>
    {
        public Guid Id { get; set; }
    }

    public class GetQuestionQueryHandler : IRequestHandler<GetQuestionQuery, QuestionDto>
    {
        private readonly IApplicationDbContext _context;

        public GetQuestionQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<QuestionDto> Handle(GetQuestionQuery request, CancellationToken cancellationToken)
        {
            var question = await _context.Questions
                .Include(q => q.FormPage)
                    .ThenInclude(fp => fp.Template)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return question.ToQuestionDto();
        }
    }
}
