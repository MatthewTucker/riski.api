﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Questions
{
    public class GetQuestionsQuery : IRequest<List<QuestionDto>>
    {
    }

    public class GetQuestionsQueryHandler : IRequestHandler<GetQuestionsQuery, List<QuestionDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetQuestionsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<QuestionDto>> Handle(GetQuestionsQuery request, CancellationToken cancellationToken)
        {
            return await _context.Questions
                .Include(q => q.FormPage)
                    .ThenInclude(fp => fp.Template)
                .Where(q => q.Status == PersistenceStatus.Available)
                .Select(r => r.ToQuestionDto())
                .ToListAsync(cancellationToken);
        }
    }
}
