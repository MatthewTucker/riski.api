﻿using System;

namespace Riski.Api.Application.RiskRatings
{
    public class UpdateRiskRatingDto
    {
        public Guid Id { get; set; }
        public int Value { get; set; }
        public string Display { get; set; }
        public string Message { get; set; }
        public string Colour { get; set; }
        public string RiskCodes { get; set; }
        public bool ShouldNotify { get; set; }
    }
}
