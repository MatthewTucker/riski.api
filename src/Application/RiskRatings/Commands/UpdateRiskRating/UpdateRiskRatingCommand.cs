﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskRatings
{
    public class UpdateRiskRatingCommand : IRequest<Guid>
    {
        public UpdateRiskRatingDto UpdateRiskRating { get; set; }
    }

    public class UpdateRiskRatingCommandHandler : IRequestHandler<UpdateRiskRatingCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateRiskRatingCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateRiskRatingCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateRiskRating;

            var oldRecord = await _context.RiskRatings.FindAsync(requestDto.Id);
            oldRecord.Status = PersistenceStatus.Deleted;

            var riskRating = new RiskRating
            {
                Value = requestDto.Value,
                Display = requestDto.Display,
                Message = requestDto.Message,
                Colour = requestDto.Colour,
                RiskCodes = requestDto.RiskCodes,
                ShouldNotify = requestDto.ShouldNotify,
                Status = PersistenceStatus.Available
            };

            await _context.RiskRatings.AddAsync(riskRating);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return riskRating.Id;
        }
    }
}
