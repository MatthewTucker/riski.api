﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskRatings
{
    class UpdateRiskRatingCommandValidator : AbstractValidator<UpdateRiskRatingCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateRiskRatingCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateRiskRating).NotNull().WithMessage("Create body not found").DependentRules(() =>
            {
                RuleFor(v => v.UpdateRiskRating.Id).NotEmpty().MustAsync(IsValidId).WithMessage("Invalid Id");
                RuleFor(v => v.UpdateRiskRating.Value).NotEmpty();
                RuleFor(v => v.UpdateRiskRating.Display).NotEmpty();
                RuleFor(v => v.UpdateRiskRating.Message).NotEmpty();
                RuleFor(v => v.UpdateRiskRating.Colour).NotEmpty();
                RuleFor(v => v.UpdateRiskRating.RiskCodes).NotEmpty();
                RuleFor(v => v.UpdateRiskRating.ShouldNotify).NotEmpty();
            });
        }

        public async Task<bool> IsValidId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.RiskRatings.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}


