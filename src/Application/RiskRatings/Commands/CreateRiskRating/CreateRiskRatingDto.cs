﻿namespace Riski.Api.Application.RiskRatings
{
    public class CreateRiskRatingDto
    {
        public int Value { get; set; }
        public string Display { get; set; }
        public string Message { get; set; }
        public string Colour { get; set; }
        public string RiskCodes { get; set; }
        public bool ShouldNotify { get; set; }
    }
}
