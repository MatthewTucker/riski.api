﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskRatings
{
    class CreateRiskRatingCommandValidator : AbstractValidator<CreateRiskRatingCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskRatingCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateRiskRating).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateRiskRating.Value).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("RiskRating already exists with this Value");
                    RuleFor(v => v.CreateRiskRating.Display).NotEmpty();
                    RuleFor(v => v.CreateRiskRating.Message).NotEmpty();
                    RuleFor(v => v.CreateRiskRating.Colour).NotEmpty();
                    RuleFor(v => v.CreateRiskRating.RiskCodes).NotEmpty();
                    RuleFor(v => v.CreateRiskRating.ShouldNotify).NotEmpty();
                });
        }

        public async Task<bool> IsNotDuplicate(int value, CancellationToken cancellationToken)
        {
            return await _context.RiskRatings.AnyAsync(r => r.Value == value, cancellationToken);
        }
    }
}

