﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskRatings
{
    public class CreateRiskRatingCommand : IRequest<Guid>
    {
        public CreateRiskRatingDto CreateRiskRating { get; set; }
    }

    public class CreateRiskRatingCommandHandler : IRequestHandler<CreateRiskRatingCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskRatingCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateRiskRatingCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateRiskRating;
            var RiskRating = new RiskRating
            {
                Value = requestDto.Value,
                Display = requestDto.Display,
                Message = requestDto.Message,
                Colour = requestDto.Colour,
                RiskCodes = requestDto.RiskCodes,
                ShouldNotify = requestDto.ShouldNotify,
                Status = PersistenceStatus.Available
            };

            await _context.RiskRatings.AddAsync(RiskRating, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return RiskRating.Id;
        }
    }
}
