﻿using FluentValidation;

namespace Riski.Api.Application.RiskRatings
{
    class DeleteRiskRatingCommandValidator : AbstractValidator<DeleteRiskRatingCommand>
    {
        public DeleteRiskRatingCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
