﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskRatings
{
    public class DeleteRiskRatingCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteRiskRatingCommandHandler : IRequestHandler<DeleteRiskRatingCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteRiskRatingCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteRiskRatingCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var record = await _context.RiskRatings.FindAsync(request.Id);

            record.Status = PersistenceStatus.Deleted;

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
