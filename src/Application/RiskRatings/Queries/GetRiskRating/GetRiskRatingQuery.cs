﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.RiskRatings
{
    public class GetRiskRatingQuery : IRequest<RiskRatingDto>
    {
        public Guid Id { get; set; }
    }

    public class GetRiskRatingQueryHandler : IRequestHandler<GetRiskRatingQuery, RiskRatingDto>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskRatingQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<RiskRatingDto> Handle(GetRiskRatingQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.RiskRatings.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return record.ToRiskRatingDto();
        }
    }
}
