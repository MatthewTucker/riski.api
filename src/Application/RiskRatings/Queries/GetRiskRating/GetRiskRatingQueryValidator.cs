﻿using FluentValidation;

namespace Riski.Api.Application.RiskRatings
{
    public class GetRiskRatingQueryValidator : AbstractValidator<GetRiskRatingQuery>
    {
        public GetRiskRatingQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
