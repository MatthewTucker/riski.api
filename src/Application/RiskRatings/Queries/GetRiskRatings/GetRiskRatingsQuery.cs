﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.RiskRatings
{
    public class GetRiskRatingsQuery : IRequest<List<RiskRatingDto>>
    {
    }

    public class GetRiskRatingsQueryHandler : IRequestHandler<GetRiskRatingsQuery, List<RiskRatingDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskRatingsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<RiskRatingDto>> Handle(GetRiskRatingsQuery request, CancellationToken cancellationToken)
        {
            return await _context.RiskRatings.Select(r => r.ToRiskRatingDto()).ToListAsync(cancellationToken);
        }
    }
}
