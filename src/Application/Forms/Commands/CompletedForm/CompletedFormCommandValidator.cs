﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Forms
{
    class CompletedFormCommandValidator : AbstractValidator<CompletedFormCommand>
    {

        private readonly IApplicationDbContext _context;

        public CompletedFormCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Id).NotEmpty();
            RuleFor(v => v.Username).NotEmpty();
            RuleFor(v => v).MustAsync(IsNotComplete).WithMessage($"This form has already been completed");
            RuleFor(v => v).MustAsync(IsValidForm).WithMessage("Invalid Id and Username");
        }

        private Task<bool> IsValidForm(CompletedFormCommand command, CancellationToken cancellationToken)
        {
            return _context.Forms.AnyAsync(f => f.Id == command.Id && f.User.Username == command.Username, cancellationToken: cancellationToken);
        }

        private Task<bool> IsNotComplete(CompletedFormCommand command, CancellationToken cancellationToken)
        {
            return _context.Forms.AnyAsync(f => f.Id == command.Id && f.DateTimeCompleted != null, cancellationToken: cancellationToken);
        }
    }
}

