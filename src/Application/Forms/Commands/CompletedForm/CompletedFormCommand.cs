﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Forms
{
    public class CompletedFormCommand : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
    }

    public class CompletedFormCommandHandler : IRequestHandler<CompletedFormCommand, Unit>
    {

        private readonly IApplicationDbContext _context;
        private readonly IDateTime _dateTime;

        public CompletedFormCommandHandler(IApplicationDbContext context, IDateTime dateTime)
        {
            _context = context;
            _dateTime = dateTime;
        }

        public async Task<Unit> Handle(CompletedFormCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var form = await _context.Forms.FindAsync(request.Id);

            var dateTimeNow = _dateTime.Now;

            form.DateTimeCompleted = dateTimeNow;

            if (form.SubForms?.Any() ?? false)
            {
                foreach (var subForm in form.SubForms)
                {
                    subForm.DateTimeCompleted = dateTimeNow;
                }
            }

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
