﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Forms
{
    class CreateFormCommandValidator : AbstractValidator<CreateFormCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateFormCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateFormDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateFormDto.UserId).NotEmpty().MustAsync(IsValidUserIdAsync).WithMessage("Invalid UserId");
                    RuleFor(v => v.CreateFormDto.Answers).NotEmpty();
                    RuleFor(v => v.CreateFormDto.SiteId).NotEmpty().MustAsync(IsValidSiteIdAsync).WithMessage("Invalid SiteId");
                    RuleFor(v => v.CreateFormDto.TemplateId).NotEmpty().MustAsync(IsValidTemplateIdAsync).WithMessage("Invalid TemplateId");
                    RuleFor(v => v.CreateFormDto.Risks).NotEmpty();
                    RuleFor(v => v.CreateFormDto.ProxyUserIds).MustAsync(AreValidUsersAsync).WithMessage("Invalid ProxyUserIds");
                    RuleFor(v => v.CreateFormDto.Signatures).NotEmpty();
                    RuleFor(v => v.CreateFormDto.ManagerApprovalStatus).NotEmpty();
                    RuleFor(v => v.CreateFormDto.DateTimeSubmitted).NotEmpty();
                });
        }

        private Task<bool> IsValidUserIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _context.Users.AnyAsync(u => u.Id == id, cancellationToken);
        }

        private Task<bool> IsValidSiteIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _context.Sites.AnyAsync(u => u.Id == id, cancellationToken);
        }

        private Task<bool> IsValidTemplateIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _context.Templates.AnyAsync(u => u.Id == id, cancellationToken);
        }

        private async Task<bool> AreValidUsersAsync(List<Guid> ids, CancellationToken cancellationToken)
        {
            var allUserIds = await _context.Users.Select(u => u.Id).ToListAsync();
            return ids.All(id => allUserIds.Contains(id));
        }
    }
}

