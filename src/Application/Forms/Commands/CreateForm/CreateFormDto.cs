﻿using System;
using System.Collections.Generic;

using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Forms
{
    public class CreateFormDto
    {
        public Guid UserId { get; set; }
        public List<AnswerDto> Answers { get; set; }
        public Guid SiteId { get; set; }
        public Guid TemplateId { get; set; }
        public List<RiskDto> Risks { get; set; }
        public List<Guid> ProxyUserIds { get; set; }
        public List<SignatureDto> Signatures { get; set; }
        public string ManagerApprovalStatus { get; set; }
        public string DateTimeSubmitted { get; set; }
        public List<GuestUserDto> GuestUsers { get; set; }
        public List<CreateFormDto> SubForms { get; set; }
    }
}
