﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Forms
{
    public class CreateFormCommand : IRequest<Guid>
    {
        public CreateFormDto CreateFormDto { get; set; }
    }

    public class CreateFormCommandHandler : IRequestHandler<CreateFormCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateFormCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateFormCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateFormDto;

            var user = await _context.Users.FindAsync(requestDto.UserId);
            var form = await GenerateForm(user, requestDto);

            await _context.Forms.AddAsync(form, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return form.Id;
        }

        private async Task<Form> GenerateForm(User user, CreateFormDto requestDto)
        {
            var form = new Form
            {
                User = user,
                Signatures = new List<Signature>(),
                Answers = new List<Answer>(),
                GuestUsers = new List<GuestUser>(),
                SubForms = new List<Form>(),
                Risks = new List<Risk>(),
                DateTimeSubmitted = DateTime.UtcNow
            };

            var template = await _context.Templates.FindAsync(requestDto.TemplateId);
            form.Template = template;
            form.Name = template?.Name;

            form.Answers = await GenerateAnswersAsync(requestDto.Answers);

            form.Site = await _context.Sites.FindAsync(requestDto.SiteId);

            form.ManagerApprovalStatus = requestDto.ManagerApprovalStatus;

            if (requestDto.ProxyUserIds.Any())
            {
                form.ProxyWorkers = await _context.Users.Where(u => requestDto.ProxyUserIds.Contains(u.Id)).ToListAsync();
            }

            if (requestDto.GuestUsers.Any())
            {
                foreach (var guestUserDto in requestDto.GuestUsers)
                {
                    var guestUser = new GuestUser
                    {
                        FirstName = guestUserDto.FirstName,
                        LastName = guestUserDto.LastName,
                        Email = guestUserDto.Email,
                        Mobile = guestUserDto.Mobile,
                        Signature = new Signature
                        {
                            UserType = "Guest",
                            FirstName = guestUserDto.FirstName,
                            LastName = guestUserDto.LastName,
                            Base64String = guestUserDto.Signature.Value
                        }
                    };

                    form.GuestUsers.Add(guestUser);
                }
            }

            foreach (var signatureDto in requestDto.Signatures)
            {
                var signature = new Signature
                {
                    UserId = signatureDto.UserId,
                    UserType = "ApplicationUser",
                    FirstName = signatureDto.FirstName,
                    LastName = signatureDto.LastName,
                    Base64String = signatureDto.Value
                };

                form.Signatures.Add(signature);
            }

            if (requestDto.Risks.Any())
            {
                foreach (var riskDto in requestDto.Risks)
                {
                    var risk = new Risk
                    {
                        Description = riskDto.Description,
                        Code = riskDto.Code,
                        ConsequenceDescription = riskDto.ConsequenceDesc,
                        MitigationDescription = riskDto.MitigationDesc,
                        PostMitigationCode = riskDto.PostMitigationCode,
                        Timestamp = riskDto.Timestamp,
                        ApplicationUser = user,
                        Site = form.Site,
                        RiskStatus = "Identified"
                    };

                    risk.Type = await _context.RiskTypes.FindAsync(riskDto.Type.Id);
                    risk.Likelihood = await _context.RiskConfigurations.FindAsync(riskDto.Likelihood.Id);
                    risk.Consequence = await _context.RiskConfigurations.FindAsync(riskDto.Consequence.Id);
                    risk.PostMigitationLikelihood = await _context.RiskConfigurations.FindAsync(riskDto.PostMitigationLikelihood.Id);
                    risk.PostMitigationConsequence = await _context.RiskConfigurations.FindAsync(riskDto.PostMitigationConsequence.Id);

                    form.Risks.Add(risk);
                }
            }

            if (requestDto.SubForms == null) return form;

            foreach (var subFormDto in requestDto.SubForms)
            {
                var subForm = await GenerateForm(user, subFormDto);
                subForm.ParentFormId = form.Id;
                form.SubForms.Add(subForm);
            }

            return form;
        }

        private async Task<List<Answer>> GenerateAnswersAsync(List<AnswerDto> answerDtos)
        {
            var answers = new List<Answer>();

            foreach (var answerDto in answerDtos)
            {
                var answer = new Answer
                {
                    ApplicationUserId = answerDto.WorkerId,
                    Value = answerDto.Value,
                    Question = await _context.Questions.FindAsync(answerDto.QuestionId),
                };

                answers.Add(answer);
            }

            return answers;
        }
    }
}
