﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Forms
{
    public class GetFormsQuery : IRequest<List<FormDto>>
    {
        public int? Skip { get; set; }
        public int? Take { get; set; }
    }

    public class GetFormsQueryHandler : IRequestHandler<GetFormsQuery, List<FormDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetFormsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<FormDto>> Handle(GetFormsQuery request, CancellationToken cancellationToken)
        {
            var formsQuery = _context.Forms
                .Include(f => f.User)
                .Include(f => f.Answers)
                    .ThenInclude(a => a.Question)
                .Include(f => f.Site)
                .Include(f => f.Template)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Likelihood)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Consequence)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Type)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.PostMigitationLikelihood)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.PostMitigationConsequence)
                .Include(f => f.Signatures)
                .Include(f => f.ProxyWorkers)
                .Include(f => f.GuestUsers)
                .Include(f => f.SubForms)
                .AsQueryable();

            if(request.Skip != null)
            {
                formsQuery = formsQuery.Skip(request.Skip.Value);
            }

            if(request.Take != null)
            {
                formsQuery = formsQuery.Take(request.Take.Value);
            }

            var forms = await formsQuery.ToListAsync(cancellationToken);

            return forms.Select(r => r.ToFormDto()).ToList();
        }
    }
}
