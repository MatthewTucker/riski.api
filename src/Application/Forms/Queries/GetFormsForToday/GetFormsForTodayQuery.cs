﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Forms
{
    public class GetFormsForTodayQuery : IRequest<List<FormDto>>
    {
    }

    public class GetFormsForTodayQueryHandler : IRequestHandler<GetFormsForTodayQuery, List<FormDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IDateTime _dateTime;

        public GetFormsForTodayQueryHandler(IApplicationDbContext context, IDateTime dateTime)
        {
            _context = context;
            _dateTime = dateTime;
        }

        public async Task<List<FormDto>> Handle(GetFormsForTodayQuery request, CancellationToken cancellationToken)
        {
            return await _context.Forms
                .Include(f => f.User)
                .Include(f => f.Answers)
                    .ThenInclude(a => a.Question)
                .Include(f => f.Site)
                .Include(f => f.Template)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Likelihood)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Consequence)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Type)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.PostMigitationLikelihood)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.PostMitigationConsequence)
                .Include(f => f.Signatures)
                .Include(f => f.ProxyWorkers)
                .Include(f => f.GuestUsers)
                .Include(f => f.SubForms)
                .Where(f => f.DateTimeSubmitted.Date == _dateTime.Now)
                .Select(r => r.ToFormDto())
                .ToListAsync(cancellationToken);
        }
    }
}
