﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Forms
{
    public class GetFormMetasQuery : IRequest<List<FormMetaDto>>
    {
        public string Username { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class GetFormMetasQueryHandler : IRequestHandler<GetFormMetasQuery, List<FormMetaDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetFormMetasQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<FormMetaDto>> Handle(GetFormMetasQuery request, CancellationToken cancellationToken)
        {
            var forms = await _context.Forms.Where(f => f.User.Username == request.Username
                                                        && ((request.StartDate.Date <= f.DateTimeSubmitted.Date
                                                        && request.EndDate.Date >= f.DateTimeSubmitted.Date)))
                                    .Include(f => f.Answers).ThenInclude(a => a.Question)
                                    .Include(f => f.Site)
                                    .Include(f => f.Template)
                                    .ToListAsync();

            return forms.Select(f => f.ToFormMetaDto()).ToList();
        }
    }
}
