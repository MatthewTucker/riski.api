﻿using FluentValidation;

namespace Riski.Api.Application.Forms
{
    public class GetFormMetasQueryValidator : AbstractValidator<GetFormMetasQuery>
    {
        public GetFormMetasQueryValidator()
        {
            RuleFor(x => x.Username).NotEmpty();
            RuleFor(x => x.StartDate).NotEmpty();
            RuleFor(x => x.EndDate).NotEmpty();
        }
    }
}
