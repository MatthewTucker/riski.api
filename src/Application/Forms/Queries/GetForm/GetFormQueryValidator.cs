﻿using FluentValidation;

namespace Riski.Api.Application.Forms
{
    public class GetFormQueryValidator : AbstractValidator<GetFormQuery>
    {
        public GetFormQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
