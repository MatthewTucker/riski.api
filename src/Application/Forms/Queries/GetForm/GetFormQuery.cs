﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Forms
{
    public class GetFormQuery : IRequest<FormDto>
    {
        public Guid Id { get; set; }
    }

    public class GetFormQueryHandler : IRequestHandler<GetFormQuery, FormDto>
    {
        private readonly IApplicationDbContext _context;

        public GetFormQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<FormDto> Handle(GetFormQuery request, CancellationToken cancellationToken)
        {
            var form = await _context.Forms
                .Include(f => f.User)
                .Include(f => f.Answers)
                    .ThenInclude(a => a.Question)
                .Include(f => f.Site)
                .Include(f => f.Template)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Likelihood)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Consequence)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.Type)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.PostMigitationLikelihood)
                .Include(f => f.Risks)
                    .ThenInclude(r => r.PostMitigationConsequence)
                .Include(f => f.Signatures)
                .Include(f => f.ProxyWorkers)
                .Include(f => f.GuestUsers)
                .Include(f => f.SubForms)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return form.ToFormDto();
        }
    }
}
