﻿using FluentValidation;

namespace Riski.Api.Application.Answers.Queries.GetAnswer
{
    public class GetAnswerQueryValidator : AbstractValidator<GetAnswerQuery>
    {
        public GetAnswerQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
