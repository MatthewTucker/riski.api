﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Answers
{
    public class GetAnswerQuery : IRequest<AnswerDto>
    {
        public Guid Id { get; set; }
    }

    public class GetAnswerQueryHandler : IRequestHandler<GetAnswerQuery, AnswerDto>
    {
        private readonly IApplicationDbContext _context;

        public GetAnswerQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<AnswerDto> Handle(GetAnswerQuery request, CancellationToken cancellationToken)
        {
            var answer = await _context.Answers.Include(a => a.Question).SingleOrDefaultAsync(a => a.Id == request.Id);

            return answer.ToAnswerDto();
        }
    }
}
