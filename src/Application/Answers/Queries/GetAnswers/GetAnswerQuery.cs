﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Answers
{
    public class GetAnswersQuery : IRequest<List<AnswerDto>>
    {
    }

    public class GetAnswersQueryHandler : IRequestHandler<GetAnswersQuery, List<AnswerDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetAnswersQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<AnswerDto>> Handle(GetAnswersQuery request, CancellationToken cancellationToken)
        {
            return await _context.Answers.Include(a => a.Question).Select(a => a.ToAnswerDto()).ToListAsync(cancellationToken);
        }
    }
}
