﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    public class DeleteReferenceDocumentTypeCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteReferenceDocumentTypeCommandHandler : IRequestHandler<DeleteReferenceDocumentTypeCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteReferenceDocumentTypeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteReferenceDocumentTypeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var record = await _context.ReferenceDocumentTypes.FindAsync(request.Id);

            _context.ReferenceDocumentTypes.Remove(record);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
