﻿using FluentValidation;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    class DeleteReferenceDocumentTypeCommandValidator : AbstractValidator<DeleteReferenceDocumentTypeCommand>
    {
        public DeleteReferenceDocumentTypeCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
