﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    class CreateReferenceDocumentTypesCommandValidator : AbstractValidator<CreateReferenceDocumentTypeCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateReferenceDocumentTypesCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateReferenceDocumentType)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateReferenceDocumentType.Description).NotEmpty();
                    RuleFor(v => v.CreateReferenceDocumentType.Code).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("ReferenceDocumentType already exists with this Code");
                });
        }

        public async Task<bool> IsNotDuplicate(string code, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocumentTypes.AnyAsync(r => r.Code == code, cancellationToken);
        }
    }
}

