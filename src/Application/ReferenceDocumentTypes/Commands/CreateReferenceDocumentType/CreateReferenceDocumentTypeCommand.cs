﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    public class CreateReferenceDocumentTypeCommand : IRequest<Guid>
    {
        public CreateReferenceDocumentTypeDto CreateReferenceDocumentType { get; set; }
    }

    public class CreateReferenceDocumentTypeCommandHandler : IRequestHandler<CreateReferenceDocumentTypeCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateReferenceDocumentTypeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateReferenceDocumentTypeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateReferenceDocumentType;
            var referenceDocumentType = new ReferenceDocumentType
            {
                Description = requestDto.Description,
                Code = requestDto.Code,
                DisplayImage = requestDto.DisplayImage,
                SelectedDisplayImage = requestDto.SelectedDisplayImage,
                Status = PersistenceStatus.Available
            };

            await _context.ReferenceDocumentTypes.AddAsync(referenceDocumentType, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return referenceDocumentType.Id;
        }
    }
}
