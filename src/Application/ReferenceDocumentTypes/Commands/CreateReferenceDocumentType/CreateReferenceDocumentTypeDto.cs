﻿namespace Riski.Api.Application.ReferenceDocumentTypes
{
    public class CreateReferenceDocumentTypeDto
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public byte[] DisplayImage { get; set; }
        public byte[] SelectedDisplayImage { get; set; }
        public string Status { get; set; }
    }
}
