﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    class UpdateReferenceDocumentTypeCommandValidator : AbstractValidator<UpdateReferenceDocumentTypeCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateReferenceDocumentTypeCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateReferenceDocumentType)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UpdateReferenceDocumentType.Id).NotEmpty().MustAsync(IsValidId).WithMessage("Invalid Id");
                    RuleFor(v => v.UpdateReferenceDocumentType.Description).NotEmpty();
                    RuleFor(v => v.UpdateReferenceDocumentType.Code).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("ReferenceDocumentType already exists with this Code");
                });
        }

        public async Task<bool> IsNotDuplicate(string code, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocumentTypes.AnyAsync(r => r.Code == code, cancellationToken);
        }

        public async Task<bool> IsValidId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocumentTypes.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}

