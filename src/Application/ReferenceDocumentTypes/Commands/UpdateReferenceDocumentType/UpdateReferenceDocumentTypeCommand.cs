﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    public class UpdateReferenceDocumentTypeCommand : IRequest<Guid>
    {
        public UpdateReferenceDocumentTypeDto UpdateReferenceDocumentType { get; set; }
    }

    public class UpdateReferenceDocumentTypeCommandHandler : IRequestHandler<UpdateReferenceDocumentTypeCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateReferenceDocumentTypeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateReferenceDocumentTypeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateReferenceDocumentType;

            var referenceDocumentType = await _context.ReferenceDocumentTypes.FindAsync(requestDto.Id);
            referenceDocumentType.Code = requestDto.Code;
            referenceDocumentType.Description = requestDto.Description;
            referenceDocumentType.DisplayImage = requestDto.DisplayImage;
            referenceDocumentType.SelectedDisplayImage = requestDto.SelectedDisplayImage;

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return referenceDocumentType.Id;
        }
    }
}
