﻿using FluentValidation;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    public class GetReferenceDocumentTypeQueryValidator : AbstractValidator<GetReferenceDocumentTypeQuery>
    {
        public GetReferenceDocumentTypeQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
