﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.ReferenceDocumentTypes
{
    public class GetReferenceDocumentTypeQuery : IRequest<ReferenceDocumentTypeDto>
    {
        public Guid Id { get; set; }
    }

    public class GetReferenceDocumentTypeQueryHandler : IRequestHandler<GetReferenceDocumentTypeQuery, ReferenceDocumentTypeDto>
    {
        private readonly IApplicationDbContext _context;

        public GetReferenceDocumentTypeQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReferenceDocumentTypeDto> Handle(GetReferenceDocumentTypeQuery request, CancellationToken cancellationToken)
        {
            var referenceDocumentType = await _context.ReferenceDocumentTypes.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return referenceDocumentType.ToReferenceDocumentTypeDto();
        }
    }
}
