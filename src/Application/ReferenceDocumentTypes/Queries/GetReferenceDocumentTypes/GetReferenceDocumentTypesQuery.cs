﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.ReferenceDocumentTypes
{
    public class GetReferenceDocumentTypesQuery : IRequest<List<ReferenceDocumentTypeDto>>
    {
    }

    public class GetReferenceDocumentTypesQueryHandler : IRequestHandler<GetReferenceDocumentTypesQuery, List<ReferenceDocumentTypeDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetReferenceDocumentTypesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<ReferenceDocumentTypeDto>> Handle(GetReferenceDocumentTypesQuery request, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocumentTypes.Select(r => r.ToReferenceDocumentTypeDto()).ToListAsync(cancellationToken);
        }
    }
}
