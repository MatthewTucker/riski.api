﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.ReferenceDocuments
{
    public class GetReferenceDocumentsQuery : IRequest<List<ReferenceDocumentDto>>
    {
    }

    public class GetReferenceDocumentsQueryHandler : IRequestHandler<GetReferenceDocumentsQuery, List<ReferenceDocumentDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetReferenceDocumentsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<ReferenceDocumentDto>> Handle(GetReferenceDocumentsQuery request, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocuments
                .Include(rd => rd.Site)
                .Include(rd => rd.WorkTask)
                .Include(rd => rd.ReferenceDocumentType)
                .Select(r => r.ToReferenceDocumentDto()).ToListAsync();
        }
    }
}
