﻿using FluentValidation;

namespace Riski.Api.Application.ReferenceDocuments
{
    public class GetReferenceDocumentQueryValidator : AbstractValidator<GetReferenceDocumentQuery>
    {
        public GetReferenceDocumentQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
