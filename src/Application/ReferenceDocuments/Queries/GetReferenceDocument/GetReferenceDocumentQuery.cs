﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.ReferenceDocuments
{
    public class GetReferenceDocumentQuery : IRequest<ReferenceDocumentDto>
    {
        public Guid Id { get; set; }
    }

    public class GetReferenceDocumentQueryHandler : IRequestHandler<GetReferenceDocumentQuery, ReferenceDocumentDto>
    {
        private readonly IApplicationDbContext _context;

        public GetReferenceDocumentQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ReferenceDocumentDto> Handle(GetReferenceDocumentQuery request, CancellationToken cancellationToken)
        {
            var referenceDocument = await _context.ReferenceDocuments
                .Include(rd => rd.Site)
                .Include(rd => rd.WorkTask)
                .Include(rd => rd.ReferenceDocumentType)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return referenceDocument.ToReferenceDocumentDto();
        }
    }
}
