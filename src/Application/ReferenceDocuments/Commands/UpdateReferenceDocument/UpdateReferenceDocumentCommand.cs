﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocuments
{
    public class UpdateReferenceDocumentCommand : IRequest<Guid>
    {
        public UpdateReferenceDocumentDto UpdateReferenceDocument { get; set; }
    }

    public class UpdateReferenceDocumentCommandHandler : IRequestHandler<UpdateReferenceDocumentCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateReferenceDocumentCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateReferenceDocumentCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateReferenceDocument;

            var referenceDocument = await _context.ReferenceDocuments.FindAsync(requestDto.Id);

            referenceDocument.Name = requestDto.Name;
            referenceDocument.Size = requestDto.Size;
            referenceDocument.ContentType = requestDto.ContentType;
            referenceDocument.Content = Convert.FromBase64String(requestDto.ContentAsBase64);
            referenceDocument.Site = await _context.Sites.FindAsync(requestDto.SiteId);
            referenceDocument.WorkTask = await _context.WorkTasks.FindAsync(requestDto.TaskId);
            referenceDocument.ReferenceDocumentType = await _context.ReferenceDocumentTypes.FindAsync(requestDto.ReferenceDocumentTypeId);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return referenceDocument.Id;
        }
    }
}
