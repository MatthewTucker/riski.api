﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocuments
{
    class UpdateReferenceDocumentCommandValidator : AbstractValidator<UpdateReferenceDocumentCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateReferenceDocumentCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateReferenceDocument).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UpdateReferenceDocument.SiteId).NotEmpty().MustAsync(IsValidId).WithMessage("Invalid Id");
                    RuleFor(v => v.UpdateReferenceDocument.SiteId).NotEmpty().MustAsync(IsValidSiteId).WithMessage("Invalid SiteId");
                    RuleFor(v => v.UpdateReferenceDocument.TaskId).NotEmpty().MustAsync(IsValidSiteId).WithMessage("Invalid TaskId");
                    RuleFor(v => v.UpdateReferenceDocument.ReferenceDocumentTypeId).NotEmpty().MustAsync(IsValidSiteId).WithMessage("Invalid ReferenceDocumentTypeId");
                    RuleFor(v => v.UpdateReferenceDocument.ContentAsBase64).NotEmpty();
                    RuleFor(v => v.UpdateReferenceDocument.Name).NotEmpty();
                    RuleFor(v => v.UpdateReferenceDocument.Size).NotEmpty();
                });
        }

        public async Task<bool> IsValidId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocuments.AnyAsync(r => r.Id == id, cancellationToken);
        }

        public async Task<bool> IsValidSiteId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Sites.AnyAsync(r => r.Id == id, cancellationToken);
        }

        public async Task<bool> IsValidTaskId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.WorkTasks.AnyAsync(r => r.Id == id, cancellationToken);
        }

        public async Task<bool> IsValidReferenceDocumentTypeId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocumentTypes.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}

