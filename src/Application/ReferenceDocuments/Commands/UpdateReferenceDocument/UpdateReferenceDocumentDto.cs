﻿using System;

namespace Riski.Api.Application.ReferenceDocuments
{
    public class UpdateReferenceDocumentDto
    {
        public Guid Id { get; set; }
        public Guid SiteId { get; set; }
        public Guid TaskId { get; set; }
        public Guid ReferenceDocumentTypeId { get; set; }
        public string ContentType { get; set; }
        public DateTime Timestamp { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string ContentAsBase64 { get; set; }
    }
}
