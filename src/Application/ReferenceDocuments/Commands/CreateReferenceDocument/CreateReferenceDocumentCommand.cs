﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.ReferenceDocuments
{
    public class CreateReferenceDocumentCommand : IRequest<Guid>
    {
        public CreateReferenceDocumentDto CreateReferenceDocumentDto { get; set; }
    }

    public class CreateReferenceDocumentCommandHandler : IRequestHandler<CreateReferenceDocumentCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateReferenceDocumentCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateReferenceDocumentCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateReferenceDocumentDto;
            var ReferenceDocuments = new ReferenceDocument
            {
                Name = requestDto.Name,
                Size = requestDto.Size,
                ContentType = requestDto.ContentType,
                Content = Convert.FromBase64String(requestDto.ContentAsBase64),
                Site = await _context.Sites.FindAsync(requestDto.SiteId),
                WorkTask = await _context.WorkTasks.FindAsync(requestDto.TaskId),
                ReferenceDocumentType = await _context.ReferenceDocumentTypes.FindAsync(requestDto.ReferenceDocumentTypeId)
            };

            await _context.ReferenceDocuments.AddAsync(ReferenceDocuments, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return ReferenceDocuments.Id;
        }
    }
}
