﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocuments
{
    class CreateReferenceDocumentsCommandValidator : AbstractValidator<CreateReferenceDocumentCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateReferenceDocumentsCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateReferenceDocumentDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateReferenceDocumentDto.SiteId).NotEmpty().MustAsync(IsValidSiteId).WithMessage("Invalid SiteId");
                    RuleFor(v => v.CreateReferenceDocumentDto.TaskId).NotEmpty().MustAsync(IsValidSiteId).WithMessage("Invalid TaskId");
                    RuleFor(v => v.CreateReferenceDocumentDto.ReferenceDocumentTypeId).NotEmpty().MustAsync(IsValidSiteId).WithMessage("Invalid ReferenceDocumentTypeId");
                    RuleFor(v => v.CreateReferenceDocumentDto.ContentAsBase64).NotEmpty();
                    RuleFor(v => v.CreateReferenceDocumentDto.Name).NotEmpty();
                    RuleFor(v => v.CreateReferenceDocumentDto.Size).NotEmpty();
                });
        }

        public async Task<bool> IsValidSiteId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Sites.AnyAsync(r => r.Id == id, cancellationToken);
        }

        public async Task<bool> IsValidTaskId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.WorkTasks.AnyAsync(r => r.Id == id, cancellationToken);
        }

        public async Task<bool> IsValidReferenceDocumentTypeId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.ReferenceDocumentTypes.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}

