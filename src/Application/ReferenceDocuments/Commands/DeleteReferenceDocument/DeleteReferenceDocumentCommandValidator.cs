﻿using FluentValidation;

namespace Riski.Api.Application.ReferenceDocuments
{
    class DeleteReferenceDocumentCommandValidator : AbstractValidator<DeleteReferenceDocumentCommand>
    {
        public DeleteReferenceDocumentCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
