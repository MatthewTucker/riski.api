﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.ReferenceDocuments
{
    public class DeleteReferenceDocumentCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteReferenceDocumentCommandHandler : IRequestHandler<DeleteReferenceDocumentCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteReferenceDocumentCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteReferenceDocumentCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var defiendAnswer = await _context.ReferenceDocuments.FindAsync(request.Id);

            _context.ReferenceDocuments.Remove(defiendAnswer);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
