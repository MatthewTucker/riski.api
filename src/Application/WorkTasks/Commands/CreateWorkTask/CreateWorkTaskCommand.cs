﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.WorkTasks
{
    public class CreateWorkTaskCommand : IRequest<Guid>
    {
        public CreateWorkTaskDto CreateWorkTask { get; set; }
    }

    public class CreateWorkTaskCommandHandler : IRequestHandler<CreateWorkTaskCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateWorkTaskCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateWorkTaskCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateWorkTask;
            var WorkTask = new WorkTask
            {
                Description = requestDto.Description,
                Name = requestDto.Name,
                MinLikelihoodRating = requestDto.MinLikelihoodRating,
                MinConsequenceRating = requestDto.MinConsequenceRating,
                Status = PersistenceStatus.Available
            };

            await _context.WorkTasks.AddAsync(WorkTask, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return WorkTask.Id;
        }
    }
}
