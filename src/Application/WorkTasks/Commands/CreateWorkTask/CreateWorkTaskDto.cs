﻿namespace Riski.Api.Application.WorkTasks
{
    public class CreateWorkTaskDto
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public int MinLikelihoodRating { get; set; }
        public int MinConsequenceRating { get; set; }
    }
}
