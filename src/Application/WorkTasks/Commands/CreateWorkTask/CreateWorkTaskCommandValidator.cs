﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.WorkTasks
{
    class CreateWorkTaskCommandValidator : AbstractValidator<CreateWorkTaskCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateWorkTaskCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateWorkTask).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateWorkTask.Name).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("WorkTask already exists with this Name");
                });
        }

        public async Task<bool> IsNotDuplicate(string name, CancellationToken cancellationToken)
        {
            return await _context.WorkTasks.AnyAsync(r => r.Name == name, cancellationToken);
        }
    }
}

