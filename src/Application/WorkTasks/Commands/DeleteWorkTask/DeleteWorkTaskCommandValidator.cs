﻿using FluentValidation;

namespace Riski.Api.Application.WorkTasks
{
    class DeleteWorkTaskCommandValidator : AbstractValidator<DeleteWorkTaskCommand>
    {
        public DeleteWorkTaskCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
