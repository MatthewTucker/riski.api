﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.WorkTasks
{
    public class UpdateWorkTaskCommand : IRequest<Guid>
    {
        public UpdateWorkTaskDto UpdateWorkTask { get; set; }
    }

    public class UpdateWorkTaskCommandHandler : IRequestHandler<UpdateWorkTaskCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateWorkTaskCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateWorkTaskCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateWorkTask;

            var oldRecord = await _context.WorkTasks.FindAsync(requestDto.Id);
            oldRecord.Status = PersistenceStatus.Deleted;

            var workTask = new WorkTask
            {
                Description = requestDto.Description,
                Name = requestDto.Name,
                MinLikelihoodRating = requestDto.MinLikelihoodRating,
                MinConsequenceRating = requestDto.MinConsequenceRating,
                Status = PersistenceStatus.Available
            };

            await _context.WorkTasks.AddAsync(workTask, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return workTask.Id;
        }
    }
}
