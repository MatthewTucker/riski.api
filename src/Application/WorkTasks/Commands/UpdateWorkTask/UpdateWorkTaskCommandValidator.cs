﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.WorkTasks
{
    class UpdateWorkTaskCommandValidator : AbstractValidator<UpdateWorkTaskCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateWorkTaskCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateWorkTask).NotNull().WithMessage("Update body not found").DependentRules(() =>
            {
                RuleFor(v => v.UpdateWorkTask.Id).NotEmpty().MustAsync(IsValidId).WithMessage("Invalid Id");
                RuleFor(v => v.UpdateWorkTask.Description).NotEmpty();
                RuleFor(v => v.UpdateWorkTask.Name).NotEmpty();
            });
        }

        public async Task<bool> IsValidId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.WorkTasks.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}


