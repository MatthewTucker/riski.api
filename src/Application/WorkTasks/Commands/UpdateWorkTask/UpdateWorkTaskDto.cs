﻿using System;

namespace Riski.Api.Application.WorkTasks
{
    public class UpdateWorkTaskDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int MinLikelihoodRating { get; set; }
        public int MinConsequenceRating { get; set; }
    }
}
