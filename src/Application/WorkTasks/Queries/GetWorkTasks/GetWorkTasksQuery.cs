﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.WorkTasks
{
    public class GetWorkTasksQuery : IRequest<List<WorkTaskDto>>
    {
    }

    public class GetWorkTasksQueryHandler : IRequestHandler<GetWorkTasksQuery, List<WorkTaskDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetWorkTasksQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<WorkTaskDto>> Handle(GetWorkTasksQuery request, CancellationToken cancellationToken)
        {
            return await _context.WorkTasks.Select(r => r.ToWorkTaskDto()).ToListAsync(cancellationToken);
        }
    }
}
