﻿using FluentValidation;

namespace Riski.Api.Application.WorkTasks
{
    public class GetWorkTaskQueryValidator : AbstractValidator<GetWorkTaskQuery>
    {
        public GetWorkTaskQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
