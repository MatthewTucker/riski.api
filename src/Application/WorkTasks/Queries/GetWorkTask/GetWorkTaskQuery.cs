﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.WorkTasks
{
    public class GetWorkTaskQuery : IRequest<WorkTaskDto>
    {
        public Guid Id { get; set; }
    }

    public class GetWorkTaskQueryHandler : IRequestHandler<GetWorkTaskQuery, WorkTaskDto>
    {
        private readonly IApplicationDbContext _context;

        public GetWorkTaskQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<WorkTaskDto> Handle(GetWorkTaskQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.WorkTasks.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return record.ToWorkTaskDto();
        }
    }
}
