﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.FormPageTypes
{
    public class GetFormPageTypesQuery : IRequest<List<FormPageTypeDto>>
    {
    }

    public class GetFormPageTypesQueryHandler : IRequestHandler<GetFormPageTypesQuery, List<FormPageTypeDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetFormPageTypesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<FormPageTypeDto>> Handle(GetFormPageTypesQuery request, CancellationToken cancellationToken)
        {
            return await _context.FormPageTypes.Select(r => r.ToFormPageTypeDto()).ToListAsync();
        }
    }
}
