﻿using FluentValidation;

namespace Riski.Api.Application.FormPageTypes
{
    public class GetFormPageTypeQueryValidator : AbstractValidator<GetFormPageTypeQuery>
    {
        public GetFormPageTypeQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
