﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.FormPageTypes
{
    public class GetFormPageTypeQuery : IRequest<FormPageTypeDto>
    {
        public Guid Id { get; set; }
    }

    public class GetFormPageTypeQueryHandler : IRequestHandler<GetFormPageTypeQuery, FormPageTypeDto>
    {
        private readonly IApplicationDbContext _context;

        public GetFormPageTypeQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<FormPageTypeDto> Handle(GetFormPageTypeQuery request, CancellationToken cancellationToken)
        {
            var formPageType = await _context.FormPageTypes.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return formPageType.ToFormPageTypeDto();
        }
    }
}
