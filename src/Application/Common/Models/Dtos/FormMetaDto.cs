﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class FormMetaDto
    {
        public Guid FormId { get; set; }
        public string FormName { get; set; }
        public string TaskDescription { get; set; }
        public string Location { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
