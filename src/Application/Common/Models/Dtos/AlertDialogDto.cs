﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class AlertDialogDto
    {
        public Guid id { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public string positiveText { get; set; }
        public string negativeText { get; set; }
        public string positiveAction { get; set; }
        public string negativeAction { get; set; }
        public virtual EventConditionDto condition { get; set; }
        public int position { get; set; }
        public object extras { get; set; }
    }
}
