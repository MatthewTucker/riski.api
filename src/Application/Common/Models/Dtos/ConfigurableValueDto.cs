﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class ConfigurableValueDto
    {
        public Guid Id { get; set; }
        public string SystemKey { get; set; }
        public string Category { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
