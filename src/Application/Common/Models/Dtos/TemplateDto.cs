﻿using System;
using System.Collections.Generic;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class TemplateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public List<FormPageDto> FormPages { get; set; }
    }
}
