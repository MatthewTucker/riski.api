﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class RiskConfigurationDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int Rating { get; set; }
        public string Status { get; set; }
    }
}
