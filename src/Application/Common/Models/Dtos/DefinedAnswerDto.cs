﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class DefinedAnswerDto
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
        public DefinedAnswerCategoryDto Category { get; set; }
    }
}
