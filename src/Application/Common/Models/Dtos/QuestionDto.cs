﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class QuestionDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string DisplayType { get; set; }
        public string PageName { get; set; }
        public bool ReadOnly { get; set; }
        public string LookupKey { get; set; }
        public bool Mandatory { get; set; }
        public Guid FormPageId { get; set; }
        public string FormPageName { get; set; }
        public Guid TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string Status { get; set; }
        public int Position { get; set; }
    }
}
