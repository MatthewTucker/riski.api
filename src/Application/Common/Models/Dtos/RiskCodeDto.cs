﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class RiskCodeDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public RiskConfigurationDto Likelihood { get; set; }
        public RiskConfigurationDto Consequence { get; set; }
        public string Status { get; set; }
    }
}
