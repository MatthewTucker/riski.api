﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class GuestUserDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public SignatureDto Signature { get; set; }
    }
}
