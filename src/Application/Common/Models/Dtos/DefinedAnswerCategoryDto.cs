﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class DefinedAnswerCategoryDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
    }
}
