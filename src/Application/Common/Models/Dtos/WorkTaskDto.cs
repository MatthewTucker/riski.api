﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class WorkTaskDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int MinLikelihoodRating { get; set; }
        public int MinConsequenceRating { get; set; }
        public string Status { get; set; }
    }
}
