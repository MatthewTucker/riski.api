﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class ReferenceDocumentDto
    {
        public Guid Id { get; set; }
        public Guid? SiteId { get; set; }
        public SiteDto Site { get; set; }
        public Guid? TaskId { get; set; }
        public WorkTaskDto Task { get; set; }

        public Guid ReferenceDocumentTypeId { get; set; }
        public ReferenceDocumentTypeDto ReferenceDocumentType { get; set; }
        public string ContentType { get; set; }
        public DateTime Timestamp { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string ContentAsBase64 { get; set; }
    }
}
