﻿using Riski.Api.Domain.Entities;

using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class ManagerDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string MobileNumber { get; set; }

        public void config(User manager)
        {
            Id = manager.Id;
            FullName = manager.FirstName + " " + manager.LastName;
            MobileNumber = manager.PhoneNumber.Value;
        }
    }
}
