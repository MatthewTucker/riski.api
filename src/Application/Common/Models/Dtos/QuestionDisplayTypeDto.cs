﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class QuestionDisplayTypeDto
    {
        public Guid Id { get; set; }
        public string SystemValue { get; set; }
        public string DisplayValue { get; set; }
        public string Description { get; set; }
    }
}
