﻿namespace Riski.Api.Application.Common.Models.Dtos
{
    public class DashboardGraphDataDto
    {
        public string Timestamp { get; set; }
        public int Risks { get; set; }
        public int FormsSubmitted { get; set; }
    }
}
