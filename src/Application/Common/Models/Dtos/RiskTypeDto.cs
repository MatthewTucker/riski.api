﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class RiskTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
    }
}
