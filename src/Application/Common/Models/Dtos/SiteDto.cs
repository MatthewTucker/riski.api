﻿using Riski.Api.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class SiteDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ManagerDto> Managers { get; set; }

        public string Status { get; set; }

        public void config(Site site)
        {
            Id = site.Id;
            Name = site.Name;
            Managers = new List<ManagerDto>();
            foreach (var manager in site.Users)
            {
                ManagerDto httpManager = new ManagerDto();
                httpManager.config(manager);
                Managers.Add(httpManager);
            }
            Status = site.Status.ToString();
        }
    }
}
