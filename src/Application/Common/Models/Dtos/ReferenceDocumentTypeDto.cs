﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class ReferenceDocumentTypeDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public byte[] DisplayImage { get; set; }
        public byte[] SelectedDisplayImage { get; set; }
        public string Status { get; set; }
    }
}
