﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class FormPageTypeDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool ReadOnly { get; set; }
    }
}
