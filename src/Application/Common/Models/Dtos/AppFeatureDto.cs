﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class AppFeatureDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool Enabled { get; set; }
    }
}
