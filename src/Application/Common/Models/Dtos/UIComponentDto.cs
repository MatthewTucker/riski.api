﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class UIComponentDto
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
    }
}
