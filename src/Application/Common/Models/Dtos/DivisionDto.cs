﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class DivisionDto
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string Status { get; set; }
    }
}
