﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class RiskDto
    {
        public Guid Id { get; set; }
        public RiskConfigurationDto Likelihood { get; set; }
        public RiskConfigurationDto Consequence { get; set; }
        public string ConsequenceDescription { get; set; }
        public string Description { get; set; }
        public RiskTypeDto Type { get; set; }
        public string Code { get; set; }
        public string PostMitigationCode { get; set; }
        public RiskConfigurationDto PostMitigationConsequence { get; set; }
        public RiskConfigurationDto PostMitigationLikelihood { get; set; }
        public string ConsequenceDesc { get; set; }
        public string MitigationDesc { get; set; }
        public string Status { get; set; }
        public DateTime Timestamp { get; set; }
        public RiskRatingDto RiskRating { get; set; }
        public SiteDto Site { get; set; }
    }
}
