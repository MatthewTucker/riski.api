﻿using System.Collections.Generic;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class DashboardSiteDataDto
    {
        public SiteDto site { get; set; }

        public List<DashboardGraphDataDto> graphData { get; set; }
    }
}
