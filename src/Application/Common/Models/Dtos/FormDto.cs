﻿using System;
using System.Collections.Generic;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class FormDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string UserFullName { get; set; }
        public List<AnswerDto> Answers { get; set; }
        public Guid SiteId { get; set; }
        public Guid TemplateId { get; set; }
        public List<RiskDto> Risks { get; set; }
        public List<Guid> ProxyUserIds { get; set; }
        public List<SignatureDto> Signatures { get; set; }
        public string ManagerApprovalStatus { get; set; }
        public string DateTimeSubmitted { get; set; }
        public string DateTimeCompleted { get; set; }
        public List<UserDto> ProxyUsers { get; set; }

        public List<GuestUserDto> GuestUsers { get; set; }

        public Guid? ParentFormId { get; set; }
        public List<FormDto> SubForms { get; set; }
    }
}
