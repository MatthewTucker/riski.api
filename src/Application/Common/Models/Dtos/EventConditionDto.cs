﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class EventConditionDto
    {
        public Guid Id { get; set; }
        public string Scope { get; set; }
        public string Specific { get; set; }
        public string Member { get; set; }
        public string Value { get; set; }
    }
}
