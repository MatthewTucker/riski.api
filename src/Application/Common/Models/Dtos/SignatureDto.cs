﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class SignatureDto
    {
        public Guid UserId { get; set; }
        public string UserType { get; set; }
        public string Value { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid Id { get; set; }
    }
}
