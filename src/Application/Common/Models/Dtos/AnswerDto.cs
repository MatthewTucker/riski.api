﻿using System;

namespace Riski.Api.Application.Common.Models.Dtos
{
    public class AnswerDto
    {
        public Guid Id { get; set; }
        public Guid QuestionId { get; set; }
        public Guid WorkerId { get; set; }
        public string Value { get; set; }
    }
}
