﻿using Riski.Api.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace Riski.Api.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
