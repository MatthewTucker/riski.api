﻿using Riski.Api.Domain.Entities;

using Microsoft.EntityFrameworkCore;

using System.Threading;
using System.Threading.Tasks;

namespace Riski.Api.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<TodoList> TodoLists { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Form> Forms { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<Answer> Answers { get; set; }

        public DbSet<Site> Sites { get; set; }

        public DbSet<Template> Templates { get; set; }

        public DbSet<Risk> Risks { get; set; }

        public DbSet<Division> Divisions { get; set; }

        public DbSet<RiskType> RiskTypes { get; set; }

        public DbSet<RiskRating> RiskRatings { get; set; }

        public DbSet<QuestionDisplayType> QuestionDisplayTypes { get; set; }

        public DbSet<FormPage> FormPages { get; set; }

        public DbSet<RiskConfiguration> RiskConfigurations { get; set; }

        public DbSet<ConfigurableValue> ConfigurableValue { get; set; }

        public DbSet<ReferenceDocument> ReferenceDocuments { get; set; }

        public DbSet<ReferenceDocumentType> ReferenceDocumentTypes { get; set; }

        public DbSet<WorkTask> WorkTasks { get; set; }

        public DbSet<Signature> Signatures { get; set; }

        public DbSet<FormPageType> FormPageTypes { get; set; }

        public DbSet<AlertDialog> AlertDialogs { get; set; }

        public DbSet<EventCondition> EventConditions { get; set; }

        public DbSet<UIComponent> UIComponents { get; set; }

        public DbSet<GuestUser> GuestUsers { get; set; }

        public DbSet<DefinedAnswer> DefinedAnswers { get; set; }

        public DbSet<DefinedAnswerCategory> DefinedAnswerCategories { get; set; }

        public DbSet<RiskCode> RiskCodes { get; set; }

        public DbSet<AppFeature> AppFeatures { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
