﻿using System;
using System.Threading.Tasks;

using Riski.Api.Application.Common.Models;
using Riski.Api.Domain;

namespace Riski.Api.Application.Common.Interfaces
{
    public interface IIdentityService
    {
        Task<string> GetUserNameAsync(string userId);

        Task<bool> IsInRoleAsync(string userId, string role);

        Task<bool> AuthorizeAsync(string userId, string policyName);

        Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password);

        Task<Result> DeleteUserAsync(string userId);

        Task<string> Login(AccessToken token);

        Task<Result> UpdatePasswordAsync(string userId, string password);

        Task<AccessToken> AuthenticateAsync(string username, string password);
    }
}
