﻿using Riski.Api.Domain.Common;
using System.Threading.Tasks;

namespace Riski.Api.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
