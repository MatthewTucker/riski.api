﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class DefinedAnswerCategoryMapper
    {
        public static DefinedAnswerCategoryDto ToDefinedAnswerCategoryDto(this DefinedAnswerCategory definedAnswerCategory)
        {
            return new DefinedAnswerCategoryDto
            {
                Description = definedAnswerCategory.Description,
                Value = definedAnswerCategory.Value,
                Status = definedAnswerCategory.Status.ToString(),
                Id = definedAnswerCategory.Id
            };
        }
    }
}
