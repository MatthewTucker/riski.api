﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class DivisionMapper
    {
        public static DivisionDto ToDivisionDto(this Division division)
        {
            return new DivisionDto()
            {
                Description = division.Description,
                Name = division.Name,
                Id = division.Id,
                Status = division.Status.ToString()
            };
        }
    }
}
