﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class AppFeatureMapper
    {
        public static AppFeatureDto ToAppFeatureDto(this AppFeature appFeature)
        {
            return new AppFeatureDto()
            {
                Id = appFeature.Id,
                Code = appFeature.Code,
                Name = appFeature.Name,
                Description = appFeature.Description,
                Category = appFeature.Category,
                Enabled = appFeature.Enabled,
            };
        }
    }
}
