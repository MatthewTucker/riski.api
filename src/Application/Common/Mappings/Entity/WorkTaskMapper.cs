﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class WorkTaskMapper
    {
        public static WorkTaskDto ToWorkTaskDto(this WorkTask workTask)
        {
            return new WorkTaskDto
            {
                Id = workTask.Id,
                Description = workTask.Description,
                Name = workTask.Name,
                MinConsequenceRating = workTask.MinConsequenceRating,
                MinLikelihoodRating = workTask.MinLikelihoodRating,
                Status = workTask.Status.ToString(),
            };
        }
    }
}
