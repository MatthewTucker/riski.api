﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class UserMapper
    {
        public static UserDto ToUserDto(this User user)
        {
            return new UserDto()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Type = user.UserType.ToString(),
                Mobile = user.PhoneNumber.Value,
                Email = user.Email.Value,
                Username = user.Username
                
            };
        }

        public static ManagerDto ToManagerDto(this User user)
        {
            return new ManagerDto()
            {
                Id = user.Id,
                FullName = $"{user.FirstName} {user.LastName}",
                MobileNumber = user.PhoneNumber.Value
            };
        }
    }
}
