﻿using System;
using System.Collections.Generic;
using System.Linq;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class FormMapper
    {
        public static FormDto ToFormDto(this Form form)
        {
            var formDto = new FormDto
            {
                Id = form.Id,
                UserId = form.User.Id,
                UserFullName = form.User.FullName(),
                Answers = form.Answers.Select(a => a.ToAnswerDto()).ToList(),
                SiteId = form.Site.Id,
                TemplateId = form.Template.Id,
                Risks = form.Risks.Select(r => r.ToRiskDto()).ToList(),
                Signatures = form.Signatures?.Select(s => s.ToSignatureDto()).ToList(),
                ManagerApprovalStatus = form.ManagerApprovalStatus,
                DateTimeSubmitted = form.DateTimeSubmitted.ToString("dd/MM/yyyy HH:mm:ss"),
                DateTimeCompleted = form.DateTimeCompleted?.ToString("dd/MM/yyyy HH:mm:ss")
            };

            formDto.ProxyUsers = new List<UserDto>();
            formDto.ProxyUserIds = new List<Guid>();

            if (form.ProxyWorkers != null)
            {
                foreach (var proxyUser in form.ProxyWorkers)
                {
                    formDto.ProxyUsers.Add(proxyUser.ToUserDto());
                    formDto.ProxyUserIds.Add(proxyUser.Id);
                }
            }

            formDto.GuestUsers = form.GuestUsers?.Select(guestUser => guestUser.ToGuestUserDto()).ToList();

            formDto.ParentFormId = form.ParentFormId;

            formDto.SubForms = form.SubForms?.Select(subForm =>
            {
                var httpSubForm = subForm.ToFormDto();
                httpSubForm.ParentFormId = form.Id;
                return httpSubForm;
            }).ToList();

            return formDto;
        }

        public static FormMetaDto ToFormMetaDto(this Form form)
        {
            return new FormMetaDto()
            {
                FormId = form.Id,
                TaskDescription = getValue(form, "workTasks"),
                Location = form.Site?.Name ?? getValue(form, "sites"),
                StartTime = form.DateTimeSubmitted.ToString("hh:mm tt"),
                EndTime = form.DateTimeCompleted?.ToString("hh:mm tt"),
                FormName = form.Template?.Name
            };
        }

        internal static string getValue(Form form, string lookupKey)
        {
            foreach (var answer in form.Answers)
            {
                if (answer.Question.LookupKey != null)
                {
                    if (answer.Question.LookupKey.Equals(lookupKey))
                    {
                        return answer.Value;
                    }
                }
            }
            return null;
        }
    }
}
