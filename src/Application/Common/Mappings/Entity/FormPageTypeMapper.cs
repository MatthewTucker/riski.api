﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class FormPageTypeMapper
    {
        public static FormPageTypeDto ToFormPageTypeDto(this FormPageType formPageType)
        {
            return new FormPageTypeDto
            {
                Id = formPageType.Id,
                Code = formPageType.Code,
                Name = formPageType.Name,
                Description = formPageType.Description,
                ReadOnly = formPageType.ReadOnly,
            };
        }
    }
}
