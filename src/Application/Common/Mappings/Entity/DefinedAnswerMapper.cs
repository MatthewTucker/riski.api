﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class DefinedAnswerMapper
    {
        public static DefinedAnswerDto ToDefinedAnswerDto(this DefinedAnswer definedAnswer)
        {
            return new DefinedAnswerDto
            {
                Id = definedAnswer.Id,
                Category = definedAnswer.Category.ToDefinedAnswerCategoryDto(),
                Value = definedAnswer.Value
            };
        }
    }
}
