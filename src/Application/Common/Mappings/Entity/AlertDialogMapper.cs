﻿using System.Text.Json;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class AlertDialogMapper
    {
        public static AlertDialogDto ToAlertDialogDto(this AlertDialog alertDialog)
        {
            var httpAlertDialog = new AlertDialogDto()
            {
                id = alertDialog.Id,
                title = alertDialog.Title,
                message = alertDialog.Message,
                positiveText = alertDialog.PositiveText,
                negativeText = alertDialog.NegativeText,
                positiveAction = alertDialog.PositiveAction,
                negativeAction = alertDialog.NegativeAction,
                condition = alertDialog.EventCondition?.ToEventConditionDto(),
                position = alertDialog.Position
            };

            if (alertDialog.Extras != null)
            {
                httpAlertDialog.extras = JsonSerializer.Deserialize<object>(alertDialog.Extras.Replace("\n", ""));
            }

            return httpAlertDialog;
        }
    }
}
