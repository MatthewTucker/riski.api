﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class AnswerMapper
    {
        public static AnswerDto ToAnswerDto(this Answer answer)
        {
            return new AnswerDto()
            {
                Id = answer.Id,
                QuestionId = answer.Question.Id,
                WorkerId = answer.ApplicationUserId,
                Value = answer.Value
            };
        }
    }
}
