﻿using System.Linq;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class SiteMapper
    {
        public static SiteDto ToSiteDto(this Site site)
        {
            return new SiteDto
            {
                Id = site.Id,
                Name = site.Name,
                Managers = site.Users?.Select(x => x.ToManagerDto()).ToList(),
                Status = site.Status.ToString()
            };
        }
    }
}
