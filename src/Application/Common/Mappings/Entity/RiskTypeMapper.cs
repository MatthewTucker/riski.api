﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class RiskTypeMapper
    {
        public static RiskTypeDto ToRiskTypeDto(this RiskType riskType)
        {
            return new RiskTypeDto
            {
                Id = riskType.Id,
                Name = riskType.Name,
                Code = riskType.Code,
                Status = riskType.Status.ToString()
            };
        }
    }
}
