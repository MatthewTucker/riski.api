﻿using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Common.Mappings
{
    public static class FormPageMapper
    {
        public static FormPageDto ToFormPageDto(this FormPage formPage)
        {
            var formPageDto = new FormPageDto
            {
                Id = formPage.Id,
                Status = formPage.Status.ToString(),
                PageName = formPage.PageName,
                Title = formPage.Title,
                Description = formPage.Description,
                PageType = formPage.FormPageType.Code,
                FormPageTypeId = formPage.FormPageType.Id,
                Position = formPage.Position,

                NavigateNext = new List<AlertDialogDto>()
            };
            foreach (var alertDialog in formPage.AlertDialogs)
            {
                formPageDto.NavigateNext.Add(alertDialog.ToAlertDialogDto());
            }

            formPageDto.NavigateNext.OrderBy(x => x.position);

            formPageDto.UIComponents = new List<UIComponentDto>();
            foreach (var component in formPage.UIComponents)
            {
                formPageDto.UIComponents.Add(component.ToUIComponentDto());
            }

            formPageDto.Questions = formPage.Questions?
                .Where(q => q.Status == PersistenceStatus.Available)
                .Select(q => q.ToQuestionDto())
                .OrderBy(q => q.Position)
                .ToList();

            formPageDto.TemplateId = formPage.TemplateId;

            if (formPage.Extras != null)
            {
                formPageDto.Extras = JsonSerializer.Deserialize<object>(formPage.Extras.Replace("\n", ""));
            }

            return formPageDto;
        }
    }
}
