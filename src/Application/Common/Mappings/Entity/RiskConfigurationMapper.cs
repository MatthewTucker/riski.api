﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class RiskConfigurationMapper
    {
        public static RiskConfigurationDto ToRiskConfigurationDto(this RiskConfiguration riskConfiguration)
        {
            return new RiskConfigurationDto
            {
                Id = riskConfiguration.Id,
                Code = riskConfiguration.Code,
                Value = riskConfiguration.Value,
                Description = riskConfiguration.Description,
                Category = riskConfiguration.Category,
                Rating = riskConfiguration.Rating,
                Status = riskConfiguration.Status.ToString()
            };
        }
    }
}
