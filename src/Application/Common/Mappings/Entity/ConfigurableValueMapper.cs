﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class ConfigurableValueMapper
    {
        public static ConfigurableValueDto ToConfigurableValueDto(this ConfigurableValue configurableValue)
        {
            return new ConfigurableValueDto()
            {
                Id = configurableValue.Id,
                SystemKey = configurableValue.SystemKey,
                Category = configurableValue.Category,
                Value = configurableValue.Value,
                Description = configurableValue.Description
            };
        }
    }
}
