﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class GuestUserMapper
    {
        public static GuestUserDto ToGuestUserDto(this GuestUser guestUser)
        {
            return new GuestUserDto()
            {
                Id = guestUser.Id,
                FirstName = guestUser.FirstName,
                LastName = guestUser.LastName,
                Mobile = guestUser.Mobile,
                Email = guestUser.Email,
                Signature = guestUser.Signature?.ToSignatureDto()
            };
        }
    }
}
