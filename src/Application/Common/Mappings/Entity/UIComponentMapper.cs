﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class UIComponentMapper
    {
        public static UIComponentDto ToUIComponentDto(this UIComponent uiComponent)
        {
            return new UIComponentDto
            {
                Id = uiComponent.Id,
                Value = uiComponent.Value,
                Description = uiComponent.Description,
                Title = uiComponent.Title,
                Type = uiComponent.Type
            };
        }
    }
}
