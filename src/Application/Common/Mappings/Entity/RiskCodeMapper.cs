﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class RiskCodeMapper
    {
        public static RiskCodeDto ToRiskCodeDto(this RiskCode riskCode)
        {
            var riskCodeDto = new RiskCodeDto
            {
                Id = riskCode.Id,
                Code = riskCode.Code,
                Status = riskCode.Status.ToString()
            };

            riskCodeDto.Likelihood = riskCode.Likelihood.ToRiskConfigurationDto();

            riskCodeDto.Consequence = riskCode.Consequence.ToRiskConfigurationDto();

            return riskCodeDto;
        }
    }
}
