﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class RiskMapper
    {
        public static RiskDto ToRiskDto(this Risk risk)
        {
            return new RiskDto
            {
                Id = risk.Id,
                Likelihood = risk.Likelihood?.ToRiskConfigurationDto(),
                Consequence = risk.Consequence?.ToRiskConfigurationDto(),
                Description = risk.Description,
                Type = risk.Type?.ToRiskTypeDto(),
                Code = risk.Code,
                ConsequenceDesc = risk.ConsequenceDescription,
                MitigationDesc = risk.MitigationDescription,

                PostMitigationCode = risk.PostMitigationCode,
                PostMitigationLikelihood = risk.PostMigitationLikelihood?.ToRiskConfigurationDto(),
                PostMitigationConsequence = risk.PostMitigationConsequence?.ToRiskConfigurationDto(),

                Status = risk.Status.ToString(),
                Timestamp = risk.Timestamp,
                Site = risk.Site?.ToSiteDto(),

                
            };
        }
    }
}
