﻿using System;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class ReferenceDocumentMapper
    {
        public static ReferenceDocumentDto ToReferenceDocumentDto(this ReferenceDocument referenceDocument)
        {
            return new ReferenceDocumentDto
            {
                Site = referenceDocument.Site?.ToSiteDto(),
                SiteId = referenceDocument.Site?.Id,
                Task = referenceDocument.WorkTask?.ToWorkTaskDto(),
                TaskId = referenceDocument.WorkTask?.Id,
                ReferenceDocumentType = referenceDocument.ReferenceDocumentType.ToReferenceDocumentTypeDto(),
                ReferenceDocumentTypeId = referenceDocument.ReferenceDocumentType.Id,
                Id = referenceDocument.Id,
                ContentType = referenceDocument.ContentType,
                Timestamp = referenceDocument.UploadDate,
                ContentAsBase64 = Convert.ToBase64String(referenceDocument.Content),
                Name = referenceDocument.Name,
                Size = referenceDocument.Size
            };
        }
    }
}
