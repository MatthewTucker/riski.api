﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class QuestionMapper
    {
        public static QuestionDto ToQuestionDto(this Question question)
        {
            var formPage = question.FormPage;
            return new QuestionDto
            {
                Id = question.Id,
                Description = question.Description,
                DisplayType = question.DisplayType,
                PageName = formPage.PageName,
                FormPageId = formPage.Id,
                FormPageName = formPage.PageName,
                ReadOnly = question.ReadOnly,
                LookupKey = question.LookupKey,
                Mandatory = question.Mandatory,
                Status = question.Status.ToString(),
                TemplateId = formPage.TemplateId,
                TemplateName = formPage.Template.Name,
                Position = question.Position
            };
        }
    }
}
