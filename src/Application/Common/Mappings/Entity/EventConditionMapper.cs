﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class EventConditionMapper
    {
        public static EventConditionDto ToEventConditionDto(this EventCondition eventCondition)
        {
            return new EventConditionDto
            {
                Id = eventCondition.Id,
                Scope = eventCondition.Scope,
                Specific = eventCondition.Specific,
                Member = eventCondition.Member,
                Value = eventCondition.Value
            };
        }
    }
}