﻿using System.Linq;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Common.Mappings
{
    public static class TemplateMapper
    {
        public static TemplateDto ToTemplateDto(this Template template)
        {
            var templateDto = new TemplateDto
            {
                Id = template.Id,
                Name = template.Name,
                Status = template.Status.ToString(),
                Description = template.Description,
                Type = template.Type,
            };

            templateDto.FormPages = template.FormPages.Where(p => p.Status == PersistenceStatus.Available)
                                                      .OrderBy(tfp => tfp.Position)
                                                      .Select(fp => fp.ToFormPageDto())
                                                      .ToList();

            return templateDto;
        }
    }
}
