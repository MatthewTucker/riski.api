﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class ReferenceDocumentTypeMapper
    {
        public static ReferenceDocumentTypeDto ToReferenceDocumentTypeDto(this ReferenceDocumentType referenceDocumentType)
        {
            return new ReferenceDocumentTypeDto
            {
                Id = referenceDocumentType.Id,
                Description = referenceDocumentType.Description,
                Code = referenceDocumentType.Code,
                DisplayImage = referenceDocumentType.DisplayImage,
                SelectedDisplayImage = referenceDocumentType.SelectedDisplayImage,
                Status = referenceDocumentType.Status.ToString(),
            };
        }
    }
}
