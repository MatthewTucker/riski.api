﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class SignatureMapper
    {
        public static SignatureDto ToSignatureDto(this Signature signature)
        {
            return new SignatureDto
            {
                UserId = signature.UserId,
                UserType = signature.UserType,
                Value = signature.Base64String,
                Id = signature.Id,
                FirstName = signature.FirstName,
                LastName = signature.LastName,
            };
        }
    }
}
