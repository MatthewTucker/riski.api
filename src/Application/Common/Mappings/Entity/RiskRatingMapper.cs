﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class RiskRatingMapper
    {
        public static RiskRatingDto ToRiskRatingDto(this RiskRating riskRating)
        {
            return new RiskRatingDto
            {
                Id = riskRating.Id,
                Value = riskRating.Value,
                Display = riskRating.Display,
                Message = riskRating.Message,
                Colour = riskRating.Colour,
                RiskCodes = riskRating.RiskCodes,
                ShouldNotify = riskRating.ShouldNotify,
                Status = riskRating.Status.ToString()
            };
        }
    }
}
