﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings
{
    public static class QuestionDisplayTypeMapper
    {
        public static QuestionDisplayTypeDto ToQuestionDisplayTypeDto(this QuestionDisplayType questionDisplayType)
        {
            return new QuestionDisplayTypeDto
            {
                Id = questionDisplayType.Id,
                SystemValue = questionDisplayType.SystemValue,
                DisplayValue = questionDisplayType.DisplayValue,
                Description = questionDisplayType.Description
            };
        }
    }
}
