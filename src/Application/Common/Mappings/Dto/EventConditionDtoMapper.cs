﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings.Dto
{
    public static class EventConditionDtoMapper
    {
        public static EventCondition ToEventCondition(this EventConditionDto eventConditionDto)
        {
            return new EventCondition
            {
                Id = eventConditionDto.Id,
                Scope = eventConditionDto.Scope,
                Specific = eventConditionDto.Specific,
                Member = eventConditionDto.Member,
                Value = eventConditionDto.Value
            };
        }
    }
}
