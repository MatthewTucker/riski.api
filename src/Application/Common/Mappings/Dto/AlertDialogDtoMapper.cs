﻿using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.Common.Mappings.Dto
{
    public static class AlertDialogDtoMapper
    {
        public static AlertDialog ToAlertDialog(this AlertDialogDto alertDialogDto)
        {
            var alertDialog = new AlertDialog
            {
                Id = alertDialogDto.id,
                Title = alertDialogDto.title,
                Message = alertDialogDto.message,
                PositiveText = alertDialogDto.positiveText,
                NegativeText = alertDialogDto.negativeText,
                PositiveAction = alertDialogDto.positiveAction,
                NegativeAction = alertDialogDto.negativeAction,
                EventCondition = alertDialogDto.condition.ToEventCondition(),
                Position = alertDialogDto.position
            };

            if (alertDialogDto.extras != null)
            {
                alertDialog.Extras = alertDialogDto.extras.ToString().Replace("\n", "");
            }

            return alertDialog;

        }
    }
}
