﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.ConfigurableValues
{
    public class GetConfigurableValuesQuery : IRequest<List<ConfigurableValueDto>>
    {
        public string Category { get; set; }
    }

    public class GetUserQueryHandler : IRequestHandler<GetConfigurableValuesQuery, List<ConfigurableValueDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetUserQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<ConfigurableValueDto>> Handle(GetConfigurableValuesQuery request, CancellationToken cancellationToken)
        {
            var configurableValues = string.IsNullOrEmpty(request.Category)
                ? _context.ConfigurableValue
                : _context.ConfigurableValue.Where(c => c.Category == request.Category);

            var values = await configurableValues
                    .AsNoTracking()
                    .OrderBy(t => t.Category)
                    .ToListAsync(cancellationToken);

            return values.Select(r => r.ToConfigurableValueDto()).ToList();
        }
    }
}
