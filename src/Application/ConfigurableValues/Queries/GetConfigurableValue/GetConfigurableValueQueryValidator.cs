﻿using FluentValidation;

namespace Riski.Api.Application.ConfigurableValues.Queries.GetConfigurableValue
{
    public class GetConfigurableValueQueryValidator : AbstractValidator<GetConfigurableValueQuery>
    {
        public GetConfigurableValueQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}