﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.ConfigurableValues.Queries.GetConfigurableValue
{
    public class GetConfigurableValueQuery : IRequest<ConfigurableValueDto>
    {
        public Guid Id { get; set; }
    }

    public class GetConfigurableValueQueryHandler : IRequestHandler<GetConfigurableValueQuery, ConfigurableValueDto>
    {
        private readonly IApplicationDbContext _context;

        public GetConfigurableValueQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ConfigurableValueDto> Handle(GetConfigurableValueQuery request, CancellationToken cancellationToken)
        {
            var configurableValue = await _context.ConfigurableValue.SingleOrDefaultAsync(a => a.Id == request.Id);

            return configurableValue.ToConfigurableValueDto();
        }
    }
}
