﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.UIComponents
{
    public class GetUIComponentsQuery : IRequest<List<UIComponentDto>>
    {
    }

    public class GetUIComponentsQueryHandler : IRequestHandler<GetUIComponentsQuery, List<UIComponentDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetUIComponentsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<UIComponentDto>> Handle(GetUIComponentsQuery request, CancellationToken cancellationToken)
        {
            return await _context.UIComponents.Select(r => r.ToUIComponentDto()).ToListAsync(cancellationToken);
        }
    }
}
