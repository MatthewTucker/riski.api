﻿using FluentValidation;

namespace Riski.Api.Application.UIComponents
{
    public class GetUIComponentQueryValidator : AbstractValidator<GetUIComponentQuery>
    {
        public GetUIComponentQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
