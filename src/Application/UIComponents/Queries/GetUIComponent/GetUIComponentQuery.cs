﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.UIComponents
{
    public class GetUIComponentQuery : IRequest<UIComponentDto>
    {
        public Guid Id { get; set; }
    }

    public class GetUIComponentQueryHandler : IRequestHandler<GetUIComponentQuery, UIComponentDto>
    {
        private readonly IApplicationDbContext _context;

        public GetUIComponentQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UIComponentDto> Handle(GetUIComponentQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.UIComponents.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return record.ToUIComponentDto();
        }
    }
}
