﻿using Riski.Api.Application.Common.Mappings;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
