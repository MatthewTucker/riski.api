﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.DefinedAnswers
{
    public class GetDefinedAnswersQuery : IRequest<List<DefinedAnswerDto>>
    {
    }

    public class GetDefinedAnswersQueryHandler : IRequestHandler<GetDefinedAnswersQuery, List<DefinedAnswerDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetDefinedAnswersQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<DefinedAnswerDto>> Handle(GetDefinedAnswersQuery request, CancellationToken cancellationToken)
        {
            return await _context.DefinedAnswers.Include(da => da.Category).Select(r => r.ToDefinedAnswerDto()).ToListAsync(cancellationToken);
        }
    }
}
