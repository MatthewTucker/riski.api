﻿using FluentValidation;

namespace Riski.Api.Application.DefinedAnswers
{
    public class GetDefinedAnswerQueryValidator : AbstractValidator<GetDefinedAnswerQuery>
    {
        public GetDefinedAnswerQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
