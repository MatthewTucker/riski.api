﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.DefinedAnswers
{
    public class GetDefinedAnswerQuery : IRequest<DefinedAnswerDto>
    {
        public Guid Id { get; set; }
    }

    public class GetDefinedAnswerQueryHandler : IRequestHandler<GetDefinedAnswerQuery, DefinedAnswerDto>
    {
        private readonly IApplicationDbContext _context;

        public GetDefinedAnswerQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DefinedAnswerDto> Handle(GetDefinedAnswerQuery request, CancellationToken cancellationToken)
        {
            var definedAnswer = await _context.DefinedAnswers.Include(da => da.Category).SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return definedAnswer.ToDefinedAnswerDto();
        }
    }
}
