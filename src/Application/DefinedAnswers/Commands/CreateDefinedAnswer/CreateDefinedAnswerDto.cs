﻿using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.DefinedAnswers
{
    public class CreateDefinedAnswerDto
    {
        public string Value { get; set; }
        public string Status { get; set; }
        public DefinedAnswerCategoryDto Category { get; set; }
    }
}
