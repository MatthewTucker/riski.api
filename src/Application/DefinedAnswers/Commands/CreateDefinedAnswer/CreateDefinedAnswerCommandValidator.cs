﻿using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.DefinedAnswerCategories;

using System.Threading;
using System.Threading.Tasks;

namespace Riski.Api.Application.DefinedAnswers
{
    class CreateDefinedAnswerCommandValidator : AbstractValidator<CreateDefinedAnswerCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateDefinedAnswerCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateDefinedAnswerDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateDefinedAnswerDto.Value).NotEmpty();
                    RuleFor(v => v.CreateDefinedAnswerDto.Category).NotEmpty().MustAsync(BeValidCategory).WithMessage("The specified Category does not exists.");
                });
        }

        public async Task<bool> BeValidCategory(DefinedAnswerCategoryDto categoryDto, CancellationToken cancellationToken)
        {
            return await _context.DefinedAnswerCategories.AnyAsync(u => u.Id == categoryDto.Id, cancellationToken);
        }
    }
}

