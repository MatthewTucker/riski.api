﻿using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Riski.Api.Application.DefinedAnswers
{
    public class CreateDefinedAnswerCommand : IRequest<Guid>
    {
        public CreateDefinedAnswerDto CreateDefinedAnswerDto { get; set; }
    }

    public class CreateDefinedAnswerCommandHandler : IRequestHandler<CreateDefinedAnswerCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateDefinedAnswerCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateDefinedAnswerCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateDefinedAnswerDto;
            var definedAnswer = new DefinedAnswer
            {
                Category = await _context.DefinedAnswerCategories.FindAsync(new object[] { requestDto.Category.Id }, cancellationToken),
                Value = requestDto.Value,
            };

            await _context.DefinedAnswers.AddAsync(definedAnswer, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return definedAnswer.Id;
        }
    }
}
