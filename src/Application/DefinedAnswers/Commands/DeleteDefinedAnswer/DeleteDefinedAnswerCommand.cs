﻿using MediatR;

using Riski.Api.Application.Common.Interfaces;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Riski.Api.Application.DefinedAnswers
{
    public class DeleteDefinedAnswerCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteDefinedAnswerCommandHandler : IRequestHandler<DeleteDefinedAnswerCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteDefinedAnswerCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteDefinedAnswerCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var defiendAnswer = await _context.DefinedAnswers.FindAsync(new object[] { request.Id }, cancellationToken);

            _context.DefinedAnswers.Remove(defiendAnswer);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
