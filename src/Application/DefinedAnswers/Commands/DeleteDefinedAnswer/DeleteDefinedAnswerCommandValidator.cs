﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riski.Api.Application.DefinedAnswers
{
    class DeleteDefinedAnswerCommandValidator : AbstractValidator<DeleteDefinedAnswerCommand>
    {
        public DeleteDefinedAnswerCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id body not found");
        }
    }
}
