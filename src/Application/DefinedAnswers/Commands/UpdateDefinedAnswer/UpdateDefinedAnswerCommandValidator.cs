﻿using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Models.Dtos;

using System.Threading;
using System.Threading.Tasks;

namespace Riski.Api.Application.DefinedAnswers
{
    class UpdateDefinedAnswerCommandValidator : AbstractValidator<UpdateDefinedAnswerCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateDefinedAnswerCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateDefinedAnswer)
                .NotNull().WithMessage("Update body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UpdateDefinedAnswer).NotEmpty().MustAsync(BeValidDefinedAnswer).WithMessage("The specified Defined Answer does not exists.");
                    RuleFor(v => v.UpdateDefinedAnswer.Value).NotEmpty();
                    RuleFor(v => v.UpdateDefinedAnswer.Category).NotEmpty().MustAsync(BeValidCategory).WithMessage("The specified Category does not exists.");
                });
        }

        public async Task<bool> BeValidCategory(DefinedAnswerCategoryDto categoryDto, CancellationToken cancellationToken)
        {
            return await _context.DefinedAnswerCategories.AnyAsync(u => u.Id == categoryDto.Id, cancellationToken);
        }

        public async Task<bool> BeValidDefinedAnswer(UpdateDefinedAnswerDto definedAnswerDto, CancellationToken cancellationToken)
        {
            return await _context.DefinedAnswers.AnyAsync(u => u.Id == definedAnswerDto.Id, cancellationToken);
        }
    }
}

