﻿using Riski.Api.Application.Common.Models.Dtos;

using System;

namespace Riski.Api.Application.DefinedAnswers
{
    public class UpdateDefinedAnswerDto
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
        public DefinedAnswerCategoryDto Category { get; set; }
    }
}
