﻿using MediatR;

using Riski.Api.Application.Common.Interfaces;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Riski.Api.Application.DefinedAnswers
{
    public class UpdateDefinedAnswerCommand : IRequest<Guid>
    {
        public UpdateDefinedAnswerDto UpdateDefinedAnswer { get; set; }
    }

    public class UpdateDefinedAnswerCommandHandler : IRequestHandler<UpdateDefinedAnswerCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateDefinedAnswerCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateDefinedAnswerCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateDefinedAnswer;

            var definedAnswer = await _context.DefinedAnswers.FindAsync(requestDto.Id);
            definedAnswer.Value = requestDto.Value;
            definedAnswer.Category = await _context.DefinedAnswerCategories.FindAsync(new object[] { requestDto.Category.Id }, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return definedAnswer.Id;
        }
    }
}
