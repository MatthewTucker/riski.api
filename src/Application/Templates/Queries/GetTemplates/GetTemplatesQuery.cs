﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.Templates
{
    public class GetTemplatesQuery : IRequest<List<TemplateDto>>
    {
    }

    public class GetTemplatesQueryHandler : IRequestHandler<GetTemplatesQuery, List<TemplateDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetTemplatesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<TemplateDto>> Handle(GetTemplatesQuery request, CancellationToken cancellationToken)
        {
            return await _context.Templates
                .Include(t => t.FormPages)
                    .ThenInclude(fp => fp.FormPageType)
                .Include(t => t.FormPages)
                    .ThenInclude(fp => fp.AlertDialogs)
                        .ThenInclude(ad => ad.EventCondition)
                .Include(t => t.FormPages)
                    .ThenInclude(fp => fp.Questions)
                        .ThenInclude(q => q.FormPage)
                            .ThenInclude(fp => fp.Template)
                .Include(t => t.FormPages)
                    .ThenInclude(fp => fp.UIComponents)
                .Select(r => r.ToTemplateDto())
                .ToListAsync();
        }
    }
}
