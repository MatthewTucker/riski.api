﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Templates
{
    public class GetTemplateQuery : IRequest<TemplateDto>
    {
        public Guid Id { get; set; }
    }

    public class GetTemplateQueryHandler : IRequestHandler<GetTemplateQuery, TemplateDto>
    {
        private readonly IApplicationDbContext _context;

        public GetTemplateQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<TemplateDto> Handle(GetTemplateQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.Templates
                .Include(t => t.FormPages)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return record.ToTemplateDto();
        }
    }
}
