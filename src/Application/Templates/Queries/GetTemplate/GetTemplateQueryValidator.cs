﻿using FluentValidation;

namespace Riski.Api.Application.Templates
{
    public class GetTemplateQueryValidator : AbstractValidator<GetTemplateQuery>
    {
        public GetTemplateQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
