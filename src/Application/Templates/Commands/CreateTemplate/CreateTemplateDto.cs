﻿using System.Collections.Generic;

using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Templates
{
    public class CreateTemplateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public List<FormPageDto> FormPages { get; set; }
    }
}
