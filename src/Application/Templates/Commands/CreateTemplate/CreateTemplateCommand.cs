﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Templates
{
    public class CreateTemplateCommand : IRequest<Guid>
    {
        public CreateTemplateDto CreateTemplate { get; set; }
    }

    public class CreateTemplateCommandHandler : IRequestHandler<CreateTemplateCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateTemplateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateTemplateCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateTemplate;

            var formPageIds = requestDto.FormPages?.Select(f => f.Id).ToList() ?? new List<Guid>();
            var formPages = new List<FormPage>();

            if (formPageIds.Any())
            {
                formPages = await _context.FormPages.Where(r => formPageIds.Contains(r.Id)).ToListAsync(cancellationToken);
            }

            var Template = new Template
            {
                Name = requestDto.Name,
                Description = requestDto.Description,
                Type = requestDto.Type,
                FormPages = formPages,
                Status = PersistenceStatus.Available
            };

            await _context.Templates.AddAsync(Template, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Template.Id;
        }
    }
}
