﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Templates
{
    class CreateTemplateCommandValidator : AbstractValidator<CreateTemplateCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateTemplateCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateTemplate).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateTemplate.Name).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("Template already exists with this Name");
                    RuleFor(v => v.CreateTemplate.Description).NotEmpty();
                    RuleFor(v => v.CreateTemplate.Type).NotEmpty();
                    RuleFor(v => v.CreateTemplate.FormPages).NotNull();
                });
        }

        public async Task<bool> IsNotDuplicate(string name, CancellationToken cancellationToken)
        {
            return await _context.Templates.AnyAsync(r => r.Name == name, cancellationToken);
        }
    }
}

