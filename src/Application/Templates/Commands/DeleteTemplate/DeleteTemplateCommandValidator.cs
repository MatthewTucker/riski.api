﻿using FluentValidation;

namespace Riski.Api.Application.Templates
{
    class DeleteTemplateCommandValidator : AbstractValidator<DeleteTemplateCommand>
    {
        public DeleteTemplateCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
