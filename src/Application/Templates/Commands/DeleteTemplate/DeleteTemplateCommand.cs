﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Templates
{
    public class DeleteTemplateCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteTemplateCommandHandler : IRequestHandler<DeleteTemplateCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteTemplateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteTemplateCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var record = await _context.Templates.Include(t => t.FormPages).SingleOrDefaultAsync(t => t.Id == request.Id);

            record.Status = PersistenceStatus.Deleted;

            foreach (var formPage in record.FormPages)
            {
                formPage.Status = PersistenceStatus.Deleted;
            }

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
