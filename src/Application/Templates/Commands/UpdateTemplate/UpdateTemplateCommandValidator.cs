﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Templates
{
    class UpdateTemplateCommandValidator : AbstractValidator<UpdateTemplateCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateTemplateCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateTemplate).NotNull().WithMessage("Update body not found").DependentRules(() =>
            {

                RuleFor(v => v.UpdateTemplate.Id).NotEmpty().MustAsync(IsValidId).WithMessage("Invalid Id");
                RuleFor(v => v.UpdateTemplate.Name).NotEmpty();
                RuleFor(v => v.UpdateTemplate.Description).NotEmpty();
                RuleFor(v => v.UpdateTemplate.Type).NotEmpty();
                RuleFor(v => v.UpdateTemplate.FormPages).NotNull();
            });
        }

        public async Task<bool> IsValidId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Templates.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}


