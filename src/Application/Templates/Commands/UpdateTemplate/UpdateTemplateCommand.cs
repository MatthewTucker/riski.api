﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Templates
{
    public class UpdateTemplateCommand : IRequest<Guid>
    {
        public UpdateTemplateDto UpdateTemplate { get; set; }
    }

    public class UpdateTemplateCommandHandler : IRequestHandler<UpdateTemplateCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateTemplateCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateTemplateCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateTemplate;

            var oldRecord = await _context.Templates.FindAsync(requestDto.Id);
            oldRecord.Status = PersistenceStatus.Deleted;

            var formPageIds = requestDto.FormPages.Select(f => f.Id).ToList();

            var formPages = await _context.FormPages.Where(r => formPageIds.Contains(r.Id)).ToListAsync(cancellationToken);

            var template = new Template
            {
                Name = requestDto.Name,
                Description = requestDto.Description,
                Type = requestDto.Type,
                FormPages = formPages,
                Status = PersistenceStatus.Available
            };

            await _context.Templates.AddAsync(template);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return template.Id;
        }
    }
}
