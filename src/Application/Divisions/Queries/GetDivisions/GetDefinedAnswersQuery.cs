﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Divisions
{
    public class GetDivisionsQuery : IRequest<List<DivisionDto>>
    {
    }

    public class GetDivisionsQueryHandler : IRequestHandler<GetDivisionsQuery, List<DivisionDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetDivisionsQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<DivisionDto>> Handle(GetDivisionsQuery request, CancellationToken cancellationToken)
        {
            return await _context.Divisions.Select(r => r.ToDivisionDto()).ToListAsync();
        }
    }
}
