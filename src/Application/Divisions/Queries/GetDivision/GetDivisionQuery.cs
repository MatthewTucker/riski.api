﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Divisions
{
    public class GetDivisionQuery : IRequest<DivisionDto>
    {
        public Guid Id { get; set; }
    }

    public class GetDivisionQueryHandler : IRequestHandler<GetDivisionQuery, DivisionDto>
    {
        private readonly IApplicationDbContext _context;

        public GetDivisionQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DivisionDto> Handle(GetDivisionQuery request, CancellationToken cancellationToken)
        {
            var division = await _context.Divisions.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return division.ToDivisionDto();
        }
    }
}
