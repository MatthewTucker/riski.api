﻿using FluentValidation;

namespace Riski.Api.Application.Divisions
{
    public class GetDivisionQueryValidator : AbstractValidator<GetDivisionQuery>
    {
        public GetDivisionQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
