﻿using MediatR;

using Riski.Api.Application.Common.Interfaces;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Riski.Api.Application.Divisions
{
    public class UpdateDivisionCommand : IRequest<Guid>
    {
        public UpdateDivisionDto UpdateDivision { get; set; }
    }

    public class UpdateDivisionCommandHandler : IRequestHandler<UpdateDivisionCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateDivisionCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateDivisionCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateDivision;

            var Division = await _context.Divisions.FindAsync(requestDto.Id);
            Division.Name = requestDto.Name;
            Division.Description = requestDto.Description;

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Division.Id;
        }
    }
}
