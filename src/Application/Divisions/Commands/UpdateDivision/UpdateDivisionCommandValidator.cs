﻿using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

using System.Threading;
using System.Threading.Tasks;

namespace Riski.Api.Application.Divisions
{
    class UpdateDivisionCommandValidator : AbstractValidator<UpdateDivisionCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateDivisionCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateDivision)
                .NotNull().WithMessage("Update body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UpdateDivision).NotEmpty().MustAsync(BeValidDivision).WithMessage(x => $"A Division with the name: {x.UpdateDivision.Name} already exists");
                    RuleFor(v => v.UpdateDivision.Name).NotEmpty();
                    RuleFor(v => v.UpdateDivision.Description).NotEmpty();
                });
        }

        public async Task<bool> BeValidCategory(UpdateDivisionDto categoryDto, CancellationToken cancellationToken)
        {
            return await _context.Divisions.AnyAsync(u => u.Id == categoryDto.Id, cancellationToken);
        }

        public async Task<bool> BeValidDivision(UpdateDivisionDto DivisionDto, CancellationToken cancellationToken)
        {
            return await _context.Divisions.AnyAsync(u => u.Id == DivisionDto.Id, cancellationToken);
        }
    }
}

