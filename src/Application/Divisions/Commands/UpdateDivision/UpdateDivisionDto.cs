﻿using System;

namespace Riski.Api.Application.Divisions
{
    public class UpdateDivisionDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}
