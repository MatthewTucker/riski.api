﻿using FluentValidation;

namespace Riski.Api.Application.DefinedAnswers
{
    class DeleteDivisionCommandValidator : AbstractValidator<DeleteDefinedAnswerCommand>
    {
        public DeleteDivisionCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
