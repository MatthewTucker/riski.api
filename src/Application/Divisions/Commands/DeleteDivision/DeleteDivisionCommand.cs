﻿using MediatR;

using Riski.Api.Application.Common.Interfaces;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Riski.Api.Application.Divisions
{
    public class DeleteDivisionCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteDivisionCommandHandler : IRequestHandler<DeleteDivisionCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteDivisionCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteDivisionCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var defiendAnswer = await _context.Divisions.FindAsync(request.Id);

            _context.Divisions.Remove(defiendAnswer);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
