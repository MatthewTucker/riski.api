﻿using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

using System.Threading;
using System.Threading.Tasks;

namespace Riski.Api.Application.Divisions
{
    class CreateDivisionsCommandValidator : AbstractValidator<CreateDivisionCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateDivisionsCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.CreateDivisionDto)
                .NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.CreateDivisionDto).MustAsync(DoesNotExist).WithMessage("The specified Division already exists.");
                    RuleFor(v => v.CreateDivisionDto.Name).NotEmpty();
                    RuleFor(v => v.CreateDivisionDto.Description).NotEmpty();
                });
        }

        public async Task<bool> DoesNotExist(CreateDivisionDto division, CancellationToken cancellationToken)
        {
            return await _context.Divisions.AnyAsync(u => u.Name == division.Name, cancellationToken);
        }
    }
}

