﻿namespace Riski.Api.Application.Divisions
{
    public class CreateDivisionDto
    {
        public string Description { get; set; }
        public string Name { get; set; }
    }
}
