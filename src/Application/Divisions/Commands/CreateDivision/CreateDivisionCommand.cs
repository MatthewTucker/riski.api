﻿using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Riski.Api.Application.Divisions
{
    public class CreateDivisionCommand : IRequest<Guid>
    {
        public CreateDivisionDto CreateDivisionDto { get; set; }
    }

    public class CreateDivisionCommandHandler : IRequestHandler<CreateDivisionCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateDivisionCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateDivisionCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateDivisionDto;
            var Divisions = new Division
            {
                Name = requestDto.Name,
                Description = requestDto.Description
            };

            await _context.Divisions.AddAsync(Divisions, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Divisions.Id;
        }
    }
}
