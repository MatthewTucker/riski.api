﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Risks
{
    public class ClearRiskCommand : IRequest<Unit>
    {
        public Guid Id { get; set; }
    }

    public class ClearRiskCommandHandler : IRequestHandler<ClearRiskCommand, Unit>
    {
        private readonly IApplicationDbContext _context;

        public ClearRiskCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(ClearRiskCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var risk = await _context.Risks.SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            risk.RiskStatus = "Cleared";

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
