﻿using FluentValidation;

namespace Riski.Api.Application.Risks
{
    public class ClearRiskCommandValidator : AbstractValidator<ClearRiskCommand>
    {
        public ClearRiskCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithMessage("Id is required.");
        }
    }
}
