﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.Risks
{
    public class GetRisksQuery : IRequest<List<RiskDto>>
    {
    }

    public class GetRisksQueryHandler : IRequestHandler<GetRisksQuery, List<RiskDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetRisksQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<RiskDto>> Handle(GetRisksQuery request, CancellationToken cancellationToken)
        {
            return await _context.Risks
                .Include(r => r.Likelihood)
                .Include(r => r.Consequence)
                .Include(r => r.PostMigitationLikelihood)
                .Include(r => r.PostMitigationConsequence)
                .Include(r => r.Site)
                .Include(r => r.Type)
                .Select(r => r.ToRiskDto()).ToListAsync(cancellationToken);
        }
    }
}
