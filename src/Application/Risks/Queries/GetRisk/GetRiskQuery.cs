﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Risks
{
    public class GetRiskQuery : IRequest<RiskDto>
    {
        public Guid Id { get; set; }
    }

    public class GetRiskQueryHandler : IRequestHandler<GetRiskQuery, RiskDto>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<RiskDto> Handle(GetRiskQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.Risks
                .Include(r => r.Likelihood)
                .Include(r => r.Consequence)
                .Include(r => r.PostMigitationLikelihood)
                .Include(r => r.PostMitigationConsequence)
                .Include(r => r.Site)
                .Include(r => r.Type)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return record.ToRiskDto();
        }
    }
}
