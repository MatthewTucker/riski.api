﻿using FluentValidation;

namespace Riski.Api.Application.Risks
{
    public class GetRiskQueryValidator : AbstractValidator<GetRiskQuery>
    {
        public GetRiskQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
