﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Risks
{
    class GetCountOfRisksFoundQueryValidator : AbstractValidator<GetCountOfRisksFoundQuery>
    {
        private readonly IApplicationDbContext _context;

        public GetCountOfRisksFoundQueryValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.SiteId).MustAsync(IsValidSiteId).WithMessage("Invalid SiteId");
            RuleFor(v => v.DateTime).NotEmpty();
        }

        public async Task<bool> IsValidSiteId(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Sites.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}