﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Risks
{
    public class GetCountOfRisksFoundQuery : IRequest<int>
    {
        public Guid SiteId { get; set; }
        public DateTime DateTime { get; set; }
    }

    public class GetCountOfRisksFoundQueryHandler : IRequestHandler<GetCountOfRisksFoundQuery, int>
    {
        private readonly IApplicationDbContext _context;

        public GetCountOfRisksFoundQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(GetCountOfRisksFoundQuery request, CancellationToken cancellationToken)
        {
            var siteId = request.SiteId;
            var date = request.DateTime;
            return await _context.Risks.CountAsync(r => r.Site.Id == siteId && r.Timestamp.Date == date.Date);
        }
    }
}
