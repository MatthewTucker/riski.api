﻿using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.Users
{
    public class LoginQuery : IRequest<UserDto>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class LoginQueryHandler : IRequestHandler<LoginQuery, UserDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IIdentityService _identityService;

        public LoginQueryHandler(IApplicationDbContext context, IIdentityService identityService)
        {
            _context = context;
            _identityService = identityService;
        }

        public async Task<UserDto> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.Username == request.Username);
            if (user == null)
            {
                return null;
            }

            var token = await _identityService.AuthenticateAsync(user.Email.Value, request.Password);
            var identityId = await _identityService.Login(token);

            if (user.IdentityId != identityId)
            {
                return null;
            }

            return user.ToUserDto();
        }
    }
}
