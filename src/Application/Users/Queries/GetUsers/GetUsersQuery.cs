﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.Users
{
    public class GetUsersQuery : IRequest<List<UserDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, List<UserDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetUsersQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<UserDto>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            return await _context.Users.Where(u => u.Status == PersistenceStatus.Available).Select(r => r.ToUserDto()).ToListAsync(cancellationToken);
        }
    }
}
