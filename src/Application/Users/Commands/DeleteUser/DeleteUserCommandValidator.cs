﻿using FluentValidation;

namespace Riski.Api.Application.Users.Commands.DeleteUser
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator()
        {
            RuleFor(v => v.Id).NotEmpty().WithMessage("Must provide Id");
        }
    }
}
