﻿using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;
using Riski.Api.Domain.ValueObjects;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace Riski.Api.Application.Users
{
    public class UpdateUserCommand : IRequest
    {
        public UpdateUserDto UserDto { get; set; }
    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand>
    {

        private readonly IApplicationDbContext _context;
        private readonly IIdentityService _identityService;

        public UpdateUserCommandHandler(IApplicationDbContext context, IIdentityService identityService)
        {
            _context = context;
            _identityService = identityService;
        }

        public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var userDto = request.UserDto;

            var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == userDto.Id);

            if (user == null)
            {
                throw new Exception($"No User found with Id: {userDto.Id}");
            }

            if (!string.IsNullOrEmpty(userDto.Password) && userDto.Password == userDto.ConfirmPassword)
            {
                await _identityService.UpdatePasswordAsync(user.IdentityId, userDto.Password);
            }

            user.FirstName = userDto.FirstName;
            user.LastName = userDto.LastName;
            user.Username = userDto.Username;
            user.PhoneNumber = new PhoneNumber(userDto.Mobile);
            user.Email = new Email(userDto.Email);
            user.UserType = Enum.Parse<UserType>(userDto.Type);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
