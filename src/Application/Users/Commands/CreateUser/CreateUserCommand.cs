﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;
using Riski.Api.Domain.ValueObjects;

namespace Riski.Api.Application.Users
{
    public class CreateUserCommand : IRequest<Guid>
    {
        public CreateUserDto UserDto { get; set; }
    }

    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Guid>
    {

        private readonly IApplicationDbContext _context;
        private readonly IIdentityService _identityService;

        public CreateUserCommandHandler(IApplicationDbContext context, IIdentityService identityService)
        {
            _context = context;
            _identityService = identityService;
        }

        public async Task<Guid> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var userDto = request.UserDto;

            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var identityResult = await _identityService.CreateUserAsync(userDto.Username, userDto.Password);

            if (!identityResult.Result.Succeeded)
            {
                var errors = string.Join(Environment.NewLine, identityResult.Result.Errors);
                throw new Exception($"Unable to create user. Errors: {errors}");
            }

            var user = new User
            {
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Username = userDto.Username,
                PhoneNumber = new PhoneNumber(userDto.Mobile),
                Email = new Email(userDto.Email),
                UserType = Enum.Parse<UserType>(userDto.Type),
                IdentityId = identityResult.UserId
            };

            await _context.Users.AddAsync(user, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return user.Id;
        }
    }
}
