﻿using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.Users
{
    class CreateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateUserCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UserDto)
                .NotNull().WithMessage("Update body not found").DependentRules(() =>
                {
                    RuleFor(v => v.UserDto.FirstName).NotEmpty();
                    RuleFor(v => v.UserDto.LastName).NotEmpty();
                    RuleFor(v => v.UserDto.Mobile).NotEmpty();
                    RuleFor(v => v.UserDto.Email).EmailAddress();
                    RuleFor(v => v.UserDto.Password).NotEmpty();
                    RuleFor(v => v.UserDto.ConfirmPassword).NotEmpty();
                    RuleFor(v => v.UserDto.Password).Equal(v => v.UserDto.ConfirmPassword);
                    RuleFor(v => v.UserDto.Username).NotEmpty().MustAsync(BeUniqueUsername).WithMessage("The specified Username already exists.");
                });
        }

        public async Task<bool> BeUniqueUsername(string username, CancellationToken cancellationToken)
        {
            return await _context.Users.AllAsync(u => u.Username != username);
        }
    }
}
