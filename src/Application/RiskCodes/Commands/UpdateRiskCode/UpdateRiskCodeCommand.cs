﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskCodes
{
    public class UpdateRiskCodeCommand : IRequest<Guid>
    {
        public UpdateRiskCodeDto UpdateRiskCode { get; set; }
    }

    public class UpdateRiskCodeCommandHandler : IRequestHandler<UpdateRiskCodeCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public UpdateRiskCodeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(UpdateRiskCodeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.UpdateRiskCode;

            var oldRecord = await _context.RiskCodes.FindAsync(requestDto.Id);
            oldRecord.Status = PersistenceStatus.Deleted;

            var riskCode = new RiskCode
            {
                Code = requestDto.Code,
                Likelihood = await _context.RiskConfigurations.FindAsync(requestDto.Likelihood.Id),
                Consequence = await _context.RiskConfigurations.FindAsync(requestDto.Consequence.Id),
                Status = PersistenceStatus.Available
            };

            await _context.RiskCodes.AddAsync(riskCode, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return riskCode.Id;
        }
    }
}
