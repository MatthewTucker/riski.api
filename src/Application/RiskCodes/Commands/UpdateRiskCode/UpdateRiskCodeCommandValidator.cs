﻿using System;
using System.Threading;
using System.Threading.Tasks;

using FluentValidation;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskCodes
{
    class UpdateRiskCodeCommandValidator : AbstractValidator<UpdateRiskCodeCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateRiskCodeCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.UpdateRiskCode).NotNull().WithMessage("Create body not found").DependentRules(() =>
            {
                RuleFor(v => v.UpdateRiskCode.Code).NotEmpty().MustAsync(IsNotDuplicate).WithMessage("RiskCode already exists with this Code");
                RuleFor(v => v.UpdateRiskCode.Likelihood).NotNull().DependentRules(() =>
                {
                    RuleFor(v => v.UpdateRiskCode.Likelihood.Id).NotEmpty().MustAsync(IsValidRiskConfiguration).WithMessage("Likelihood is not valid");
                });
                RuleFor(v => v.UpdateRiskCode.Consequence).NotNull().DependentRules(() =>
                {
                    RuleFor(v => v.UpdateRiskCode.Consequence.Id).NotEmpty().MustAsync(IsValidRiskConfiguration).WithMessage("Consequence is not valid");
                });
            });
        }

        public async Task<bool> IsNotDuplicate(string code, CancellationToken cancellationToken)
        {
            return await _context.RiskCodes.AnyAsync(r => r.Code == code, cancellationToken);
        }

        public async Task<bool> IsValidRiskConfiguration(Guid id, CancellationToken cancellationToken)
        {
            return await _context.RiskConfigurations.AnyAsync(r => r.Id == id, cancellationToken);
        }
    }
}

