﻿using System.Collections.Generic;

namespace Riski.Api.Application.RiskCodes
{
    public class CreateRiskCodesDto
    {
        public List<CreateRiskCodeDto> RiskCodes { get; set; }
    }
}
