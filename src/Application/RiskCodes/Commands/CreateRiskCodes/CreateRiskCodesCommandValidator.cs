﻿using FluentValidation;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Application.RiskCodes
{
    class CreateRiskCodesCommandValidator : AbstractValidator<CreateRiskCodesCommand>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskCodesCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v).NotNull().WithMessage("Create body not found").DependentRules(() =>
                {
                    RuleFor(v => v.RiskCodes).NotEmpty();
                });
        }
    }
}

