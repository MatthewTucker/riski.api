﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskCodes
{
    public class CreateRiskCodesCommand : IRequest<Unit>
    {
        public List<CreateRiskCodeDto> RiskCodes { get; set; }
    }

    public class CreateRiskCodesCommandHandler : IRequestHandler<CreateRiskCodesCommand, Unit>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskCodesCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(CreateRiskCodesCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var riskCodeDtos = request.RiskCodes;

            var riskCodes = new List<RiskCode>();
            foreach (var riskCodeDto in riskCodeDtos)
            {
                var riskCode = new RiskCode
                {
                    Code = riskCodeDto.Code,
                    Likelihood = await _context.RiskConfigurations.FindAsync(riskCodeDto.Likelihood.Id),
                    Consequence = await _context.RiskConfigurations.FindAsync(riskCodeDto.Consequence.Id),
                    Status = PersistenceStatus.Available
                };

                riskCodes.Add(riskCode);
            }

            await _context.RiskCodes.AddRangeAsync(riskCodes, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
