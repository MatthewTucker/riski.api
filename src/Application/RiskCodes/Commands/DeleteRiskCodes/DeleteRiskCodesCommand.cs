﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskCodes
{
    public class DeleteRiskCodesCommand : IRequest<Unit>
    {
    }

    public class DeleteRiskCodesCommandHandler : IRequestHandler<DeleteRiskCodesCommand, Unit>
    {

        private readonly IApplicationDbContext _context;

        public DeleteRiskCodesCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteRiskCodesCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var records = await _context.RiskCodes.Where(r => r.Status == PersistenceStatus.Available).ToListAsync();

            foreach (var record in records)
            {
                record.Status = PersistenceStatus.Deleted;
            }

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
