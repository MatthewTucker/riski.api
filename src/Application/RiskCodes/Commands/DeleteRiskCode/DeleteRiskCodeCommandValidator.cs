﻿using FluentValidation;

namespace Riski.Api.Application.RiskCodes
{
    class DeleteRiskCodeCommandValidator : AbstractValidator<DeleteRiskCodeCommand>
    {
        public DeleteRiskCodeCommandValidator()
        {
            RuleFor(v => v.Id).NotNull().WithMessage("Id not found");
        }
    }
}
