﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskCodes
{
    public class DeleteRiskCodeCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteRiskCodeCommandHandler : IRequestHandler<DeleteRiskCodeCommand>
    {

        private readonly IApplicationDbContext _context;

        public DeleteRiskCodeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteRiskCodeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var record = await _context.RiskCodes.FindAsync(request.Id);

            record.Status = PersistenceStatus.Deleted;

            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return Unit.Value;
        }
    }
}
