﻿using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.RiskCodes
{
    public class CreateRiskCodeDto
    {
        public string Code { get; set; }
        public RiskConfigurationDto Likelihood { get; set; }
        public RiskConfigurationDto Consequence { get; set; }
    }
}
