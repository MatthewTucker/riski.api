﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

using MediatR;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;

namespace Riski.Api.Application.RiskCodes
{
    public class CreateRiskCodeCommand : IRequest<Guid>
    {
        public CreateRiskCodeDto CreateRiskCode { get; set; }
    }

    public class CreateRiskCodeCommandHandler : IRequestHandler<CreateRiskCodeCommand, Guid>
    {

        private readonly IApplicationDbContext _context;

        public CreateRiskCodeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Guid> Handle(CreateRiskCodeCommand request, CancellationToken cancellationToken)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var requestDto = request.CreateRiskCode;
            var RiskCode = new RiskCode
            {
                Code = requestDto.Code,
                Likelihood = await _context.RiskConfigurations.FindAsync(requestDto.Likelihood.Id),
                Consequence = await _context.RiskConfigurations.FindAsync(requestDto.Consequence.Id),
                Status = PersistenceStatus.Available
            };

            await _context.RiskCodes.AddAsync(RiskCode, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            transaction.Complete();

            return RiskCode.Id;
        }
    }
}
