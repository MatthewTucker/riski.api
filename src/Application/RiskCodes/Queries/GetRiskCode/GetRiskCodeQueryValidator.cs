﻿using FluentValidation;

namespace Riski.Api.Application.RiskCodes
{
    public class GetRiskCodeQueryValidator : AbstractValidator<GetRiskCodeQuery>
    {
        public GetRiskCodeQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty().WithMessage("Id is required.");
        }
    }
}
