﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;

namespace Riski.Api.Application.RiskCodes
{
    public class GetRiskCodeQuery : IRequest<RiskCodeDto>
    {
        public Guid Id { get; set; }
    }

    public class GetRiskCodeQueryHandler : IRequestHandler<GetRiskCodeQuery, RiskCodeDto>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskCodeQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<RiskCodeDto> Handle(GetRiskCodeQuery request, CancellationToken cancellationToken)
        {
            var riskCode = await _context.RiskCodes
                .Include(rc => rc.Consequence)
                .Include(rc => rc.Likelihood)
                .SingleOrDefaultAsync(a => a.Id == request.Id, cancellationToken);

            return riskCode.ToRiskCodeDto();
        }
    }
}
