﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Mappings;
using Riski.Api.Application.Common.Models.Dtos;


namespace Riski.Api.Application.RiskCodes
{
    public class GetRiskCodesQuery : IRequest<List<RiskCodeDto>>
    {
    }

    public class GetRiskCodesQueryHandler : IRequestHandler<GetRiskCodesQuery, List<RiskCodeDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetRiskCodesQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<RiskCodeDto>> Handle(GetRiskCodesQuery request, CancellationToken cancellationToken)
        {
            return await _context.RiskCodes
                .Include(rc => rc.Consequence)
                .Include(rc => rc.Likelihood)
                .Select(r => r.ToRiskCodeDto()).ToListAsync(cancellationToken);
        }
    }
}
