﻿using System.Linq;
using System.Threading.Tasks;
using System;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Models;
using Riski.Api.Domain;

//namespace Riski.Api.Identity
//{
    //public class IdentityService : IIdentityService
    //{
        //private readonly UserManager<ApplicationUser> _userManager;
        //private readonly IUserClaimsPrincipalFactory<ApplicationUser> _userClaimsPrincipalFactory;
        //private readonly IAuthorizationService _authorizationService;

        //public IdentityService(
        //    UserManager<ApplicationUser> userManager,
        //    IUserClaimsPrincipalFactory<ApplicationUser> userClaimsPrincipalFactory,
        //    IAuthorizationService authorizationService)
        //{
        //    _userManager = userManager;
        //    _userClaimsPrincipalFactory = userClaimsPrincipalFactory;
        //    _authorizationService = authorizationService;
        //}

        //public async Task<string> GetUserNameAsync(string userId)
        //{
        //    var user = await _userManager.Users.FirstAsync(u => u.Id == userId);

        //    return user.UserName;
        //}

        //public async Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password)
        //{
        //    var user = new ApplicationUser
        //    {
        //        UserName = userName,
        //        Email = userName,
        //    };

        //    var result = await _userManager.CreateAsync(user, password);

        //    return (result.ToApplicationResult(), user.Id);
        //}

        //public async Task<bool> IsInRoleAsync(string userId, string role)
        //{
        //    var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        //    return await _userManager.IsInRoleAsync(user, role);
        //}

        //public async Task<bool> AuthorizeAsync(string userId, string policyName)
        //{
        //    var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        //    var principal = await _userClaimsPrincipalFactory.CreateAsync(user);

        //    var result = await _authorizationService.AuthorizeAsync(principal, policyName);

        //    return result.Succeeded;
        //}

        //public async Task<Result> DeleteUserAsync(string userId)
        //{
        //    var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

        //    if (user != null)
        //    {
        //        return await DeleteUserAsync(user);
        //    }

        //    return Result.Success();
        //}

        //public async Task<Result> DeleteUserAsync(ApplicationUser user)
        //{
        //    var result = await _userManager.DeleteAsync(user);

        //    return result.ToApplicationResult();
        //}

        //public async Task<Guid> Login(string username, string password)
        //{
        //    var identity = await _userManager.FindByNameAsync(username);

        //    if (identity == null)
        //    {
        //        return Guid.Empty;
        //    }

        //    return await _userManager.CheckPasswordAsync(identity, password)
        //        ? Guid.Parse(identity.Id)
        //        : Guid.Empty;
        //}

        //public async Task<Result> UpdatePasswordAsync(string identityId, string password)
        //{
        //    var identity = await _userManager.FindByIdAsync(identityId);

        //    await _userManager.RemovePasswordAsync(identity);
        //    await _userManager.AddPasswordAsync(identity, password);

        //    return Result.Success();
        //}

        //public Task<string> Login(AccessToken token)
        //{
        //    throw new NotImplementedException();
        //}

        //public Task<AccessToken> AuthenticateAsync(string username, string password)
        //{
        //    throw new NotImplementedException();
        //}
//    }
//}
