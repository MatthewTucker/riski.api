﻿using Identity.Auth0;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Riski.Api.Application.Common.Interfaces;

namespace Riski.Api.Identity
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IIdentityService, Auth0IdentityService>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = "https://riskiapp.au.auth0.com/";
                options.Audience = "api.riski.com.au";
            });

            services.AddHttpClient();

            return services;
        }
    }
}