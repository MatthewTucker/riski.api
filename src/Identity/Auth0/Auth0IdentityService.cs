﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Application.Common.Models;
using Riski.Api.Domain;

namespace Identity.Auth0
{
    public class Auth0IdentityService : IIdentityService
    {
        private readonly IApplicationDbContext _context;
        private readonly IHttpClientFactory _httpClientFactory;

        public Auth0IdentityService(IApplicationDbContext context, IHttpClientFactory httpClientFactory)
        {
            _context = context;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<AccessToken> AuthenticateAsync(string username, string password)
        {
            using var client = _httpClientFactory.CreateClient();
            var dict = new Dictionary<string, string>
            {
                { "grant_type" , "password" },
                { "client_id", "bI8ayM2LOMJGsKcZhROHD1b6KbRZ6DFJ"},
                { "client_secret", "1jNS8zstry1ZdfySErqnfr8KJQn_8gM9Mg23m4R2TMzhtHZVf5RiHwwZf0isYBeX"},
                { "audience", "api.riski.com.au"},
                { "username", username},
                { "password", password},
                { "scope", "openid" }
            };

            var response = await client.PostAsync("https://riskiapp.au.auth0.com/oauth/token", new FormUrlEncodedContent(dict));

            if (!response.IsSuccessStatusCode)
            {
                var errorMessage = await response.Content.ReadAsStringAsync();
                Console.WriteLine(errorMessage);
                return null;
            }

            var jsonString = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<AccessToken>(jsonString);
        }

        public Task<bool> AuthorizeAsync(string userId, string policyName)
        {
            return new Task<bool>(() => true);
        }

        public async Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password)
        {
            using var client = _httpClientFactory.CreateClient();

            var payload = new
            {
                client_id = "bI8ayM2LOMJGsKcZhROHD1b6KbRZ6DFJ",
                email = $"{userName}@riski.com.au",
                password = password,
                connection = "Username-Password-Authentication"
            };

            var response = await client.PostAsync($"https://riskiapp.au.auth0.com/dbconnections/signup", new StringContent(JsonSerializer.Serialize(payload), Encoding.UTF8, "application/json"));

            var responseMsg = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {

                throw new Exception(responseMsg);
            }

            var signUpResult = JsonSerializer.Deserialize<SignUpResult>(responseMsg);

            return (Result: Result.Success(), UserId: signUpResult._id);
        }

        public async Task<Result> DeleteUserAsync(string userId)
        {
            var accessToken = await GetManagementTokenAsync();

            if (accessToken == null)
            {
                return Result.Failure(new[] { "Unable to retrieve acceess token from Auth0" });
            }

            using var client = _httpClientFactory.CreateClient();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(accessToken.token_type, accessToken.access_token);

            var response = await client.DeleteAsync($"https://riskiapp.au.auth0.com/api/v2/users/auth0|{userId}");

            if (!response.IsSuccessStatusCode)
            {
                var errors = await response.Content.ReadAsStringAsync();
                return Result.Failure(new[] { errors });
            }

            return Result.Success();
        }

        public async Task<string> GetUserNameAsync(string userId)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.IdentityId == userId);

            return user?.Username ?? "Username not found";
        }

        public Task<bool> IsInRoleAsync(string userId, string role)
        {
            return new Task<bool>(() => true);
        }

        public async Task<string> Login(AccessToken token)
        {
            using var client = _httpClientFactory.CreateClient();

            client.DefaultRequestHeaders.Add("Authorization", $"{token.token_type} {token.access_token}");

            var response = await client.GetAsync("https://riskiapp.au.auth0.com/userinfo");
            var json = await response.Content.ReadAsStringAsync();

            var userInfo = JsonSerializer.Deserialize<UserInfo>(json);

            return userInfo.sub;
        }

        public async Task<Result> UpdatePasswordAsync(string userId, string password)
        {
            using var client = _httpClientFactory.CreateClient();

            var payload = new
            {
                password = password,
                connection = "Username-Password-Authentication"
            };

            client.DefaultRequestHeaders.Add("content-type", "application/json");

            var response = await client.PatchAsync($"https://riskiapp.au.auth0.com/api/v2/users/{userId}", new StringContent(JsonSerializer.Serialize(payload)));

            if (!response.IsSuccessStatusCode)
            {
                var errors = await response.Content.ReadAsStringAsync();
                return Result.Failure(new[] { errors });
            }

            return Result.Success();

        }

        private async Task<AccessToken> GetManagementTokenAsync()
        {
            using var client = _httpClientFactory.CreateClient();
            var payload = new
            {
                grant_type = "client_credentials",
                client_id = "UDJjEma9LrT92hn2iU5FXaOQcfNXUjhE",
                client_secret = "CeTRe1CWzoHKZsPlF0KIfqNbG1s5o97mqkNG47rvrNhClXbMO9IyTNtSFXT6jnX-",
                audience = "https://riskiapp.au.auth0.com/api/v2/"
            };

            var response = await client.PostAsync("https://riskiapp.au.auth0.com/oauth/token", new StringContent(JsonSerializer.Serialize(payload), Encoding.UTF8, "application/json"));

            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(content);
                return null;
            }

            return JsonSerializer.Deserialize<AccessToken>(content);
        }

        internal class UserInfo
        {
            public string sub { get; set; }
        }

        internal class SignUpResult
        {
            public string _id { get; set; }
        }
    }
}
