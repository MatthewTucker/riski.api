﻿using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Riski.Api.Application.Common.Interfaces;
using Riski.Api.Domain.Common;
using Riski.Api.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Riski.Api.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTime _dateTime;
        private readonly IDomainEventService _domainEventService;

        public ApplicationDbContext(
            DbContextOptions options,
            ICurrentUserService currentUserService,
            IDomainEventService domainEventService,
            IDateTime dateTime) : base(options)
        {
            _currentUserService = currentUserService;
            _domainEventService = domainEventService;
            _dateTime = dateTime;
        }

        public DbSet<Form> Forms { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<Risk> Risks { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<RiskType> RiskTypes { get; set; }
        public DbSet<RiskRating> RiskRatings { get; set; }
        public DbSet<QuestionDisplayType> QuestionDisplayTypes { get; set; }
        public DbSet<FormPage> FormPages { get; set; }
        public DbSet<RiskConfiguration> RiskConfigurations { get; set; }
        public DbSet<ConfigurableValue> ConfigurableValue { get; set; }
        public DbSet<ReferenceDocument> ReferenceDocuments { get; set; }
        public DbSet<ReferenceDocumentType> ReferenceDocumentTypes { get; set; }
        public DbSet<WorkTask> WorkTasks { get; set; }
        public DbSet<Signature> Signatures { get; set; }
        public DbSet<FormPageType> FormPageTypes { get; set; }
        public DbSet<AlertDialog> AlertDialogs { get; set; }
        public DbSet<EventCondition> EventConditions { get; set; }
        public DbSet<UIComponent> UIComponents { get; set; }
        public DbSet<GuestUser> GuestUsers { get; set; }
        public DbSet<DefinedAnswer> DefinedAnswers { get; set; }
        public DbSet<DefinedAnswerCategory> DefinedAnswerCategories { get; set; }
        public DbSet<RiskCode> RiskCodes { get; set; }
        public DbSet<AppFeature> AppFeatures { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<TodoList> TodoLists { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (EntityEntry<AuditableEntity> entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = _currentUserService.UserId;
                        entry.Entity.Created = _dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = _currentUserService.UserId;
                        entry.Entity.LastModified = _dateTime.Now;
                        break;
                }
            }

            var result = await base.SaveChangesAsync(cancellationToken);

            await DispatchEvents();

            return result;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }

        private async Task DispatchEvents()
        {
            while (true)
            {
                var domainEventEntity = ChangeTracker.Entries<IHasDomainEvent>()
                    .Select(x => x.Entity.DomainEvents)
                    .SelectMany(x => x)
                    .Where(domainEvent => !domainEvent.IsPublished)
                    .FirstOrDefault();
                if (domainEventEntity == null) break;

                domainEventEntity.IsPublished = true;
                await _domainEventService.Publish(domainEventEntity);
            }
        }
    }
}
