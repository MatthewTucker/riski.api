﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    class RiskConfigurationConfiguration : IEntityTypeConfiguration<Riski.Api.Domain.Entities.RiskConfiguration>
    {
        public void Configure(EntityTypeBuilder<Riski.Api.Domain.Entities.RiskConfiguration> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Status).HasConversion<string>();
        }
    }
}
