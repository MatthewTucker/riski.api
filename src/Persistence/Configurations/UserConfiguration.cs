﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Riski.Api.Domain.Entities;
using Riski.Api.Domain.ValueObjects;

namespace Persistence.Configurations
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.Email).HasConversion(v => v.Value, v => new Email(v));
            builder.Property(e => e.PhoneNumber).HasConversion(v => v.Value, v => new PhoneNumber(v));
            //builder.Property(e => e.UserType).HasConversion<string>();
            builder.Property(e => e.Status).HasConversion<string>();
        }
    }
}
