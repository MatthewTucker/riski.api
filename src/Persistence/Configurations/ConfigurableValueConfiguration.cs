﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Riski.Api.Domain.Entities;

namespace Persistence.Configurations
{
    class ConfigurableValueConfiguration : IEntityTypeConfiguration<ConfigurableValue>
    {
        public void Configure(EntityTypeBuilder<ConfigurableValue> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Status).HasConversion<string>();
        }
    }
}
