﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Riski.Api.Domain.Entities;

namespace Persistence.Configurations
{
    class UIComponentConfiguration : IEntityTypeConfiguration<UIComponent>
    {
        public void Configure(EntityTypeBuilder<UIComponent> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();
        }
    }
}
