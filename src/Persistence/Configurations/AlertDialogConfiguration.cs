﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Riski.Api.Domain.Entities;

namespace Persistence.Configurations
{
    class AlertDialogConfiguration : IEntityTypeConfiguration<AlertDialog>
    {
        public void Configure(EntityTypeBuilder<AlertDialog> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Status).HasConversion<string>();
        }
    }
}
