﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

using Riski.Api.Domain.Entities;
using Riski.Api.Domain.Enums;
using Riski.Api.Domain.ValueObjects;

namespace Riski.Api.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedSampleDataAsync(ApplicationDbContext context)
        {

            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            // Seed, if necessary
            await context.ConfigurableValue.AddRangeAsync(
                      new ConfigurableValue() { Category = "WorkerType", SystemKey = "admin", Value = "Administrator", Description = "Administrator of the Admin Portal.", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "WorkerType", SystemKey = "worker", Value = "Worker", Description = "Primary user of the Mobile application.", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "WorkerType", SystemKey = "manager", Value = "Manager", Description = "Secondary user of the Mobile application. This user may be contacted if a risk is identified by a worker.", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ButtonCellLookupKey", SystemKey = "sites", Value = "Sites", Description = "A list of sites that a worker can be located at.", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ButtonCellLookupKey", SystemKey = "divisions", Value = "Divisions", Description = "A list of divisions that a worker can be apart of.", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ButtonCellLookupKey", SystemKey = "workTasks", Value = "Work Tasks", Description = "A list of tasks that a worker can perform.", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "RiskConfigurationCategory", SystemKey = "Type", Value = "Type", Description = "Type of risk", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "RiskConfigurationCategory", SystemKey = "likelihood", Value = "Likelihood", Description = "Likelihood of the risk", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "RiskConfigurationCategory", SystemKey = "consequence", Value = "Consequence", Description = "Consequence of the risk", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventAction", SystemKey = "CONTINUE", Value = "Continue", Description = "Allow the user to move on", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventAction", SystemKey = "PHONE_CALL", Value = "Call a manager", Description = "Allows the user to call a manager for the current site", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventAction", SystemKey = "NOTHING", Value = "Do not proceed", Description = "Allow the user to move on", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventAction", SystemKey = "MANAGER_APPROVED", Value = "Update form to say manager approved activity", Description = "Allow the user to move on", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventAction", SystemKey = "MANAGER_DID_NOT_APPROVE", Value = "Update form to say manager did not approve activity", Description = "Allow the user to move on", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventAction", SystemKey = "POST_FORM", Value = "Submit Form", Description = "Allow the user to submit the form", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventAction", SystemKey = "ACTION_FINISH_SUB_FORM", Value = "Finish", Description = "Allow the user to finish the sub form", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventScope", SystemKey = "ALWAYS", Value = "Always", Description = "This eill always be triggered", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventScope", SystemKey = "FORM", Value = "Check Form", Description = "Check the entire form", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventScope", SystemKey = "PAGE", Value = "Only Page", Description = "Check only the current page", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventSpecific", SystemKey = "QUESTION", Value = "Question", Description = "Check the condition against Questions", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventSpecific", SystemKey = "RISK", Value = "Risk", Description = "Check the condition against Risks", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventRisk", SystemKey = "SHOULD_NOTIFY", Value = "Should Notify", Description = "Check to see if the risk's should notify status matches", Status = PersistenceStatus.Available },
                      new ConfigurableValue() { Category = "ConditionalEventQuestion", SystemKey = "ANSWER", Value = "Answer", Description = "Check to if the answer to the question matches", Status = PersistenceStatus.Available }
                  );

            await context.Divisions.AddRangeAsync(
                new Division() { Description = "Division 1", Name = "Division 1", Status = PersistenceStatus.Available },
                new Division() { Description = "Division 2", Name = "Division 2", Status = PersistenceStatus.Available },
                new Division() { Description = "Division 3", Name = "Division 3", Status = PersistenceStatus.Available },
                new Division() { Description = "Division 4", Name = "Division 4", Status = PersistenceStatus.Available },
                new Division() { Description = "Division 5", Name = "Division 5", Status = PersistenceStatus.Available }
            );

            // TODO: Replace with AuthorizationRepository for registering users

            /* context.Workers.AddOrUpdate(x => x.Id,
                new ApplicationUser() { FirstName = "John", LastName = "Smith", UserName = "john", Password = "1234", Type = "worker", Email = "john@riski.com.au", Mobile = "0412345678" },
                new ApplicationUser() { FirstName = "Bob", LastName = "Jones", UserName = "bob", Password = "1234", Type = "worker", Email = "bob@riski.com.au", Mobile = "0412345679" },
                new ApplicationUser() { FirstName = "James", LastName = "Rustle", UserName = "james", Password = "1234", Type = "worker", Email = "james@riski.com.au", Mobile = "0412345670" },
                new ApplicationUser() { FirstName = "Tom", LastName = "Johnson", UserName = "tom", Password = "1234", Type = "manager", Email = "tom@riski.com.au", Mobile = "0412345671" },
                new ApplicationUser() { FirstName = "Jeremy", LastName = "Fenton", UserName = "jeremy", Password = "1234", Type = "manager", Email = "jack@riski.com.au", Mobile = "0412345672" }
            );*/

            //AuthorizatrionRepository authRepo = new AuthorizatrionRepository(context);

            var admin = new User()
            {
                Username = "admin",
                FirstName = "SYSTEM",
                LastName = "Admin",
                UserType = UserType.Manager,
                Email = new Email("admin@riski.com.au"),
                PhoneNumber = new PhoneNumber("0412345678"),
                IdentityId = "auth0|60705c156461820068a45e8d",
                Status = PersistenceStatus.Available
            };

            var john = new User()
            {
                Username = "john",
                FirstName = "John",
                LastName = "Smith",
                UserType = UserType.Worker,
                Email = new Email("john@riski.com.au"),
                PhoneNumber = new PhoneNumber("0432329440"),
                IdentityId = "auth0|6071adcf2f125d0068f7c012",
                Status = PersistenceStatus.Available
            };

            var bob = new User()
            {
                Username = "bob",
                FirstName = "Bob",
                LastName = "Jones",
                UserType = UserType.Worker,
                Email = new Email("bob@riski.com.au"),
                PhoneNumber = new PhoneNumber("0412345678"),
                IdentityId = "auth0|6071ad7031565a0067a41243",
                Status = PersistenceStatus.Available
            };

            var james = new User()
            {
                Username = "james",
                FirstName = "James",
                LastName = "Rustle",
                UserType = UserType.Worker,
                Email = new Email("james@riski.com.au"),
                PhoneNumber = new PhoneNumber("0412345678"),
                IdentityId = "auth0|6071adf83adb8a0069dc8ebb",
                Status = PersistenceStatus.Available
            };

            await context.Users.AddRangeAsync(admin, john, bob, james);

            await context.Sites.AddRangeAsync(new List<Site>{
                    new Site() { Name = "Burwood" , Status = PersistenceStatus.Available},
                    new Site() { Name = "Chatswood", Status = PersistenceStatus.Available },
                    new Site() { Name = "Hornsby", Status = PersistenceStatus.Available },
                    new Site() {  Name = "Miranda" , Status = PersistenceStatus.Available},
                    new Site() {  Name = "Paramatta" , Status = PersistenceStatus.Available},
                    new Site() {  Name = "Pitt Street", Status = PersistenceStatus.Available }
                }
            );

            await context.RiskTypes.AddRangeAsync(
                new RiskType() { Name = "Personnel", Status = PersistenceStatus.Available },
                new RiskType() { Name = "Electrical", Status = PersistenceStatus.Available },
                new RiskType() { Name = "Site", Status = PersistenceStatus.Available },
                new RiskType() { Name = "Weather", Status = PersistenceStatus.Available }
            );

            var likelihoodAlmostCertian = new RiskConfiguration() { Code = "A", Value = "Almost Certain", Description = "Likelihood - Almost Certain", Category = "likelihood", Rating = 5, Status = PersistenceStatus.Available };
            var likelihoodLikely = new RiskConfiguration() { Code = "B", Value = "Likely", Description = "Likelihood - Likely", Category = "likelihood", Rating = 4, Status = PersistenceStatus.Available };
            var likelihoodPossible = new RiskConfiguration() { Code = "C", Value = "Possible", Description = "Likelihood - Possible", Category = "likelihood", Rating = 3, Status = PersistenceStatus.Available };
            var likelihoodUnlikely = new RiskConfiguration() { Code = "D", Value = "Unlikely", Description = "Likelihood - Unlikely", Category = "likelihood", Rating = 2, Status = PersistenceStatus.Available };
            var likelihoodRare = new RiskConfiguration() { Code = "E", Value = "Rare", Description = "Likelihood - Rare", Category = "likelihood", Rating = 1, Status = PersistenceStatus.Available };

            var consequenceInsignificant = new RiskConfiguration() { Code = "1", Value = "Insignificant", Description = "Consequence - Insignificant", Category = "consequence", Rating = 1, Status = PersistenceStatus.Available };
            var consequenceMinor = new RiskConfiguration() { Code = "2", Value = "Minor", Description = "Consequence - Minor", Category = "consequence", Rating = 2, Status = PersistenceStatus.Available };
            var consequenceModerate = new RiskConfiguration() { Code = "3", Value = "Moderate", Description = "Consequence - Moderate", Category = "consequence", Rating = 3, Status = PersistenceStatus.Available };
            var consequenceMajor = new RiskConfiguration() { Code = "4", Value = "Major", Description = "Consequence - Major", Category = "consequence", Rating = 4, Status = PersistenceStatus.Available };
            var consequenceSevere = new RiskConfiguration() { Code = "5", Value = "Severe", Description = "Consequence - Severe", Category = "consequence", Rating = 5, Status = PersistenceStatus.Available };

            await context.RiskConfigurations.AddRangeAsync(
                new RiskConfiguration() { Code = "personnel", Value = "Personnel", Description = "Risk Type - Personnel", Category = "Type", Status = PersistenceStatus.Available },
                new RiskConfiguration() { Code = "electrical", Value = "Electrical", Description = "Risk Type - Electrical", Category = "Type", Status = PersistenceStatus.Available },
                new RiskConfiguration() { Code = "site", Value = "Site", Description = "Risk Type - Site", Category = "Type", Status = PersistenceStatus.Available },
                new RiskConfiguration() { Code = "weather", Value = "Weather", Description = "Risk Type - Weather", Category = "Type", Status = PersistenceStatus.Available },
                likelihoodAlmostCertian,
                likelihoodLikely,
                likelihoodPossible,
                likelihoodUnlikely,
                likelihoodRare,
                consequenceInsignificant,
                consequenceMinor,
                consequenceModerate,
                consequenceMajor,
                consequenceSevere,
                new RiskConfiguration() { Code = "Identified", Value = "Identified", Description = "Status - the risk has been identified but not viewed", Category = "status", Status = PersistenceStatus.Available },
                new RiskConfiguration() { Code = "Viewed", Value = "Viewed", Description = "Status - the risk has been identified and viewed", Category = "status", Status = PersistenceStatus.Available },
                new RiskConfiguration() { Code = "Cleared", Value = "Cleared", Description = "Status - the risk has been cleared", Category = "status", Status = PersistenceStatus.Available }
            );

            await context.RiskRatings.AddRangeAsync(
                new RiskRating() { Value = 1, Display = "Low", Colour = "#8BC34A", Message = "You can proceed", ShouldNotify = false, RiskCodes = "C1,D1,E1,E2,E3", Status = PersistenceStatus.Available },
                new RiskRating() { Value = 2, Display = "Medium", Colour = "#FFEB3B", Message = "Mitigate risk, then proceed", ShouldNotify = false, RiskCodes = "A1,B1,C2,D2,D3,E4,E5", Status = PersistenceStatus.Available },
                new RiskRating() { Value = 3, Display = "High", Colour = "#FF5722", Message = "Do not proceed. Alert management", ShouldNotify = true, RiskCodes = "A2,B2,B3,C3,C4,D4,D5", Status = PersistenceStatus.Available },
                new RiskRating() { Value = 4, Display = "Extreme", Colour = "#F44336", Message = "Do not proceed. Alert management", ShouldNotify = true, RiskCodes = "A3,A4,A5,B4,B5,C5", Status = PersistenceStatus.Available }
            );

            await context.RiskCodes.AddRangeAsync(
                GetRiskCode("E1", likelihoodRare, consequenceInsignificant),
                GetRiskCode("E2", likelihoodRare, consequenceMinor),
                GetRiskCode("E3", likelihoodRare, consequenceModerate),
                GetRiskCode("E4", likelihoodRare, consequenceMajor),
                GetRiskCode("E5", likelihoodRare, consequenceSevere),
                GetRiskCode("D1", likelihoodUnlikely, consequenceInsignificant),
                GetRiskCode("D2", likelihoodUnlikely, consequenceMinor),
                GetRiskCode("D3", likelihoodUnlikely, consequenceModerate),
                GetRiskCode("D4", likelihoodUnlikely, consequenceMajor),
                GetRiskCode("D5", likelihoodUnlikely, consequenceSevere),
                GetRiskCode("C1", likelihoodPossible, consequenceInsignificant),
                GetRiskCode("C2", likelihoodPossible, consequenceMinor),
                GetRiskCode("C3", likelihoodPossible, consequenceModerate),
                GetRiskCode("C4", likelihoodPossible, consequenceMajor),
                GetRiskCode("C5", likelihoodPossible, consequenceSevere),
                GetRiskCode("B1", likelihoodLikely, consequenceInsignificant),
                GetRiskCode("B2", likelihoodLikely, consequenceMinor),
                GetRiskCode("B3", likelihoodLikely, consequenceModerate),
                GetRiskCode("B4", likelihoodLikely, consequenceMajor),
                GetRiskCode("B5", likelihoodLikely, consequenceSevere),
                GetRiskCode("A1", likelihoodAlmostCertian, consequenceInsignificant),
                GetRiskCode("A2", likelihoodAlmostCertian, consequenceMinor),
                GetRiskCode("A3", likelihoodAlmostCertian, consequenceModerate),
                GetRiskCode("A4", likelihoodAlmostCertian, consequenceMajor),
                GetRiskCode("A5", likelihoodAlmostCertian, consequenceSevere)
            );

            await context.QuestionDisplayTypes.AddRangeAsync(
                new QuestionDisplayType() { SystemValue = "buttonCell", DisplayValue = "Link Button", Description = "A button that will navigate to a list view.", Status = PersistenceStatus.Available },
                new QuestionDisplayType() { SystemValue = "dateNowCell", DisplayValue = "Current Date Field", Description = "A textfield that shows the current date.", Status = PersistenceStatus.Available },
                new QuestionDisplayType() { SystemValue = "timeNowCell", DisplayValue = "Current Time Field", Description = "A textfield that shows the current time.", Status = PersistenceStatus.Available },
                new QuestionDisplayType() { SystemValue = "textfieldCell", DisplayValue = "Text Field", Description = "A textfield that will allow the user to enter input.", Status = PersistenceStatus.Available },
                new QuestionDisplayType() { SystemValue = "switchCell", DisplayValue = "Switch Field", Description = "A switch field that will allow the user to switch between true and false.", Status = PersistenceStatus.Available },
                new QuestionDisplayType() { SystemValue = "captureImageCell", DisplayValue = "Image Capture", Description = "Allow the user to take attach an image", Status = PersistenceStatus.Available }
            );

            await context.WorkTasks.AddRangeAsync(
                new WorkTask() { Name = "Welding", Description = "Welding", MinLikelihoodRating = 1, MinConsequenceRating = 1, Status = PersistenceStatus.Available },
                new WorkTask() { Name = "Brick Laying", Description = "Brick Laying", MinLikelihoodRating = 2, MinConsequenceRating = 1, Status = PersistenceStatus.Available },
                new WorkTask() { Name = "Roofing", Description = "Roofing", MinLikelihoodRating = 1, MinConsequenceRating = 2, Status = PersistenceStatus.Available },
                new WorkTask() { Name = "Electrical Work", Description = "Electrical Work", MinLikelihoodRating = 2, MinConsequenceRating = 2, Status = PersistenceStatus.Available }
            );

            await context.ReferenceDocumentTypes.AddRangeAsync(
                new ReferenceDocumentType() { Code = "SWMS", Description = "Safe Work Method Statement", Status = PersistenceStatus.Available },
                new ReferenceDocumentType() { Code = "SOP", Description = "Standard Operating Procedure", Status = PersistenceStatus.Available },
                new ReferenceDocumentType() { Code = "WI", Description = "Work Instruction", Status = PersistenceStatus.Available },
                new ReferenceDocumentType() { Code = "RM", Description = "Risk Matrix", Status = PersistenceStatus.Available }
            );

            var ec1 = new EventCondition() { Scope = "ALWAYS", Status = PersistenceStatus.Available };
            var ec2 = new EventCondition() { Scope = "PAGE", Specific = "QUESTION", Member = "ANSWER", Value = "", Status = PersistenceStatus.Available };
            var ec3 = new EventCondition() { Scope = "PAGE", Specific = "QUESTION", Member = "ANSWER", Value = "false", Status = PersistenceStatus.Available };
            var ec4 = new EventCondition() { Scope = "FORM", Specific = "RISK", Member = "SHOULD_NOTIFY", Value = "true", Status = PersistenceStatus.Available };

            await context.EventConditions.AddRangeAsync(
                ec1, ec2, ec3, ec4
            );

            var ad1 = new AlertDialog() { Title = "Form complete", Message = "Your form is about to be completed and sent to the server. Do you wish to continue", PositiveText = "Yes", NegativeText = "No", NegativeAction = "DO_NOTHING", PositiveAction = "POST_FORM", EventCondition = ec1, Status = PersistenceStatus.Available };
            var ad2 = new AlertDialog() { Title = "Page incomplete", Message = "You must complete all questions on this page", PositiveText = "OK", PositiveAction = "DO_NOTHING", EventCondition = ec2, Status = PersistenceStatus.Available };
            var ad3 = new AlertDialog() { Title = "Approval Required", Message = "You have answered false to at least one question. In order to continue you must get managerial approval. Would you like to call them now?", PositiveText = "Yes", NegativeText = "No", PositiveAction = "PHONE_CALL", NegativeAction = "DO_NOTHING", EventCondition = ec3, Status = PersistenceStatus.Available };
            var ad4 = new AlertDialog() { Title = "Notifiable risk found", Message = "You have identified a risk that requires management to be nofified. Do you want to call them?", PositiveText = "Yes", NegativeText = "No", PositiveAction = "PHONE_CALL", NegativeAction = "CONTINUE", EventCondition = ec4, Status = PersistenceStatus.Available };
            var ad5 = new AlertDialog() { Title = "Approval Required", Message = "Did you receive approval from your manager?", PositiveText = "Yes", NegativeText = "No", PositiveAction = "MANAGER_APPROVED", NegativeAction = "MANAGER_DID_NOT_APPROVE", EventCondition = ec3, Status = PersistenceStatus.Available };

            await context.AlertDialogs.AddRangeAsync(
                ad1, ad2, ad3, ad4, ad5
            );

            var standard = new FormPageType() { Code = "STANDARD", Name = "Standard", ReadOnly = false, Status = PersistenceStatus.Available };
            var review = new FormPageType() { Code = "REVIEW", Name = "Review", ReadOnly = true, Status = PersistenceStatus.Available };
            var identifyRisk = new FormPageType() { Code = "IDENTIFY_RISKS", Name = "Identify Risks", ReadOnly = false, Status = PersistenceStatus.Available };
            var addRisk = new FormPageType() { Code = "ADD_RISKS", Name = "Add Risk", ReadOnly = false, Status = PersistenceStatus.Available };
            var signature = new FormPageType() { Code = "SIGNATURE", Name = "Signature", ReadOnly = false, Status = PersistenceStatus.Available };
            await context.FormPageTypes.AddRangeAsync(standard, review, identifyRisk, addRisk, signature);

            var simple = new Template() { Name = "Simple", Status = PersistenceStatus.Available };
            //var simpleNoSignatures = new Template() { Name = "Simple without Signatures" };

            await context.Templates.AddAsync(simple);

            var addProxyUser = new UIComponent() { Value = "ADD_PROXY_USER", Description = "Allow the user to add proxy users", Title = "Add Proxy User", Type = "HEADER" };
            var addGuestUser = new UIComponent() { Value = "ADD_GUEST_USER", Description = "Allow the user to add guest users", Title = "Add Guest User", Type = "HEADER" };
            var viewReferenceDocument = new UIComponent() { Value = "VIEW_REFERENCE_DOCUMENT", Description = "View the primary reference document", Title = "View Primary Reference Document", Type = "HEADER" };
            var viewReferenceDocuments = new UIComponent() { Value = "VIEW_REFERENCE_DOCUMENTS", Description = "Allow the user to view the available reference documents based on answers provided and the Category selected", Title = "View Reference Documents", Type = "FOOTER" };
            await context.UIComponents.AddRangeAsync(addProxyUser, addGuestUser, viewReferenceDocument, viewReferenceDocuments);


            var q1 = new Question() { Description = "Date", DisplayType = "dateNowCell", ReadOnly = true, Mandatory = true, Position = 1, Status = PersistenceStatus.Available };
            var q2 = new Question() { Description = "Time", DisplayType = "timeNowCell", ReadOnly = true, Mandatory = true, Position = 2, Status = PersistenceStatus.Available };
            var q3 = new Question() { Description = "Location", DisplayType = "buttonCell", ReadOnly = false, LookupKey = "sites", Mandatory = true, Position = 3, Status = PersistenceStatus.Available };
            var q4 = new Question() { Description = "Division", DisplayType = "buttonCell", ReadOnly = false, LookupKey = "divisions", Mandatory = true, Position = 4, Status = PersistenceStatus.Available };
            var q5 = new Question() { Description = "Task Description", DisplayType = "buttonCell", ReadOnly = false, LookupKey = "workTasks", Mandatory = true, Position = 5, Status = PersistenceStatus.Available };
            //new Question() { Description = "Completed By", DisplayType = "textfieldCell", FormPage = siteInformationFormPage, ReadOnly = true },
            var q6 = new Question() { Description = "Job No.", DisplayType = "textfieldCell", ReadOnly = false, Mandatory = true, Position = 6, Status = PersistenceStatus.Available };

            var q7 = new Question() { Description = "Do I fully understand the required scope of works?", DisplayType = "switchCell", ReadOnly = false, Mandatory = true, Position = 1, Status = PersistenceStatus.Available };
            var q8 = new Question() { Description = "Do you have the required skills, knowledge and resources to complete this task safely?", DisplayType = "switchCell", ReadOnly = false, Mandatory = true, Position = 2, Status = PersistenceStatus.Available };
            var q9 = new Question() { Description = "Have you read and understood the procedures for this task and agree to observe all defined requirements?", DisplayType = "switchCell", ReadOnly = false, Mandatory = true, Position = 3, Status = PersistenceStatus.Available };
            var q10 = new Question() { Description = "Are all tools, equipment and PPE required to be utilised for this task, in good condition?", DisplayType = "switchCell", ReadOnly = false, Mandatory = true, Position = 4, Status = PersistenceStatus.Available };
            var q11 = new Question() { Description = "Do you have all required permits and approval to complete this task?", DisplayType = "switchCell", ReadOnly = false, Mandatory = true, Position = 5, Status = PersistenceStatus.Available };
            //new Question() { Description = "Is Mobile plant, equipment, or machinery operating adjacent to where the work will occur?", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false },
            //new Question() { Description = "Are sources of stored electricity/energy (e.g water, gas, air, steam) present?", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false },
            //new Question() { Description = "Will I be 'Working at Height' or could something fall from a height?", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false },
            //new Question() { Description = "Will Hot Work be performed?", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false },
            //new Question() { Description = "Will substance be used, or is there potential for substances or fibres to be released?", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false },
            //new Question() { Description = "Does the task require Manual Handling/exertion on body position?", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false },
            //new Question() { Description = "Are there other Hazards present that need to be addressed? (dust, fumes, other trades/public)", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false },
            //new Question() { Description = "Is work area fit for use (e.g slips/trips/falls, lighting levels, weather conditions)", DisplayType = "switchCell", FormPage = taskAssessmentFormPage, ReadOnly = false }
            await context.Questions.AddRangeAsync(q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11);

            var siteInformationFormPage = new FormPage() { Template = simple, TemplateId = simple.Id, PageName = "site information", Position = 1, Description = "This page will allow the user to enter details about the site.", Title = "Site Information", FormPageType = standard, AlertDialogs = new HashSet<AlertDialog>() { ad2 }, Questions = new HashSet<Question>() { q1, q2, q3, q4, q5, q6 }, Status = PersistenceStatus.Available };

            var taskAssessmentFormPage = new FormPage() { Template = simple, TemplateId = simple.Id, PageName = "task assessment", Position = 2, Description = "This page will allow the user to answer questions that relate to assessing the task.", Title = "Task Assessment", FormPageType = standard, AlertDialogs = new HashSet<AlertDialog>() { ad3 }, Questions = new HashSet<Question>() { q7, q8, q9, q10, q11 }, Status = PersistenceStatus.Available };

            var identifyRisksFormPage = new FormPage() { Template = simple, TemplateId = simple.Id, PageName = "identify risks", Position = 3, Description = "This page will list the identified risks that have been added by the user", Title = "Identify Risks", FormPageType = identifyRisk, Status = PersistenceStatus.Available };
            var signatureFormPage = new FormPage() { Template = simple, TemplateId = simple.Id, PageName = "signature", Position = 4, Description = "This page will allow the user to sign their form", Title = "Signature", FormPageType = signature, Status = PersistenceStatus.Available };
            var reviewFormPage = new FormPage() { Template = simple, TemplateId = simple.Id, PageName = "review", Position = 5, Description = "This page will allow the user to review their form", Title = "Review", FormPageType = review, AlertDialogs = new HashSet<AlertDialog>() { ad1, ad4 }, Status = PersistenceStatus.Available };
            await context.FormPages.AddRangeAsync(
                siteInformationFormPage,
                taskAssessmentFormPage,
                identifyRisksFormPage,
                signatureFormPage,
                reviewFormPage
            );

            await context.SaveChangesAsync();

            transaction.Complete();
        }

        private static RiskCode GetRiskCode(string code, RiskConfiguration likelihood, RiskConfiguration consequence) => new RiskCode { Code = code, Consequence = consequence, Likelihood = likelihood, Status = PersistenceStatus.Available };
    }
}
