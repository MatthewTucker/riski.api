﻿using Riski.Api.Application.Common.Interfaces;
using System;

namespace Riski.Api.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.UtcNow;
    }
}
