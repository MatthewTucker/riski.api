﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.Sites;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SitesController : ApiControllerBase
    {
        [HttpGet]
        public async Task<List<SiteDto>> GetSitesAsync()
        {
            return await Mediator.Send(new GetSitesQuery());
        }

        // GET: api/Sites/5
        [HttpGet("{id}")]
        public async Task<SiteDto> GetSiteAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetSiteQuery { Id = id });
        }

        // POST: api/Sites
        [HttpPost]
        public async Task<IActionResult> PostSite([FromBody] CreateSiteDto Site)
        {
            var result = await Mediator.Send(new CreateSiteCommand { CreateSite = Site });

            return CreatedAtAction("GetDivision", new { Id = result.ToString() }, Site);
        }

        // DELETE: api/Sites/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSiteAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteSiteCommand { Id = id });

            return Ok();
        }

        [HttpGet("dashboardData")]
        public async Task<List<DashboardSiteDataDto>> GetAllTimeStampSummaryForToday()
        {
            return await Mediator.Send(new GetSiteSummaryForTodayQuery());
        }
    }
}
