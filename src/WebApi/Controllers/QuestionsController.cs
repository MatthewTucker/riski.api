﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.Questions;
using Riski.Api.Application.Questions.Queries.GetQuestionDisplayTypes;

namespace WebApi.Controllers
{
    public class QuestionsController : ApiControllerBase
    {
        // GET: api/Questions
        [HttpGet]
        public async Task<IList<QuestionDto>> GetQuestions()
        {
            return await Mediator.Send(new GetQuestionsQuery());
        }

        [HttpGet("displayTypes")]
        public async Task<List<QuestionDisplayTypeDto>> GetQuestionDisplayTypes()
        {
            return await Mediator.Send(new GetQuestionDisplayTypesQuery());
        }

        // GET: api/Questions/5
        [HttpGet("{id}")]
        public async Task<QuestionDto> GetQuestion([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetQuestionQuery { Id = id });
        }

        // PUT: api/Questions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuestion([FromRoute] Guid id, [FromBody] UpdateQuestionDto updateQuestionDto)
        {
            var result = await Mediator.Send(new UpdateQuestionCommand { UpdateQuestionDto = updateQuestionDto });

            return Ok(result);
        }

        // POST: api/Questions
        [HttpPost]
        public async Task<IActionResult> PostQuestion([FromBody] CreateQuestionDto createQuestionDto)
        {
            var result = await Mediator.Send(new CreateQuestionCommand { CreateQuestionDto = createQuestionDto });

            return CreatedAtAction("GetQuestion", new { result });
        }

        // DELETE: api/Questions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteQuestion([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteQuestionCommand { Id = id });

            return Ok();
        }
    }
}
