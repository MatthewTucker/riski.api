﻿using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.ConfigurableValues;
using Riski.Api.Application.ConfigurableValues.Queries.GetConfigurableValue;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurableValuesController : ApiControllerBase
    {
        [HttpGet("category/{category}")]
        public async Task<List<ConfigurableValueDto>> GetConfigurableValuesByCategory([FromRoute] string category)
        {
            return await Mediator.Send(new GetConfigurableValuesQuery { Category = category });
        }

        [HttpGet("{id}")]
        public async Task<ConfigurableValueDto> GetConfigurableValue([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetConfigurableValueQuery { Id = id });
        }

        [HttpGet]
        public async Task<List<ConfigurableValueDto>> GetConfigurableValues()
        {
            return await Mediator.Send(new GetConfigurableValuesQuery());
        }
    }
}
