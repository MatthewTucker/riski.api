﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.Forms;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FormsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<List<FormDto>> GetFormsAsync([FromQuery] int? skip, [FromQuery] int? take)
        {
            return await Mediator.Send(new GetFormsQuery { Skip = skip, Take = take });
        }

        // GET: api/Forms/5
        [HttpGet("{id}")]
        public async Task<FormDto> GetFormAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetFormQuery { Id = id });
        }

        // POST: api/Forms
        [HttpPost]
        public async Task<IActionResult> PostFormAsync([FromBody] CreateFormDto createFormDto)
        {
            var id = await Mediator.Send(new CreateFormCommand { CreateFormDto = createFormDto });

            return CreatedAtAction("GetForm", new { id }, createFormDto);
        }

        // POST: api/Forms/completedForm?id={id}&username={username}
        [Route("completed")]
        [HttpPost]
        public async Task<IActionResult> CompletedForm(Guid id, string username)
        {
            await Mediator.Send(new CompletedFormCommand { Id = id, Username = username });

            return Ok(new { });
        }

        // GET: api/Forms/meta?username={username}&startDate={startDate}&endDate={endDate}
        [Route("meta")]
        [HttpGet]
        public async Task<List<FormMetaDto>> GetFormMetasAsync(string username, string startDate, string endDate)
        {
            var startDateTime = !string.IsNullOrEmpty(startDate) ? DateTime.ParseExact(startDate, "dd-MM-yyyy", null) : DateTime.Today;
            var endDateTime = !string.IsNullOrEmpty(endDate) ? DateTime.ParseExact(endDate, "dd-MM-yyyy", null) : DateTime.Today;

            return await Mediator.Send(new GetFormMetasQuery { Username = username, StartDate = startDateTime, EndDate = endDateTime });
        }

        // GET: api/Forms/today
        [Route("today")]
        [HttpGet]
        public async Task<List<FormDto>> GetFormsForTodayAsync()
        {
            return await Mediator.Send(new GetFormsForTodayQuery());
        }

        [Route("sub_form_lookup_test_data")]
        [HttpGet]
        public IEnumerable<object> GetSubFormLookupTestData(string lookupCode)
        {
            var list = new List<object>
            {
                GetFakeSubFormLookup("Product A", "Warehouse Returns Form", 6),
                GetFakeSubFormLookup( "Product B", "Warehouse Returns Form", 6)
            };
            return list;
        }

        object GetFakeSubFormLookup(string name, string description, int templateId)
        {
            return new
            {
                templateId,
                isCompleted = false,
                description,
                name
            };
        }
    }
}
