﻿using Microsoft.AspNetCore.Mvc;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.Users;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ApiControllerBase
    {
        // GET: api/users
        [HttpGet]
        public async Task<List<UserDto>> Get()
        {
            return await Mediator.Send(new GetUsersQuery());
        }

        // GET: api/users/{id}
        [HttpGet("{id}")]
        public async Task<UserDto> Get(Guid id)
        {
            return await Mediator.Send(new GetUserQuery { Id = id });
        }

        // GET: api/users/login
        [HttpGet("login")]
        public async Task<UserDto> Get([FromQuery] string username, [FromQuery] string password)
        {
            var query = new LoginQuery
            {
                Username = username,
                Password = password
            };

            var userDto = await Mediator.Send(query);

            return userDto;
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(UpdateUserDto userDto)
        {
            await Mediator.Send(new UpdateUserCommand
            {
                UserDto = userDto
            });

            return NoContent();
        }

        [HttpPost()]
        public async Task<ActionResult<Guid>> Post(CreateUserDto userDto)
        {
            return await Mediator.Send(new CreateUserCommand
            {
                UserDto = userDto
            });
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            await Mediator.Send(new DeleteUserCommand
            {
                Id = id
            });

            return NoContent();
        }
    }
}
