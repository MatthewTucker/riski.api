﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.WorkTasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkTasksController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<WorkTaskDto>> GetWorkTasksAsync()
        {
            return await Mediator.Send(new GetWorkTasksQuery());
        }

        // GET: api/WorkTasks/5
        [HttpGet("{id}")]
        public async Task<WorkTaskDto> GetWorkTaskAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetWorkTaskQuery { Id = id });
        }

        // PUT: api/WorkTasks/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkTask([FromRoute] Guid id, [FromBody] UpdateWorkTaskDto WorkTask)
        {
            var result = await Mediator.Send(new UpdateWorkTaskCommand { UpdateWorkTask = WorkTask });

            return Ok(result);
        }

        // POST: api/WorkTasks
        [HttpPost]
        public async Task<IActionResult> PostWorkTask([FromBody] CreateWorkTaskDto WorkTask)
        {
            var result = await Mediator.Send(new CreateWorkTaskCommand { CreateWorkTask = WorkTask });

            return CreatedAtAction("GetDivision", new { Id = result.ToString() }, WorkTask);
        }

        // DELETE: api/WorkTasks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkTaskAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteWorkTaskCommand { Id = id });

            return Ok();
        }
    }
}