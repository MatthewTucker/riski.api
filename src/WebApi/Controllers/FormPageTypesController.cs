﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.FormPageTypes;

namespace WebApi.Controllers
{
    public class FormPageTypesController : ApiControllerBase
    {
        // GET: api/FormPages
        [HttpGet]
        public async Task<IList<FormPageTypeDto>> GetFormPageTypes()
        {
            return await Mediator.Send(new GetFormPageTypesQuery());
        }

        // GET: api/FormPages/5
        [HttpGet("{id}")]
        public async Task<FormPageTypeDto> GetFormPageType([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetFormPageTypeQuery { Id = id });
        }
    }
}
