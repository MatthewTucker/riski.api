﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.FormPages;

namespace WebApi.Controllers
{
    public class FormPagesController : ApiControllerBase
    {
        // GET: api/FormPages
        [HttpGet]
        public async Task<IList<FormPageDto>> GetFormPages()
        {
            return await Mediator.Send(new GetFormPagesQuery());
        }

        // GET: api/FormPages/5
        [HttpGet("{id}")]
        public async Task<FormPageDto> GetFormPage([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetFormPageQuery { Id = id });
        }

        // PUT: api/FormPages/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFormPage([FromRoute] Guid id, [FromBody] UpdateFormPageDto updateFormPageDto)
        {
            var result = await Mediator.Send(new UpdateFormPageCommand { UpdateFormPage = updateFormPageDto });

            return Ok(result);
        }

        // POST: api/FormPages
        [HttpPost]
        public async Task<IActionResult> PostFormPage([FromBody] CreateFormPageDto createFormPageDto)
        {
            var result = await Mediator.Send(new CreateFormPageCommand { CreateFormPageDto = createFormPageDto });

            return CreatedAtAction("GetFormPage", new { result });
        }

        // DELETE: api/FormPages/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFormPage([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteFormPageCommand { Id = id });

            return Ok();
        }
    }
}
