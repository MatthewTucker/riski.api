﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.RiskCodes;

namespace WebApi.Controllers
{
    public class RiskCodesController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<RiskCodeDto>> GetDefinedAnswerCategoriesAsync()
        {
            return await Mediator.Send(new GetRiskCodesQuery());
        }

        [HttpGet("{id}")]
        public async Task<RiskCodeDto> GetRiskCodeAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetRiskCodeQuery { Id = id });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutRiskCode([FromRoute] Guid id, [FromBody] UpdateRiskCodeDto RiskCode)
        {
            var result = await Mediator.Send(new UpdateRiskCodeCommand { UpdateRiskCode = RiskCode });

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> PostRiskCode([FromBody] CreateRiskCodeDto RiskCode)
        {
            var result = await Mediator.Send(new CreateRiskCodeCommand { CreateRiskCode = RiskCode });

            return CreatedAtAction("GetRiskCode", new { Id = result.ToString() }, RiskCode);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRiskCodeAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteRiskCodeCommand { Id = id });

            return Ok();
        }

        [HttpPost("matrix")]
        public async Task<IActionResult> PostRiskCodes([FromBody] List<CreateRiskCodeDto> riskCodes)
        {
            await Mediator.Send(new CreateRiskCodesCommand { RiskCodes = riskCodes });

            return Ok();
        }

        [HttpDelete("matrix")]
        public async Task<IActionResult> DeleteRiskCodesAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteRiskCodesCommand());

            return Ok();
        }
    }
}
