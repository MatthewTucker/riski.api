﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.RiskTypes;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RiskTypesController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<RiskTypeDto>> GetRiskTypesAsync()
        {
            return await Mediator.Send(new GetRiskTypesQuery());
        }

        // GET: api/RiskTypes/5
        [HttpGet("{id}")]
        public async Task<RiskTypeDto> GetRiskTypeAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetRiskTypeQuery { Id = id });
        }

        // PUT: api/RiskTypes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRiskType([FromRoute] Guid id, [FromBody] UpdateRiskTypeDto RiskType)
        {
            var result = await Mediator.Send(new UpdateRiskTypeCommand { UpdateRiskType = RiskType });

            return Ok(result);
        }

        // POST: api/RiskTypes
        [HttpPost]
        public async Task<IActionResult> PostRiskType([FromBody] CreateRiskTypeDto RiskType)
        {
            var result = await Mediator.Send(new CreateRiskTypeCommand { CreateRiskType = RiskType });

            return CreatedAtAction("GetDivision", new { Id = result.ToString() }, RiskType);
        }

        // DELETE: api/RiskTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRiskTypeAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteRiskTypeCommand { Id = id });

            return Ok();
        }
    }
}