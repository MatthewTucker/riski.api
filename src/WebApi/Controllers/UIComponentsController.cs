﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.UIComponents;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UIComponentsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<UIComponentDto>> GetUIComponentsAsync()
        {
            return await Mediator.Send(new GetUIComponentsQuery());
        }

        // GET: api/UIComponents/5
        [HttpGet("{id}")]
        public async Task<UIComponentDto> GetUIComponentAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetUIComponentQuery { Id = id });
        }
    }
}

