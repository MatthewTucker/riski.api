﻿using Microsoft.AspNetCore.Mvc;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.Divisions;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<DivisionDto>> GetDefinedAnswerCategoriesAsync()
        {
            return await Mediator.Send(new GetDivisionsQuery());
        }

        // GET: api/Divisions/5
        [HttpGet("{id}")]
        public async Task<DivisionDto> GetDivisionAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetDivisionQuery { Id = id });
        }

        // PUT: api/Divisions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDivision([FromRoute] Guid id, [FromBody] UpdateDivisionDto division)
        {
            var result = await Mediator.Send(new UpdateDivisionCommand { UpdateDivision = division });

            return Ok(result);
        }

        // POST: api/Divisions
        [HttpPost]
        public async Task<IActionResult> PostDivision([FromBody] CreateDivisionDto Division)
        {
            var result = await Mediator.Send(new CreateDivisionCommand { CreateDivisionDto = Division });

            return CreatedAtAction("GetDivision", new { Id = result.ToString() }, Division);
        }

        // DELETE: api/Divisions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDivisionAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteDivisionCommand { Id = id });

            return Ok();
        }
    }
}
