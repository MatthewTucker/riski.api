﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.ReferenceDocumentTypes;

namespace WebApi.Controllers
{
    public class ReferenceDocumentTypesController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<ReferenceDocumentTypeDto>> GetDefinedAnswerCategoriesAsync()
        {
            return await Mediator.Send(new GetReferenceDocumentTypesQuery());
        }

        // GET: api/DefinedAnswerCategories/5
        [HttpGet("{id}")]
        public async Task<ReferenceDocumentTypeDto> GetReferenceDocumentTypeAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetReferenceDocumentTypeQuery { Id = id });
        }

        // PUT: api/DefinedAnswerCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReferenceDocumentType([FromRoute] Guid id, [FromBody] UpdateReferenceDocumentTypeDto referenceDocumentType)
        {
            var result = await Mediator.Send(new UpdateReferenceDocumentTypeCommand { UpdateReferenceDocumentType = referenceDocumentType });

            return Ok(result);
        }

        // POST: api/DefinedAnswerCategories
        [HttpPost]
        public async Task<IActionResult> PostReferenceDocumentType([FromBody] CreateReferenceDocumentTypeDto referenceDocumentType)
        {
            var result = await Mediator.Send(new CreateReferenceDocumentTypeCommand { CreateReferenceDocumentType = referenceDocumentType });

            return CreatedAtAction("GetDivision", new { Id = result.ToString() }, referenceDocumentType);
        }

        // DELETE: api/DefinedAnswerCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReferenceDocumentTypeAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteReferenceDocumentTypeCommand { Id = id });

            return Ok();
        }
    }
}
