﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.RiskConfigurations;

namespace WebApi.Controllers
{
    public class RiskConfigurationsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<RiskConfigurationDto>> GetRiskConfigurationsAsync()
        {
            return await Mediator.Send(new GetRiskConfigurationsQuery());
        }

        // GET: api/DefinedAnswerCategories/5
        [HttpGet("{id}")]
        public async Task<RiskConfigurationDto> GetRiskConfigurationAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetRiskConfigurationQuery { Id = id });
        }

        // PUT: api/DefinedAnswerCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRiskConfiguration([FromRoute] Guid id, [FromBody] UpdateRiskConfigurationDto RiskConfiguration)
        {
            var result = await Mediator.Send(new UpdateRiskConfigurationCommand { UpdateRiskConfiguration = RiskConfiguration });

            return Ok(result);
        }

        // POST: api/DefinedAnswerCategories
        [HttpPost]
        public async Task<IActionResult> PostRiskConfiguration([FromBody] CreateRiskConfigurationDto RiskConfiguration)
        {
            var result = await Mediator.Send(new CreateRiskConfigurationCommand { CreateRiskConfiguration = RiskConfiguration });

            return CreatedAtAction("GetRiskConfiguration", new { Id = result.ToString() }, RiskConfiguration);
        }

        // DELETE: api/DefinedAnswerCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRiskConfigurationAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteRiskConfigurationCommand { Id = id });

            return Ok();
        }
    }
}
