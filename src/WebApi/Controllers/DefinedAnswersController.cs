﻿using Microsoft.AspNetCore.Mvc;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.DefinedAnswers;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefinedAnswersController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<DefinedAnswerDto>> GetDefinedAnswerAsync()
        {
            return await Mediator.Send(new GetDefinedAnswersQuery());
        }

        // GET: api/Answers/5
        [HttpGet("{id}")]
        public async Task<DefinedAnswerDto> GetDefinedAnswerAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetDefinedAnswerQuery { Id = id });
        }

        // PUT: api/Answers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDefinedAnswer([FromRoute] Guid id, [FromBody] UpdateDefinedAnswerDto updateDefinedAnswer)
        {
            var result = await Mediator.Send(new UpdateDefinedAnswerCommand { UpdateDefinedAnswer = updateDefinedAnswer });

            return Ok(result);
        }

        // POST: api/Answers
        [HttpPost]
        public async Task<IActionResult> PostDefinedAnswer([FromBody] CreateDefinedAnswerDto DefinedAnswer)
        {
            var result = await Mediator.Send(new CreateDefinedAnswerCommand { CreateDefinedAnswerDto = DefinedAnswer });

            return CreatedAtAction("GetDefinedAnswer", new { Id = result.ToString() }, DefinedAnswer);
        }

        // DELETE: api/Answers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDefinedAnswerAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteDefinedAnswerCommand { Id = id });

            return Ok();
        }
    }
}
