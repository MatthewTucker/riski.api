﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Riski.Api.Application.Common.Interfaces;

namespace WebApi.Controllers
{
    public class TokenController : ApiControllerBase
    {
        private readonly IIdentityService _identityService;
        private readonly IApplicationDbContext _dbContext;

        public TokenController(IIdentityService identityService, IApplicationDbContext applicationDbContext)
        {
            _identityService = identityService;
            _dbContext = applicationDbContext;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateTokenAsync([FromQuery] string username, [FromQuery] string password)
        {
            //var username = Request.Form["username"];
            //var password = Request.Form["password"];

            //if (string.IsNullOrEmpty(username))
            //{
            //    Request.Query.TryGetValue("username", out username);
            //}

            //if (string.IsNullOrEmpty(password))
            //{
            //    Request.Query.TryGetValue("password", out password);
            //}

            try
            {
                var user = await _dbContext.Users.SingleOrDefaultAsync(u => u.Username == username);

                if (user == null)
                {
                    return BadRequest("Unvalid username or password");
                }

                var email = user.Email.Value;
                var token = await _identityService.AuthenticateAsync(email, password);

                if (token == null)
                {
                    return Unauthorized();
                }

                return Ok(token);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest("Unvalid username or password");
            }
        }
    }
}
