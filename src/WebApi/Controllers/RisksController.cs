﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.Risks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RisksController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<RiskDto>> GetRisksAsync()
        {
            return await Mediator.Send(new GetRisksQuery());
        }

        // GET: api/Risks/5
        [HttpGet("{id}")]
        public async Task<RiskDto> GetRiskAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetRiskQuery { Id = id });
        }

        [HttpPut("{id}/clear")]
        public async Task<IActionResult> ClearRiskAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new ClearRiskCommand { Id = id });

            return Ok();
        }
    }
}