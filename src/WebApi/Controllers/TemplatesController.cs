﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.Templates;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TemplatesController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<TemplateDto>> GetTemplatesAsync()
        {
            return await Mediator.Send(new GetTemplatesQuery());
        }

        // GET: api/Templates/5
        [HttpGet("{id}")]
        public async Task<TemplateDto> GetTemplateAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetTemplateQuery { Id = id });
        }

        // PUT: api/Templates/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTemplate([FromRoute] Guid id, [FromBody] UpdateTemplateDto template)
        {
            var result = await Mediator.Send(new UpdateTemplateCommand { UpdateTemplate = template });

            return Ok(result);
        }

        // POST: api/Templates
        [HttpPost]
        public async Task<IActionResult> PostTemplate([FromBody] CreateTemplateDto template)
        {
            var result = await Mediator.Send(new CreateTemplateCommand { CreateTemplate = template });

            return CreatedAtAction("GetTemplate", new { Id = result.ToString() }, template);
        }

        // DELETE: api/Templates/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTemplateAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteTemplateCommand { Id = id });

            return Ok();
        }
    }
}
