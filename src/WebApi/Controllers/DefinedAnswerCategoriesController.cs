﻿using Microsoft.AspNetCore.Mvc;
using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.DefinedAnswerCategories;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefinedAnswerCategoriesController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<DefinedAnswerCategoryDto>> GetDefinedAnswerCategoriesAsync()
        {
            return await Mediator.Send(new GetDefinedAnswerCategoriesQuery());
        }

        // GET: api/DefinedAnswerCategories/5
        [HttpGet("{id}")]
        public async Task<DefinedAnswerCategoryDto> GetDefinedAnswerCategoryAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetDefinedAnswerCategoryQuery { Id = id });
        }

        // PUT: api/DefinedAnswerCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDefinedAnswerCategory([FromRoute] Guid id, [FromBody] UpdateDefinedAnswerCategoryDto definedAnswerCategory)
        {
            var result = await Mediator.Send(new UpdateDefinedAnswerCategoryCommand { UpdateDefinedAnswerCategory = definedAnswerCategory });

            return Ok(result);
        }

        // POST: api/DefinedAnswerCategories
        [HttpPost]
        public async Task<IActionResult> PostDefinedAnswerCategory([FromBody] CreateDefinedAnswerCategoryDto definedAnswerCategory)
        {
            var result = await Mediator.Send(new CreateDefinedAnswerCategoryCommand { CreateDefinedAnswerCategoryDto = definedAnswerCategory });

            return CreatedAtAction("GetDivision", new { Id = result.ToString() }, definedAnswerCategory);
        }

        // DELETE: api/DefinedAnswerCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDefinedAnswerCategoryAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteDefinedAnswerCategoryCommand { Id = id });

            return Ok();
        }
    }
}
