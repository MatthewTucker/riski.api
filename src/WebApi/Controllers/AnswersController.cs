﻿using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Answers;
using Riski.Api.Application.Common.Models.Dtos;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswersController : ApiControllerBase
    {
        // GET: api/Answers
        [HttpGet]
        public async Task<IList<AnswerDto>> Get()
        {
            return await Mediator.Send(new GetAnswersQuery());
        }

        // GET: api/Answers
        [HttpGet("{id}")]
        public async Task<AnswerDto> Get(Guid id)
        {
            return await Mediator.Send(new GetAnswerQuery { Id = id });
        }
    }
}
