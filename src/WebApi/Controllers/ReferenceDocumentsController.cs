﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.ReferenceDocuments;

namespace WebApi.Controllers
{
    public class ReferenceDocumentsController : ApiControllerBase
    {
        // GET: api/ReferenceDocuments
        [HttpGet]
        public async Task<IList<ReferenceDocumentDto>> GetReferenceDocuments()
        {
            return await Mediator.Send(new GetReferenceDocumentsQuery());
        }

        // GET: api/ReferenceDocuments/5
        [HttpGet("{id}")]
        public async Task<ReferenceDocumentDto> GetReferenceDocument([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetReferenceDocumentQuery { Id = id });
        }

        // PUT: api/ReferenceDocuments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReferenceDocument([FromRoute] Guid id, [FromBody] UpdateReferenceDocumentDto updateReferenceDocumentDto)
        {
            var result = await Mediator.Send(new UpdateReferenceDocumentCommand { UpdateReferenceDocument = updateReferenceDocumentDto });

            return Ok(result);
        }

        // POST: api/ReferenceDocuments
        [HttpPost]
        public async Task<IActionResult> PostReferenceDocument([FromBody] CreateReferenceDocumentDto createReferenceDocumentDto)
        {
            var result = await Mediator.Send(new CreateReferenceDocumentCommand { CreateReferenceDocumentDto = createReferenceDocumentDto });

            return CreatedAtAction("GetReferenceDocument", new { result });
        }

        // DELETE: api/ReferenceDocuments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReferenceDocument([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteReferenceDocumentCommand { Id = id });

            return Ok();
        }
    }
}
