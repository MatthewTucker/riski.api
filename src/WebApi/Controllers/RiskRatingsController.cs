﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using Riski.Api.Application.Common.Models.Dtos;
using Riski.Api.Application.RiskRatings;


namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RiskRatingsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IList<RiskRatingDto>> GetRiskRatingsAsync()
        {
            return await Mediator.Send(new GetRiskRatingsQuery());
        }

        // GET: api/RiskRatings/5
        [HttpGet("{id}")]
        public async Task<RiskRatingDto> GetRiskRatingAsync([FromRoute] Guid id)
        {
            return await Mediator.Send(new GetRiskRatingQuery { Id = id });
        }

        // PUT: api/RiskRatings/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRiskRating([FromRoute] Guid id, [FromBody] UpdateRiskRatingDto RiskRating)
        {
            var result = await Mediator.Send(new UpdateRiskRatingCommand { UpdateRiskRating = RiskRating });

            return Ok(result);
        }

        // POST: api/RiskRatings
        [HttpPost]
        public async Task<IActionResult> PostRiskRating([FromBody] CreateRiskRatingDto RiskRating)
        {
            var result = await Mediator.Send(new CreateRiskRatingCommand { CreateRiskRating = RiskRating });

            return CreatedAtAction("GetDivision", new { Id = result.ToString() }, RiskRating);
        }

        // DELETE: api/RiskRatings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRiskRatingAsync([FromRoute] Guid id)
        {
            await Mediator.Send(new DeleteRiskRatingCommand { Id = id });

            return Ok();
        }
    }
}