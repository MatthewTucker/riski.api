﻿using Riski.Api.Domain.Enums;

using System;

namespace Riski.Api.Domain.Common
{
    public abstract class AuditableEntity
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastModified { get; set; }

        public string LastModifiedBy { get; set; }

        public PersistenceStatus Status { get; set; }
    }
}
