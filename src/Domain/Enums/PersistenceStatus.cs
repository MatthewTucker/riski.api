﻿namespace Riski.Api.Domain.Enums
{
    public enum PersistenceStatus
    {
        Invalid,
        Available,
        Deleted
    }
}
