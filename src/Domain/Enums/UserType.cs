﻿namespace Riski.Api.Domain.Enums
{
    public enum UserType
    {
        Worker,
        Manager,
        Admin
    }
}
