﻿using Riski.Api.Domain.Common;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Domain.Events
{
    public class TodoItemCompletedEvent : DomainEvent
    {
        public TodoItemCompletedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
