﻿using Riski.Api.Domain.Common;
using Riski.Api.Domain.Entities;

namespace Riski.Api.Domain.Events
{
    public class TodoItemCreatedEvent : DomainEvent
    {
        public TodoItemCreatedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
