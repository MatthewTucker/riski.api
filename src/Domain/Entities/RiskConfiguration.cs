﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class RiskConfiguration: AuditableEntity
    {
        public string Code { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int Rating { get; set; }
    }
}
