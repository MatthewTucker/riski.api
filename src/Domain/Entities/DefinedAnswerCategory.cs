﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class DefinedAnswerCategory : AuditableEntity
    {
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
