﻿using System;
using System.Collections.Generic;

using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class FormPage : AuditableEntity
    {
        public string PageName { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public FormPageType FormPageType { get; set; }
        public ICollection<AlertDialog> AlertDialogs { get; set; }
        public ICollection<UIComponent> UIComponents { get; set; }
        public ICollection<Question> Questions { get; set; }
        public Template Template { get; set; }
        public Guid TemplateId { get; set; }
        public string Extras { get; set; }
        public int Position { get; set; }
    }
}
