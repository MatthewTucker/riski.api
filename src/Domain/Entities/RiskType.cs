﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class RiskType : AuditableEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
