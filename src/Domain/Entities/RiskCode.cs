﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class RiskCode : AuditableEntity
    {
        public string Code { get; set; }
        public RiskConfiguration Likelihood { get; set; }
        public RiskConfiguration Consequence { get; set; }
    }
}
