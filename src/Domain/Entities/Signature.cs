﻿using Riski.Api.Domain.Common;
using System;

namespace Riski.Api.Domain.Entities
{
    public class Signature : AuditableEntity
    {
        public Guid UserId { get; set; }
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Base64String { get; set; }
    }
}
