﻿using System.Collections.Generic;

using Riski.Api.Domain.Common;
using Riski.Api.Domain.Enums;
using Riski.Api.Domain.ValueObjects;

namespace Riski.Api.Domain.Entities
{
    public class User : AuditableEntity
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityId { get; set; }
        public UserType UserType { get; set; }
        public Email Email { get; set; }
        public PhoneNumber PhoneNumber { get; set; }
        public virtual ICollection<Site> Sites { get; set; }

        public string FullName() => $"{FirstName} {LastName}";
    }
}
