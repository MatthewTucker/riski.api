﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class Question : AuditableEntity
    {
        public string Description { get; set; }
        public string DisplayType { get; set; }
        public bool ReadOnly { get; set; }
        public string LookupKey { get; set; }
        public bool Mandatory { get; set; }
        public FormPage FormPage { get; set; }
        public int Position { get; set; }
    }
}
