﻿using Riski.Api.Domain.Common;
using System;

namespace Riski.Api.Domain.Entities
{
    public class Answer : AuditableEntity
    {
        public string Value { get; set; }
        public Question Question { get; set; }
        public Guid ApplicationUserId { get; set; }
    }
}
