﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class GuestUser : AuditableEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public Signature Signature { get; set; }

        public string fullName()
        {
            return FirstName + " " + LastName;
        }
    }
}
