﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class ConfigurableValue : AuditableEntity
    {
        public string SystemKey { get; set; }
        public string Category { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
