﻿using System;

namespace Riski.Api.Domain.Entities
{
    public class ReferenceDocument : Document
    {
        public Site? Site { get; set; }
        public WorkTask? WorkTask { get; set; }
        public ReferenceDocumentType ReferenceDocumentType { get; set; }

        public string GetTypeForDisplay()
        {
            return ReferenceDocumentType != null ? ReferenceDocumentType.Description : "n/a";
        }

        public string GetSiteForDisplay()
        {
            return Site != null ? Site.Name : "n/a";
        }

        public string GetTaskForDisplay()
        {
            return WorkTask != null ? WorkTask.Name : "n/a";
        }
    }
}
