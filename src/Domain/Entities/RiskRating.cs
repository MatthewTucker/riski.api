﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class RiskRating: AuditableEntity
    {
        public int Value { get; set; }
        public string Display { get; set; }
        public string Message { get; set; }
        public string Colour { get; set; }
        public string RiskCodes { get; set; }
        public bool ShouldNotify { get; set; }
    }
}
