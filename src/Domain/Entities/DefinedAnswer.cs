﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class DefinedAnswer: AuditableEntity
    {
        public DefinedAnswerCategory Category { get; set; }

        public string Value { get; set; }
    }
}
