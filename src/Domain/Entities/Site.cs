﻿using Riski.Api.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Riski.Api.Domain.Entities
{
    public class Site : AuditableEntity
    {
        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Risk> Risks { get; set; }

        public int riskCountSince(DateTime date)
        {
            return Risks.Where(r => r.Site.Id == this.Id && r.Form.DateTimeSubmitted >= date).Count();
        }

        public int formCountSince(DateTime date)
        {
            //return Forms.Where(f => f.Site.Id == this.Id && f.DateTimeSubmitted >= date).Count();
            return -1;
        }

        public int risksFoundToday()
        {
            DateTime today = DateTime.Today;
            return Risks.Where(r => r.Site.Id == this.Id && r.Form.DateTimeSubmitted == today).Count();
        }

        public int formsSubmittedToday()
        {
            // DateTime today = DateTime.Today;
            //return Forms.Where(f => f.Site.Id == this.Id && f.DateTimeSubmitted == today).Count();
            return -1;
        }

        public string managersForDisplay()
        {
            string display = "";
            foreach (var user in Users)
            {
                if (display.Count() != 0)
                {
                    display += ", ";
                }
                display += $"{user.FirstName} {user.LastName}";
            }
            return display;
        }

        public string[,] formCountsWithTimestampsToday(IEnumerable<Form> forms)
        {
            var results = new Dictionary<string, int>();
            foreach (Form form in forms)
            {
                var key = form.DateTimeSubmitted.ToString();
                if (results.ContainsKey(key))
                {
                    results[key] = 1;
                }
                else
                {
                    results[key] += 1;
                }
            }

            return DictionaryTo2DArray(results);
        }

        public string[,] riskCountWithTimestampsToday(IEnumerable<Form> forms)
        {
            var results = new Dictionary<string, int>();
            foreach (Form form in forms)
            {
                results[form.DateTimeSubmitted.ToString()] += form.Risks.Count;
            }

            return DictionaryTo2DArray(results);
        }

        private static string[,] DictionaryTo2DArray(Dictionary<string, int> results)
        {
            var tmpArray = new string[results.Keys.Count, 2];
            for (int i = 0; i < results.Keys.Count; i++)
            {
                tmpArray[i, 0] = results.Keys.ElementAt(i);
                tmpArray[i, 1] = results.Values.ElementAt(i).ToString();
            }
            return tmpArray;
        }
    }
}
