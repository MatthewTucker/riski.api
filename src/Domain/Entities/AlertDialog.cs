﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class AlertDialog : AuditableEntity
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string PositiveText { get; set; }
        public string NegativeText { get; set; }
        public string PositiveAction { get; set; }
        public string NegativeAction { get; set; }
        public EventCondition EventCondition { get; set; }
        public int Position { get; set; }
        public string Extras { get; set; }
    }
}
