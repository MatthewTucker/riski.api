﻿using Riski.Api.Domain.Common;

using System;
using System.Linq;

namespace Riski.Api.Domain.Entities
{
    public class Document : AuditableEntity
    {
        public string Name { get; set; }
        public string ContentType { get; set; }

        public DateTime UploadDate { get; set; }
        public long Size { get; set; }

        public byte[] Content { get; set; }

        public string GetExtention(string path)
        {
            return path.Split('.').Last();
        }

        public string SizeForDisplay()
        {
            return (Size / 1024f / 1024f).ToString("0.00") + " MB";
        }
    }
}
