﻿using System;

namespace Riski.Api.Domain.Entities
{
    public class UIComponent
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }

        public static string TYPE_HEADER = "HEADER";
        public static string TYPE_FOOTER = "FOOTER";
    }



}
