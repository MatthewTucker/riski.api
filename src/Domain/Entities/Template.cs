﻿using Riski.Api.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riski.Api.Domain.Entities
{
    public class Template : AuditableEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Form> Forms { get; set; }
        public virtual ICollection<FormPage> FormPages { get; set; }
        public string Type { get; set; }
    }
}
