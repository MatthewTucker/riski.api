﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class AppFeature: AuditableEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool Enabled { get; set; }
    }
}
