﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class EventCondition : AuditableEntity
    {
        public string Scope { get; set; }
        public string Specific { get; set; }
        public string Member { get; set; }
        public string Value { get; set; }
    }
}
