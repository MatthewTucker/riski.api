﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class WorkTask : AuditableEntity
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public int MinLikelihoodRating { get; set; }
        public int MinConsequenceRating { get; set; }
    }
}
