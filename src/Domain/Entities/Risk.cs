﻿using Riski.Api.Domain.Common;

using System;

namespace Riski.Api.Domain.Entities
{
    public class Risk : AuditableEntity
    {
        public RiskConfiguration Likelihood { get; set; }
        public RiskConfiguration Consequence { get; set; }
        public string Description { get; set; }
        public RiskType Type { get; set; }
        public virtual Site Site { get; set; }
        public virtual Form Form { get; set; }
        public virtual User ApplicationUser { get; set; }
        public string Code { get; set; }
        public string PostMitigationCode { get; set; }
        public RiskConfiguration PostMitigationConsequence { get; set; }
        public RiskConfiguration PostMigitationLikelihood { get; set; }
        public string ConsequenceDescription { get; set; }
        public string MitigationDescription { get; set; }

        public DateTime Timestamp { get; set; }

        public string RiskStatus { get; set; }
    }
}
