﻿using Riski.Api.Domain.Common;
using Riski.Api.Domain.ValueObjects;
using System.Collections.Generic;

namespace Riski.Api.Domain.Entities
{
    public class TodoList : AuditableEntity
    {
        public string Title { get; set; }

        public Colour Colour { get; set; } = Colour.White;

        public IList<TodoItem> Items { get; private set; } = new List<TodoItem>();
    }
}
