﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class QuestionDisplayType: AuditableEntity
    {
        public string SystemValue { get; set; }
        public string DisplayValue { get; set; }
        public string Description { get; set; }
    }
}
