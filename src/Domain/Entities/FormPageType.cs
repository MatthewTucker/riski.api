﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class FormPageType : AuditableEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool ReadOnly { get; set; }
    }
}
