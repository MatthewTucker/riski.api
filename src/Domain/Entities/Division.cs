﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class Division : AuditableEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
