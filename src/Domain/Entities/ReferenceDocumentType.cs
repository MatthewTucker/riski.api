﻿using Riski.Api.Domain.Common;

namespace Riski.Api.Domain.Entities
{
    public class ReferenceDocumentType : AuditableEntity
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public byte[] DisplayImage { get; set; }
        public byte[] SelectedDisplayImage { get; set; }
    }
}
