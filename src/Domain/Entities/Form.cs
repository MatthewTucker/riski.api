﻿using Riski.Api.Domain.Common;
using System;
using System.Collections.Generic;

namespace Riski.Api.Domain.Entities
{
    public class Form : AuditableEntity
    {
        public string Name { get; set; }
        public User User { get; set; }
        public ICollection<User> ProxyWorkers { get; set; }
        public ICollection<Answer> Answers { get; set; }
        public ICollection<Risk> Risks { get; set; }
        public Site Site { get; set; }
        public DateTime DateTimeSubmitted { get; set; }
        public DateTime? DateTimeCompleted { get; set; }
        public ICollection<Signature> Signatures { get; set; }
        public string ManagerApprovalStatus { get; set; }
        public Template Template { get; set; }
        public ICollection<GuestUser> GuestUsers { get; set; }
        public Guid? ParentFormId { get; set; }
        public ICollection<Form> SubForms { get; set; }
    }
}
