﻿using Riski.Api.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Riski.Api.Domain.ValueObjects
{
    public class PhoneNumber : ValueObject
    {
        public string Value { private set; get; }

        public PhoneNumber(string value)
        {
            if (!IsValid(value))
            {
                throw new ArgumentException($"Invalid phone number: {value}");
            }

            Value = value;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }

        public static bool IsValid(string value)
        {
            var cleanValue = value?.Replace(" ", string.Empty);

            if (string.IsNullOrEmpty(cleanValue))
            {
                return false;
            }

            var numbersOnlyRegex = new Regex("^[0-9]+$");

            if (cleanValue.First() == '+')
            {
                return numbersOnlyRegex.IsMatch(cleanValue.Substring(1));
            }

            return numbersOnlyRegex.IsMatch(cleanValue);
        }
    }
}
